-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 01, 2020 at 04:37 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `e_rapot`
--

-- --------------------------------------------------------

--
-- Table structure for table `bobot_kkm`
--

CREATE TABLE `bobot_kkm` (
  `kode_bobot_kkm` int(11) NOT NULL,
  `kode_mapel` varchar(200) NOT NULL,
  `bobot_rph` int(20) NOT NULL,
  `bobot_pat` int(20) NOT NULL,
  `total_bobot` int(20) NOT NULL,
  `kkm` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bobot_kkm`
--

INSERT INTO `bobot_kkm` (`kode_bobot_kkm`, `kode_mapel`, `bobot_rph`, `bobot_pat`, `total_bobot`, `kkm`) VALUES
(4, 'A03', 20, 80, 100, 70),
(5, 'A02', 40, 60, 100, 76),
(6, 'A01', 30, 70, 100, 70),
(7, 'A06', 50, 50, 100, 70);

-- --------------------------------------------------------

--
-- Table structure for table `catatan_siswa`
--

CREATE TABLE `catatan_siswa` (
  `id_catatan` int(11) NOT NULL,
  `id_siswa` varchar(255) NOT NULL,
  `id_kelas` varchar(255) NOT NULL,
  `sikap_spiritual_predikat` varchar(255) NOT NULL,
  `sikap_spiritual_deskripsi` text NOT NULL,
  `sikap_sosial_predikat` varchar(255) NOT NULL,
  `sikap_sosial_deskripsi` text NOT NULL,
  `sakit` varchar(255) NOT NULL,
  `izin` varchar(255) NOT NULL,
  `tanpa_keterangan` varchar(255) NOT NULL,
  `status_siswa` varchar(255) NOT NULL,
  `catatan_wali` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `catatan_siswa`
--

INSERT INTO `catatan_siswa` (`id_catatan`, `id_siswa`, `id_kelas`, `sikap_spiritual_predikat`, `sikap_spiritual_deskripsi`, `sikap_sosial_predikat`, `sikap_sosial_deskripsi`, `sakit`, `izin`, `tanpa_keterangan`, `status_siswa`, `catatan_wali`) VALUES
(1, '9700089', '4', 'Cukup', 'pertahankan', 'Baik', 'pertahankan', '2', '10', '3', 'Aktif', 'sangat baik'),
(3, '970003', '4', 'Kurang Baik', 'perbaiki', 'Kurang Baik', 'perbaiki', '', '', '', '', ''),
(4, '970002', '4', 'Baik', 'Mulai berkembang kebiasaan berdoa sebelum dan sesudah melakukan kegiatan, dan menjalankan ibadah sunah dan wajib serta mengajak teman untuk melakukan ibadah, memberi dan menjawa salam.', 'Baik', 'Mau mengakui kesalahan atau kekeliruan,Melaksanakan peraturan sekolah dengan baik,Menunjukkan wajah ramah, bersahabat, dan tidak cemberut,Menjaga keasrian, keindahan, dan kebersihan lingkungan sekolah, serta Berani mencoba hal baru,', '', '', '', '', ''),
(6, 'plugins', 'images', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ekstrakulikuler`
--

CREATE TABLE `ekstrakulikuler` (
  `kode_ekstra` int(11) NOT NULL,
  `nama_ekstra` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ekstrakulikuler`
--

INSERT INTO `ekstrakulikuler` (`kode_ekstra`, `nama_ekstra`) VALUES
(5, 'Pramuka');

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE `guru` (
  `kode_guru` int(11) NOT NULL,
  `nama_lengkap` varchar(200) NOT NULL,
  `nip` varchar(200) NOT NULL,
  `nuptk` varchar(200) NOT NULL,
  `jenis_kelamin` enum('L','P') NOT NULL,
  `agama` varchar(200) NOT NULL,
  `alamat` text NOT NULL,
  `foto` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`kode_guru`, `nama_lengkap`, `nip`, `nuptk`, `jenis_kelamin`, `agama`, `alamat`, `foto`) VALUES
(15, 'Septia Dwi Kurniawan', '', '12345', 'L', 'islam', 'karangcegak', '123451.jpg'),
(16, 'Coba', '', '54321', 'P', 'islam', 'coba', '54321.jpg'),
(17, 'Pepi', '-', '1997', 'L', 'islam', '-', '');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `kode_kelas` int(11) NOT NULL,
  `tingkat` varchar(255) NOT NULL,
  `rombel` varchar(255) NOT NULL,
  `kode_guru` int(50) NOT NULL,
  `tahun_ajar` varchar(255) NOT NULL,
  `semester` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`kode_kelas`, `tingkat`, `rombel`, `kode_guru`, `tahun_ajar`, `semester`) VALUES
(4, '7', 'A', 12345, '2019/2020', 'Genap'),
(8, '8', 'A', 54321, '2019/2020', 'Genap');

-- --------------------------------------------------------

--
-- Table structure for table `mata_pelajaran`
--

CREATE TABLE `mata_pelajaran` (
  `kode_mapel` varchar(100) NOT NULL,
  `tingkat` varchar(100) NOT NULL,
  `nama_mapel` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mata_pelajaran`
--

INSERT INTO `mata_pelajaran` (`kode_mapel`, `tingkat`, `nama_mapel`, `deskripsi`) VALUES
('A01', '7', 'Matematika', ''),
('A0100', '7', 'IPA', ''),
('A02', '8', 'Matematika', ''),
('A03', '9', 'Matematika', ''),
('A06', '7', 'IPS', 'Mengenal konsep-konsep yang berkaitan dengan kehidupan masyarakat dan lingkungannya.');

-- --------------------------------------------------------

--
-- Table structure for table `mengajar`
--

CREATE TABLE `mengajar` (
  `kode_mengajar` int(11) NOT NULL,
  `kode_guru` varchar(100) NOT NULL,
  `kode_mapel` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mengajar`
--

INSERT INTO `mengajar` (`kode_mengajar`, `kode_guru`, `kode_mapel`) VALUES
(12, '12345', 'A01'),
(14, '54321', 'A06'),
(15, '54321', 'A01');

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE `nilai` (
  `id_nilai` int(11) NOT NULL,
  `id_siswa` varchar(200) NOT NULL,
  `id_kelas` varchar(200) NOT NULL,
  `id_mapel` varchar(200) NOT NULL,
  `uh1` int(20) NOT NULL,
  `uh2` int(20) NOT NULL,
  `uh3` int(20) NOT NULL,
  `uh4` int(20) NOT NULL,
  `uh5` int(20) NOT NULL,
  `uh6` int(20) NOT NULL,
  `uas` int(20) NOT NULL,
  `rph` int(20) NOT NULL,
  `uts` int(20) NOT NULL,
  `nilai_akhir` int(20) NOT NULL,
  `predikat_akhir` varchar(200) NOT NULL,
  `deskripsi_nilai_akhir` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai`
--

INSERT INTO `nilai` (`id_nilai`, `id_siswa`, `id_kelas`, `id_mapel`, `uh1`, `uh2`, `uh3`, `uh4`, `uh5`, `uh6`, `uas`, `rph`, `uts`, `nilai_akhir`, `predikat_akhir`, `deskripsi_nilai_akhir`) VALUES
(11, '9700089', '4', 'A01', 70, 80, 90, 80, 70, 80, 100, 81, 100, 94, 'A', 'dsa'),
(12, '970002', '4', 'A01', 80, 80, 90, 90, 90, 80, 90, 86, 90, 89, 'B', 'fdgf'),
(13, '970003', '4', 'A01', 90, 70, 80, 90, 80, 90, 90, 81, 70, 87, 'B', 'cek 123'),
(14, '9700089', '4', 'A06', 80, 80, 90, 70, 80, 80, 90, 83, 100, 87, 'B', 'cek'),
(15, '970001', '8', 'A02', 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 'A', 'luar biasa');

-- --------------------------------------------------------

--
-- Table structure for table `nilai_ekstra`
--

CREATE TABLE `nilai_ekstra` (
  `id_nilai_ekstra` int(11) NOT NULL,
  `id_siswa` varchar(255) NOT NULL,
  `id_kelas` varchar(255) NOT NULL,
  `kode_ekstra` varchar(255) NOT NULL,
  `predikat` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai_ekstra`
--

INSERT INTO `nilai_ekstra` (`id_nilai_ekstra`, `id_siswa`, `id_kelas`, `kode_ekstra`, `predikat`, `deskripsi`) VALUES
(5, '9700089', '4', '5', 'A', 'sdad');

-- --------------------------------------------------------

--
-- Table structure for table `prestasi`
--

CREATE TABLE `prestasi` (
  `id_prestasi` int(11) NOT NULL,
  `id_siswa` varchar(255) NOT NULL,
  `id_kelas` varchar(255) NOT NULL,
  `nama_prestasi` varchar(255) NOT NULL,
  `predikat` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prestasi`
--

INSERT INTO `prestasi` (`id_prestasi`, `id_siswa`, `id_kelas`, `nama_prestasi`, `predikat`, `deskripsi`) VALUES
(19, '9700089', '4', 'Juara 1 Mateimatika', 'A', 'pertahankan');

-- --------------------------------------------------------

--
-- Table structure for table `ranking_siswa`
--

CREATE TABLE `ranking_siswa` (
  `id_ranking` int(11) NOT NULL,
  `id_siswa` varchar(255) NOT NULL,
  `id_kelas` varchar(255) NOT NULL,
  `ranking` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ranking_siswa`
--

INSERT INTO `ranking_siswa` (`id_ranking`, `id_siswa`, `id_kelas`, `ranking`) VALUES
(1, '9700089', '4', '3'),
(2, '970003', '4', '2'),
(3, '970002', '4', '1');

-- --------------------------------------------------------

--
-- Table structure for table `rombel`
--

CREATE TABLE `rombel` (
  `kode_rombel` int(11) NOT NULL,
  `kode_kelas` int(50) NOT NULL,
  `nis` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rombel`
--

INSERT INTO `rombel` (`kode_rombel`, `kode_kelas`, `nis`) VALUES
(6, 8, '970001'),
(7, 4, '970002'),
(8, 4, '9700089'),
(10, 4, '970003');

-- --------------------------------------------------------

--
-- Table structure for table `semester`
--

CREATE TABLE `semester` (
  `kode_semester` int(11) NOT NULL,
  `nama_semester` varchar(200) NOT NULL,
  `aktif` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `semester`
--

INSERT INTO `semester` (`kode_semester`, `nama_semester`, `aktif`) VALUES
(22, 'Genap', 'Y'),
(23, 'Ganjil', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `nis` int(50) NOT NULL,
  `nisn` varchar(255) NOT NULL,
  `nama_lengkap` varchar(250) NOT NULL,
  `tempat_lahir` varchar(250) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` enum('L','P') NOT NULL,
  `agama` varchar(250) NOT NULL,
  `alamat` text NOT NULL,
  `diterima` date NOT NULL,
  `foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`nis`, `nisn`, `nama_lengkap`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `agama`, `alamat`, `diterima`, `foto`) VALUES
(970001, '', 'Coba', 'Purwokerto', '2020-03-21', 'L', 'islam', 'karangcegak', '2020-03-21', '9700011.JPG'),
(970002, '', 'Pepi', 'Banyumas', '2020-03-25', 'L', 'islam', 'Banyumas', '2020-03-25', '970002.jpg'),
(970003, '', 'Kurniawan', 'Banyumas', '2020-03-25', 'L', 'islam', 'Banyumas', '2020-03-25', '9700033.JPG'),
(9700089, '10091997', 'AAN ANDRIAWAN', 'Banyumas', '2020-05-01', 'L', 'islam', 'karangcegak', '2020-05-01', '');

-- --------------------------------------------------------

--
-- Table structure for table `tahun_ajar`
--

CREATE TABLE `tahun_ajar` (
  `kode_tahunajar` int(11) NOT NULL,
  `nama_tahunajar` varchar(200) NOT NULL,
  `aktif` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tahun_ajar`
--

INSERT INTO `tahun_ajar` (`kode_tahunajar`, `nama_tahunajar`, `aktif`) VALUES
(2, '2019/2020', 'Y'),
(4, '2020/2021', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` varchar(20) DEFAULT NULL,
  `id_guru` varchar(25) NOT NULL,
  `created_date` timestamp(6) NOT NULL DEFAULT current_timestamp(6)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `level`, `id_guru`, `created_date`) VALUES
(1, 'admin', '80177534a0c99a7e3645b52f2027a48b', 'admin', '', '2020-03-25 05:36:29.110836'),
(2, '12345', '202cb962ac59075b964b07152d234b70', 'wali_murid', '12345', '2020-05-12 11:22:21.853225'),
(3, '54321', '202cb962ac59075b964b07152d234b70', 'wali_murid', '54321', '2020-05-19 10:03:32.386580'),
(4, '1997', '202cb962ac59075b964b07152d234b70', 'guru', '1997', '2020-06-01 02:09:34.060473');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bobot_kkm`
--
ALTER TABLE `bobot_kkm`
  ADD PRIMARY KEY (`kode_bobot_kkm`);

--
-- Indexes for table `catatan_siswa`
--
ALTER TABLE `catatan_siswa`
  ADD PRIMARY KEY (`id_catatan`);

--
-- Indexes for table `ekstrakulikuler`
--
ALTER TABLE `ekstrakulikuler`
  ADD PRIMARY KEY (`kode_ekstra`);

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`kode_guru`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`kode_kelas`);

--
-- Indexes for table `mata_pelajaran`
--
ALTER TABLE `mata_pelajaran`
  ADD PRIMARY KEY (`kode_mapel`);

--
-- Indexes for table `mengajar`
--
ALTER TABLE `mengajar`
  ADD PRIMARY KEY (`kode_mengajar`);

--
-- Indexes for table `nilai`
--
ALTER TABLE `nilai`
  ADD PRIMARY KEY (`id_nilai`);

--
-- Indexes for table `nilai_ekstra`
--
ALTER TABLE `nilai_ekstra`
  ADD PRIMARY KEY (`id_nilai_ekstra`);

--
-- Indexes for table `prestasi`
--
ALTER TABLE `prestasi`
  ADD PRIMARY KEY (`id_prestasi`);

--
-- Indexes for table `ranking_siswa`
--
ALTER TABLE `ranking_siswa`
  ADD PRIMARY KEY (`id_ranking`);

--
-- Indexes for table `rombel`
--
ALTER TABLE `rombel`
  ADD PRIMARY KEY (`kode_rombel`);

--
-- Indexes for table `semester`
--
ALTER TABLE `semester`
  ADD PRIMARY KEY (`kode_semester`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`nis`);

--
-- Indexes for table `tahun_ajar`
--
ALTER TABLE `tahun_ajar`
  ADD PRIMARY KEY (`kode_tahunajar`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bobot_kkm`
--
ALTER TABLE `bobot_kkm`
  MODIFY `kode_bobot_kkm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `catatan_siswa`
--
ALTER TABLE `catatan_siswa`
  MODIFY `id_catatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `ekstrakulikuler`
--
ALTER TABLE `ekstrakulikuler`
  MODIFY `kode_ekstra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `guru`
--
ALTER TABLE `guru`
  MODIFY `kode_guru` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `kode_kelas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `mengajar`
--
ALTER TABLE `mengajar`
  MODIFY `kode_mengajar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `nilai`
--
ALTER TABLE `nilai`
  MODIFY `id_nilai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `nilai_ekstra`
--
ALTER TABLE `nilai_ekstra`
  MODIFY `id_nilai_ekstra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `prestasi`
--
ALTER TABLE `prestasi`
  MODIFY `id_prestasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `ranking_siswa`
--
ALTER TABLE `ranking_siswa`
  MODIFY `id_ranking` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `rombel`
--
ALTER TABLE `rombel`
  MODIFY `kode_rombel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `semester`
--
ALTER TABLE `semester`
  MODIFY `kode_semester` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `tahun_ajar`
--
ALTER TABLE `tahun_ajar`
  MODIFY `kode_tahunajar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
