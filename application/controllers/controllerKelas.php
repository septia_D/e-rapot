<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ControllerKelas extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('KelasModel');
        $this->load->model('RombelModel');
        $this->load->library('form_validation');
        $this->load->library('Datatables');
        $this->load->library('upload');
        $this->load->helper(array('form', 'url'));
        if (empty($this->session->session_login['username'])) {
            $this->session->set_flashdata("pesan", "Anda harus login terlebih dahulu.");
            redirect(site_url("controllerLogin"));
        }
    }

    public function json()
    {
        header('Content-Type: application/json');
        echo $this->KelasModel->json();
    }

    public function json_aturKelas()
    {
        $kode_kelas = $this->input->post("kode_kelas");
        header('Content-Type: application/json');
        echo $this->RombelModel->json($kode_kelas);
    }

    public function index()
    {
        $data['listGuru'] = $this->KelasModel->get_all_guru();
        $this->load->view('header');
        $this->load->view('kelas/listKelas', $data);
        $this->load->view('footer');
    }

    public function insert_rombel()
    {
        $data = [
            'action'           => site_url("controllerKelas/insert_kelas_action"),
            'listGuru'         => $this->KelasModel->get_all_guru(),
            'listTahunAjar'    => $this->KelasModel->get_all_tahunAjar(),
            'listSemester'     => $this->KelasModel->get_all_semester(),
        ];
        $this->load->view('header');
        $this->load->view('kelas/formKelas', $data);
        $this->load->view('footer');
    }

    public function insert_kelas_action()
    {
        $this->form_validation->set_rules('tingkat', 'Tingkat', 'required');
        $this->form_validation->set_rules('rombel', 'Rombel', 'required');
        $this->form_validation->set_rules('kode_guru', 'Guru', 'required');
        $this->form_validation->set_rules('tahun_ajar', 'Tahun Ajar', 'required');
        $this->form_validation->set_rules('semester', 'Semester', 'required');
        $this->form_validation->set_message('required', '* {field} Harus diisi');

        if ($this->form_validation->run() == FALSE) {
            $this->insert_rombel();
        } else {

            $data = [
                'kode_kelas'      => $this->input->post("kode_kelas"),
                'tingkat'         => $this->input->post("tingkat"),
                'rombel'          => $this->input->post("rombel"),
                'kode_guru'       => $this->input->post("kode_guru"),
                'tahun_ajar'      => $this->input->post("tahun_ajar"),
                'semester'        => $this->input->post("semester"),
            ];
            $kode = $this->input->post("kode_guru");
            $data_guru = $this->KelasModel->get_by_id_guru($kode);
            $nuptk  = $data_guru->nuptk;
            $data_user = [
                'level' => 'wali_murid'
            ];

            $cek_ketersediaan_kelas = $this->KelasModel->cek_ketersediaan_kelas($this->input->post("tingkat"), $this->input->post("rombel"));
            if (empty($cek_ketersediaan_kelas)) {
                $this->KelasModel->insert_kelas($data);
                $this->KelasModel->update_user($nuptk, $data_user);
                $this->session->set_flashdata("flash_message", "Berhasil tambah data rombel.");
            } else {
                $this->session->set_flashdata("error_message", "kelas sudah ada.");
            }
            redirect(site_url("controllerKelas"));
        }
    }

    public function atur_kelas($kode_kelas)
    {
        $data_kelas = $this->KelasModel->get_by_id($kode_kelas);

        $data = [
            'action'        => site_url("controllerKelas/insert_siswa_kelas"),
            "kode_kelas"    => $data_kelas->kode_kelas,
            "tingkat"       => $data_kelas->tingkat,
            "rombel"        => $data_kelas->rombel,
            "nama_lengkap"  => $data_kelas->nama_lengkap,
            'listSiswa'     => $this->KelasModel->get_all_siswa(),
            'jumlahSiswa'   => $this->KelasModel->jumlah_siswa($kode_kelas)->row_array(),
        ];

        $this->load->view('header');
        $this->load->view('kelas/formRombel', $data);
        $this->load->view('footer');
    }

    public function insert_siswa_kelas()
    {
        $kode_kelas = $this->input->post("kode_kelas");
        $nis = $this->input->post("nis");
        $data = [
            'kode_kelas'    => $kode_kelas,
            'nis'           => $nis,
        ];

        $cek_data_kelas = $this->RombelModel->get_by_id_siswa($nis);

        if ($cek_data_kelas) {
            $this->session->set_flashdata("error_message", "gagal tambah siswa sudah mempunyai kelas.");
        } else {
            $this->KelasModel->insert_aturSiswa($data);
            $this->session->set_flashdata("flash_message", "Berhasil tambah siswa.");
        }

        redirect(site_url("controllerKelas/atur_kelas/" . $kode_kelas));
    }

    public function edit_wali_kelas()
    {
        $kode_kelas = $this->input->post("kode_kelas");
        $kode_guru = $this->input->post("kode_guru");
        $kode_guru_lama = $this->input->post("kode_guru_lama");

        $data = [
            'kode_guru'     => $kode_guru,
        ];

        $data_guru_lama = $this->KelasModel->get_by_id_guru($kode_guru_lama);
        $nuptk_lama  = $data_guru_lama->nuptk;
        $data_guru = $this->KelasModel->get_by_id_guru($kode_guru);
        $nuptk_update  = $data_guru->nuptk;
        $data_user_update = [
            'level' => 'wali_murid'
        ];

        $data_user_lama = [
            'level' => 'guru'
        ];

        $this->KelasModel->update_kelas($kode_kelas, $data);
        $this->KelasModel->update_user($nuptk_lama, $data_user_lama);
        $this->KelasModel->update_user($nuptk_update, $data_user_update);

        $this->session->set_flashdata("flash_message", "Berhasil edit data wali kelas.");
        redirect(site_url("ControllerKelas"));
    }

    public function hapus_kelas($kode_kelas)
    {
        $data_kelas = $this->KelasModel->get_by_id($kode_kelas);
        $kode_guru = $data_kelas->kode_guru;
        $data_guru = $this->KelasModel->get_by_id_guru($kode_guru);
        $nuptk  = $data_guru->nuptk;
        $data_user = [
            'level' => 'guru'
        ];

        if ($data_kelas) {
            $this->KelasModel->delete_kelas($kode_kelas);
            $this->KelasModel->update_user($nuptk, $data_user);

            $this->session->set_flashdata("flash_message", "Berhasil hapus data rombel.");
            redirect(site_url("controllerKelas"));
        } else {
            $this->session->set_flashdata("error_message", "Gagal hapus data kelas.");
            redirect(site_url("controllerKelas"));
        }
    }

    public function hapus_siswa_rombel($kode_rombel)
    {
        $data_siswa = $this->RombelModel->get_by_id($kode_rombel);
        $kode_kelas = $data_siswa->kode_kelas;

        if ($data_siswa) {
            $this->RombelModel->delete_siswa($kode_rombel);
            $this->session->set_flashdata("flash_message", "Berhasil hapus data siswa.");
            redirect(site_url("controllerKelas/atur_kelas/" . $kode_kelas));
        } else {
            $this->session->set_flashdata("error_message", "Gagal hapus data siswa.");
            redirect(site_url("controllerKelas/atur_kelas/" . $kode_kelas));
        }
    }
}
