<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ControllerKonfigurasi extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->model('KelasModel');
        $this->load->library('form_validation');
        if(empty($this->session->session_login['username'])){
            $this->session->set_flashdata("pesan", "Anda harus login terlebih dahulu.");
            redirect(site_url("controllerLogin"));
        }
    }

	public function index()
	{   
        $data = [
            'listSemester'  => $this->KelasModel->get_all_semester(),
            'listTahunAjar' => $this->KelasModel->get_all_tahunAjar()
        ];
        $this->load->view('header');
        $this->load->view('konfigurasi/formKonfigurasi', $data);
        $this->load->view('footer');
    }

    public function insert_session_konfigurasi()
    {
        $this->form_validation->set_rules('tahun_ajar', 'Tahun Ajar', 'required');
        $this->form_validation->set_rules('semester', 'Semester', 'required');
        $this->form_validation->set_message('required', '* {field} Harus diisi');

        if ($this->form_validation->run() == FALSE){       
           $this->index();
        }else{
            $data = [
                'tahun_ajar'      => $this->input->post("tahun_ajar"),
                'semester'        => $this->input->post("semester"),
            ];

            $this->session->set_userdata('konfigurasi', $data);
            redirect(site_url("ControllerHome"));

        }
    }
    
}
