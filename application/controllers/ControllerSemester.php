<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ControllerSemester extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->model('SemesterModel');
        $this->load->library('form_validation');
        if(empty($this->session->session_login['username'])){
            $this->session->set_flashdata("pesan", "Anda harus login terlebih dahulu.");
            redirect(site_url("controllerLogin"));
        }
    }

	public function index()
	{   
        $data['semester'] = $this->SemesterModel->get_all();
        $this->load->view('header');
        $this->load->view('semester/listSemester', $data);
        $this->load->view('footer');
    }

    public function insert_semester()
    {
        $data = [
            'action'        => site_url("controllerSemester/insert_semester_action"),         
            'kode_semester' => set_value("kode_semester"),
            'nama_semester' => set_value("nama_semester"),
            'aktif'         => set_value("aktif")
        ];
        $this->load->view('header');
        $this->load->view('semester/formSemester', $data);
        $this->load->view('footer');
    }

    public function insert_semester_action()
    {
        $this->form_validation->set_rules('nama_semester', 'Semester', 'required');
        $this->form_validation->set_rules('aktif', 'Aktif', 'required');
        $this->form_validation->set_message('required', '* {field} Harus diisi');

        if ($this->form_validation->run() == FALSE){       
            $this->insert_semester();
        }else{
            $nama_semester = $this->input->post("nama_semester");
            $aktif         = $this->input->post("aktif");

            $data = [
                'nama_semester' => $nama_semester,
                'aktif'         => $aktif
            ];

            $this->SemesterModel->insert_semester($data);
            $this->session->set_flashdata("flash_message", "Berhasil tambah data semester.");
            redirect(site_url("controllerSemester"));
        }
    }

    public function edit_semester($kode_semester)
    {
        $data_semester = $this->SemesterModel->get_by_id($kode_semester);
        if($data_semester){
            $data = [
                'action'        => site_url("controllerSemester/edit_semester_action"),
                'kode_semester' => set_value("kode_semester", $data_semester->kode_semester),
                'nama_semester' => set_value("nama_semester", $data_semester->nama_semester),
                'aktif'         => set_value("aktif", $data_semester->aktif)
            ];
            $this->load->view("header");
            $this->load->view("semester/formSemester", $data);
            $this->load->view("footer");
        } else {
            $this->session->set_flashdata("error_message", "Gagal edit data semester.");
            redirect(site_url("controllerSemester"));
        }
    }

    public function edit_semester_action()
    {
        $this->form_validation->set_rules('nama_semester', 'Semester', 'required');
        $this->form_validation->set_rules('aktif', 'Aktif', 'required');
        $this->form_validation->set_message('required', '* {field} Harus diisi');

        if ($this->form_validation->run() == FALSE){       
            $this->edit_semester($this->input->post("kode_semester"));
        }else{
            $kode_semester = $this->input->post("kode_semester");
            $nama_semester = $this->input->post("nama_semester");
            $aktif         = $this->input->post("aktif");

            $data = [
                'nama_semester' => $nama_semester,
                'aktif'         => $aktif
            ];

            $this->SemesterModel->update_semester($kode_semester, $data);
            $this->session->set_flashdata("flash_message", "Berhasil edit data semester.");

            redirect(site_url("controllerSemester"));
        }
    }

    public function hapus_semester($kode_semester)
    {
        $data_semester = $this->SemesterModel->get_by_id($kode_semester);
        if($data_semester){
            $this->SemesterModel->delete_semester($kode_semester);
            $this->session->set_flashdata("flash_message", "Berhasil hapus data semester.");
            redirect(site_url("controllerSemester"));
        }else{
            $this->session->set_flashdata("error_message", "Gagal hapus data semester.");
            redirect(site_url("controllerSemester"));
        }
    }
    
}
