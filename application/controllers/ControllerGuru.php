<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ControllerGuru extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('GuruModel');
        $this->load->model('MengajarModel');
        $this->load->model('KelasModel');
        $this->load->library('form_validation');
        $this->load->library('Datatables');
        $this->load->library('upload');
        $this->load->helper(array('form', 'url'));
        if (empty($this->session->session_login['username'])) {
            $this->session->set_flashdata("pesan", "Anda harus login terlebih dahulu.");
            redirect(site_url("controllerLogin"));
        }
    }

    public function json()
    {
        header('Content-Type: application/json');
        echo $this->GuruModel->json();
    }

    public function json_mengajar()
    {
        $kode_guru = $this->input->post('kode_guru');
        header('Content-Type: application/json');
        echo $this->MengajarModel->json($kode_guru);
    }

    public function index()
    {
        $this->load->view('header');
        $this->load->view('guru/listGuru');
        $this->load->view('footer');
    }

    public function insert_guru()
    {
        $data = [
            'action'        => site_url("controllerGuru/insert_guru_action"),
            'kode_guru'     => set_value("kode_guru"),
            'nama_lengkap'  => set_value("nama_lengkap"),
            'nip'           => set_value("nip"),
            'nuptk'         => set_value("nuptk"),
            'jenis_kelamin' => set_value("jenis_kelamin"),
            'agama'         => set_value("agama"),
            'alamat'        => set_value("alamat"),
            'password'          => set_value("password"),
            'confirm_password'  => set_value("confirm_password"),
            'foto'              => set_value("foto"),
        ];
        $this->load->view('header');
        $this->load->view('guru/formGuru', $data);
        $this->load->view('footer');
    }

    public function insert_guru_action()
    {
        $this->form_validation->set_rules('nama_lengkap', 'Nama lengkap', 'required');
        $this->form_validation->set_rules('nip', 'Nip', 'required');
        $this->form_validation->set_rules('nuptk', 'Nuptk', 'required');
        $this->form_validation->set_rules('jenis_kelamin', 'Jenis kelamin', 'required');
        $this->form_validation->set_rules('agama', 'Agama', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');
        $this->form_validation->set_message('required', '* {field} Harus diisi');
        $this->form_validation->set_message('matches', '* {field} dan password harus sama');

        if ($this->form_validation->run() == FALSE) {
            $this->insert_guru();
        } else {

            $cek_nuptk = $this->GuruModel->get_by_id($this->input->post("nuptk"));

            if ($cek_nuptk) {
                $this->session->set_flashdata("error_message", "Gagal tambah siswa. NUPTK sudah ada, atas nama " . $cek_nuptk->nama_lengkap);
                redirect(site_url("controllerGuru"));
            }

            // konfigurasi untuk melakukan upload photo
            $config['upload_path']   = './images/';    //path folder image
            $config['allowed_types'] = 'jpg|png|jpeg'; //type yang dapat diupload jpg|png|jpeg			
            $config['file_name']     = url_title($this->input->post('nuptk')); //nama file photo dirubah menjadi nama berdasarkan nim	
            $this->upload->initialize($config);

            if (!empty($_FILES['foto']['name'])) {
                if ($this->upload->do_upload('foto')) {
                    $photo = $this->upload->data();
                    $dataphoto = $photo['file_name'];
                    $this->load->library('upload', $config);

                    $data = [
                        'nama_lengkap'    => $this->input->post("nama_lengkap"),
                        'nip'             => $this->input->post("nip"),
                        'nuptk'           => $this->input->post("nuptk"),
                        'jenis_kelamin'   => $this->input->post("jenis_kelamin"),
                        'agama'           => $this->input->post("agama"),
                        'alamat'          => $this->input->post("alamat"),
                        'foto'                 => $dataphoto,
                    ];

                    $data_users = [
                        'username'  => $this->input->post("nuptk"),
                        'password'  => md5($this->input->post("password")),
                        'level'     => 'guru',
                        'id_guru'   => $this->input->post("nuptk")
                    ];

                    $this->GuruModel->insert_Guru($data);
                    $this->GuruModel->insert_user($data_users);
                }
                $this->session->set_flashdata("flash_message", "Berhasil tambah data guru.");
                redirect(site_url("controllerGuru"));
            } else {
                $data = [
                    'nama_lengkap'    => $this->input->post("nama_lengkap"),
                    'nip'             => $this->input->post("nip"),
                    'nuptk'           => $this->input->post("nuptk"),
                    'jenis_kelamin'   => $this->input->post("jenis_kelamin"),
                    'agama'           => $this->input->post("agama"),
                    'alamat'          => $this->input->post("alamat"),
                ];

                $data_users = [
                    'username'  => $this->input->post("nuptk"),
                    'password'  => md5($this->input->post("password")),
                    'level'     => 'guru',
                    'id_guru'   => $this->input->post("nuptk")
                ];

                $this->GuruModel->insert_Guru($data);
                $this->GuruModel->insert_user($data_users);
            }

            $this->session->set_flashdata("flash_message", "Berhasil tambah data guru.");
            redirect(site_url("controllerGuru"));
        }
    }

    public function atur_mengajar($kode_guru)
    {
        $data_guru = $this->GuruModel->get_by_id_nuptk($kode_guru);
        $data = [
            'action'        => site_url("controllerGuru/insert_mengajar_action"),
            'kode_guru'     => set_value("kode_guru", $data_guru->nuptk),
            'nama_lengkap'  => set_value("nama_lengkap", $data_guru->nama_lengkap),
            'kode_makul'    => $this->GuruModel->get_all(),
        ];
        $this->load->view('header');
        $this->load->view('guru/formAturMengajar', $data);
        $this->load->view('footer');
    }

    public function insert_mengajar_action()
    {
        $kode_guru = $this->input->post("kode_guru");
        $data = [
            'kode_guru'    => $kode_guru,
            'kode_mapel'   => $this->input->post("kode_mapel")
        ];

        $this->GuruModel->insert_aturMengajar($data);
        $this->session->set_flashdata("flash_message", "Berhasil atur mengajar.");
        redirect(site_url("controllerGuru/atur_mengajar/" . $kode_guru));
    }

    public function lihat_guru($kode_guru)
    {
        $data['guru'] = $this->GuruModel->get_by_id($kode_guru);
        $nuptk = $data['guru']->nuptk;
        $data['user'] = $this->GuruModel->get_by_id_user($nuptk);

        $this->load->view('header');
        $this->load->view('guru/profilGuru', $data);
        $this->load->view('footer');
    }


    public function edit_guru_action()
    {
        $kode_guru  = $this->input->post("kode_guru");
        $id = $this->input->post('id_user');

        // konfigurasi untuk melakukan upload photo
        $config['upload_path']   = './images/';    //path folder image
        $config['allowed_types'] = 'jpg|png|jpeg'; //type yang dapat diupload jpg|png|jpeg			
        $config['file_name']     = url_title($this->input->post('nuptk')); //nama file photo dirubah menjadi nama berdasarkan nim	
        $this->upload->initialize($config);

        // Jika file photo ada 
        if (!empty($_FILES['foto']['name'])) {
            if ($this->upload->do_upload('foto')) {
                $photo = $this->upload->data();
                $dataphoto = $photo['file_name'];
                $this->load->library('upload', $config);
                $data = [
                    'nama_lengkap'    => $this->input->post("nama_lengkap"),
                    'nip'             => $this->input->post("nip"),
                    'nuptk'           => $this->input->post("nuptk"),
                    'jenis_kelamin'   => $this->input->post("jenis_kelamin"),
                    'agama'           => $this->input->post("agama"),
                    'alamat'          => $this->input->post("alamat"),
                    'foto'            => $dataphoto,
                ];
                $this->GuruModel->update_guru($kode_guru, $data);

                if(!empty($this->input->post("password")) || $this->input->post("password") != ''){
                    $data_user = [
                        'username'  => $this->input->post('nuptk'),
                        'password'  => md5($this->input->post("password")),
                    ];
                    $this->GuruModel->update_user($id, $data_user);
                }
                
            }
            $this->session->set_flashdata("flash_message", "Berhasil update data guru.");
            redirect(site_url("controllerGuru/lihat_guru/" . $kode_guru));
        } else {
            $data = [
                'nama_lengkap'    => $this->input->post("nama_lengkap"),
                'nip'             => $this->input->post("nip"),
                'nuptk'           => $this->input->post("nuptk"),
                'jenis_kelamin'   => $this->input->post("jenis_kelamin"),
                'agama'           => $this->input->post("agama"),
                'alamat'          => $this->input->post("alamat"),
            ];

            if(!empty($this->input->post("password")) || $this->input->post("password") != ''){
                $data_user = [
                    'username'  => $this->input->post('nuptk'),
                    'password'  => md5($this->input->post("password")),
                ];
                $this->GuruModel->update_user($id, $data_user);
            }

            $this->GuruModel->update_guru($kode_guru, $data);
        }
        $this->session->set_flashdata("flash_message", "Berhasil update data guru.");
        redirect(site_url("controllerGuru/lihat_guru/" . $kode_guru));
    }

    public function hapus_guru($kode_guru)
    {
        $data_guru = $this->GuruModel->get_by_id_nuptk($kode_guru);
        if ($data_guru) {
            $this->GuruModel->delete_guru($data_guru->kode_guru);
            $this->GuruModel->delete_user($data_guru->nuptk);
            $this->session->set_flashdata("flash_message", "Berhasil hapus data guru.");
            redirect(site_url("controllerGuru"));
        } else {
            $this->session->set_flashdata("error_message", "Gagal hapus data guru.");
            redirect(site_url("controllerGuru"));
        }
    }

    public function hapus_mengajar($kode)
    {
        $data_mengajar = $this->MengajarModel->get_by_id($kode);
        $kode_guru = $data_mengajar->kode_guru;
        if ($data_mengajar) {
            $this->MengajarModel->delete_mengajar($kode);
            $this->session->set_flashdata("flash_message", "Berhasil hapus data mengajar.");
            redirect(site_url("controllerGuru/atur_mengajar/" . $kode_guru));
        } else {
            $this->session->set_flashdata("error_message", "Gagal hapus data mengajar.");
            redirect(site_url("controllerGuru/atur_mengajar/" . $kode_guru));
        }
    }
}
