<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ControllerBobot extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('BobotModel');
        $this->load->library('form_validation');
        $this->load->library('Datatables');
        if (empty($this->session->session_login['username'])) {
            $this->session->set_flashdata("pesan", "Anda harus login terlebih dahulu.");
            redirect(site_url("controllerLogin"));
        }
    }

    public function json()
    {
        header('Content-Type: application/json');
        echo $this->BobotModel->json();
    }

    public function index()
    {
        // $data['mata_pelajaran'] = $this->MataPelajaranModel->get_all();
        $this->load->view('header');
        $this->load->view('bobot_kkm/listBobot');
        $this->load->view('footer');
    }

    public function atur_bobot_kkm($kode_mapel)
    {
        $data_mapelBobot    = $this->BobotModel->get_by_id_bobot($kode_mapel)->num_rows();
        $data_mataPelajaran = $this->BobotModel->get_by_id($kode_mapel);

        if ($data_mapelBobot ==  1) {
            $get_mapelBobot    = $this->BobotModel->get_by_id_bobot($kode_mapel)->row();
            $data = [
                'action'        => site_url("ControllerBobot/edit_bobot_kkm_action"),
                'mapel'         => $data_mataPelajaran->nama_mapel,
                'kode_mapel'    => $data_mataPelajaran->kode_mapel,
                'tingkat'       => $data_mataPelajaran->tingkat,
                'kode_bobot_kkm'    => $get_mapelBobot->kode_bobot_kkm,
                'bobot_rph'         => $get_mapelBobot->bobot_rph,
                'bobot_pat'         => $get_mapelBobot->bobot_pat,
                'total_bobot'       => $get_mapelBobot->total_bobot,
                'kkm'               => $get_mapelBobot->kkm
            ];
        } else {
            $data = [
                'action'        => site_url("ControllerBobot/atur_bobot_kkm_action"),
                'mapel'         => $data_mataPelajaran->nama_mapel,
                'kode_mapel'    => $data_mataPelajaran->kode_mapel,
                'tingkat'       => $data_mataPelajaran->tingkat,
                'kode_bobot_kkm'    => set_value('kode_bobot_kkm'),
                'bobot_rph'         => set_value('bobot_rph'),
                'bobot_pat'         => set_value('bobot_pat'),
                'total_bobot'       => set_value('total_bobot'),
                'kkm'               => set_value('kkm')
            ];
        }

        $this->load->view("header");
        $this->load->view('bobot_kkm/formBobotKkm', $data);
        $this->load->view("footer");
    }

    public function atur_bobot_kkm_action()
    {
        $this->form_validation->set_rules('kode_mapel', 'Kode Mapel', 'required');
        $this->form_validation->set_rules('bobot_rph', 'Bobot RPH', 'required');
        $this->form_validation->set_rules('bobot_pat', 'Bobot PAT', 'required');
        $this->form_validation->set_rules('kkm', 'KKM', 'required');
        $this->form_validation->set_message('required', '* {field} Harus diisi');

        if ($this->form_validation->run() == FALSE) {
            $this->atur_bobot_kkm($this->input->post("kode_mapel"));
        } else {
            $kode_mapel     = $this->input->post("kode_mapel");
            $bobot_rph      = intval($this->input->post("bobot_rph"));
            $bobot_pat      = intval($this->input->post("bobot_pat"));
            $total_bobot    = intval($this->input->post("total_bobot"));
            $kkm            = intval($this->input->post("kkm"));

            $data = [
                'kode_mapel'    => $kode_mapel,
                'bobot_rph'     => $bobot_rph,
                'bobot_pat'     => $bobot_pat,
                'total_bobot'   => $total_bobot,
                'kkm'           => $kkm
            ];

            $this->BobotModel->insert($data);
            $this->session->set_flashdata("flash_message", "Berhasil atur bobot dan kkm.");

            redirect(site_url("controllerBobot"));
        }
    }

    public function edit_bobot_kkm_action()
    {
        $kode_bobot_kkm  = $this->input->post("kode_bobot_kkm");
        $bobot_rph       = intval($this->input->post("bobot_rph"));
        $bobot_pat       = intval($this->input->post("bobot_pat"));
        $total_bobot     = intval($this->input->post("total_bobot"));
        $kkm             = intval($this->input->post("kkm"));

        $data = [
            'bobot_rph'     => $bobot_rph,
            'bobot_pat'     => $bobot_pat,
            'total_bobot'   => $total_bobot,
            'kkm'           => $kkm
        ];

        $this->BobotModel->update($kode_bobot_kkm, $data);
        $this->session->set_flashdata("flash_message", "Berhasil edit bobot dan kkm.");

        redirect(site_url("controllerBobot"));
    }
}
