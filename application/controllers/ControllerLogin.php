<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ControllerLogin extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->model('LoginModel');
    }

	public function index()
	{   
        if (empty($this->session->userdata("username"))){
            $this->load->view("viewLogin");
        }else{
            redirect("ControllerHome");
        }
    }

    public function cekStatusLogin()
    {
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_message('required', '* {field} Harus diisi');

        if ($this->form_validation->run() == FALSE){       
            $this->index();
        }else{
            $username = $this->input->post("username", TRUE);
            $password = md5($this->input->post("password", TRUE));
            
            $where = ["username" => $username];
            $cek = $this->LoginModel->validasi("users", $where)->row_array();
            $username_db = $cek['username'];
            $password_db = $cek['password'];
            if ($username == $username && $password == $password_db){
                $data_guru = $this->LoginModel->get_by_id_guru($cek['id_guru']);

                $data_session = [
                    'username' => $username_db,
                    'id_guru'  => $cek['id_guru'],
                    'level'    => $cek['level'],
                    'nama_guru' => $data_guru->nama_lengkap
                ];
                
                $this->session->set_userdata("session_login", $data_session);
                redirect(site_url("controllerHome"));
            } else {
                $this->session->set_flashdata("pesan", "Username atau Password salah.");
                redirect("ControllerLogin");
            }
        }

    }

    public function logout(){
        $this->session->sess_destroy();
        redirect("controllerLogin");
    }
    
}
