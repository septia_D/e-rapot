<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ControllerTahunAjar extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->model('TahunAjarModel');
        $this->load->library('form_validation');
        if(empty($this->session->session_login['username'])){
            $this->session->set_flashdata("pesan", "Anda harus login terlebih dahulu.");
            redirect(site_url("controllerLogin"));
        }
    }

	public function index()
	{   
        $data['tahunAjar'] = $this->TahunAjarModel->get_all();
        $this->load->view('header');
        $this->load->view('tahun_ajar/listTahunAjar', $data);
        $this->load->view('footer');
    }

    public function insert_tahunAjar()
    {
        $data = [
            'action'            => site_url("controllerTahunAjar/insert_tahunAjar_action"),         
            'kode_tahunajar'    => set_value("kode_tahunajar"),
            'nama_tahunajar'    => set_value("nama_tahunajar"),
            'aktif'             => set_value("aktif")
        ];
        $this->load->view('header');
        $this->load->view('tahun_ajar/formTahunAjar', $data);
        $this->load->view('footer');
    }

    public function insert_tahunAjar_action()
    {
        $this->form_validation->set_rules('nama_tahunajar', 'Tahun Ajar', 'required');
        $this->form_validation->set_rules('aktif', 'Aktif', 'required');
        $this->form_validation->set_message('required', '* {field} Harus diisi');

        if ($this->form_validation->run() == FALSE){       
            $this->insert_tahunAjar_action();
        }else{
            $nama_tahunajar = $this->input->post("nama_tahunajar");
            $aktif          = $this->input->post("aktif");

            $data = [
                'nama_tahunajar' => $nama_tahunajar,
                'aktif'          => $aktif
            ];

            $this->TahunAjarModel->insert_tahunAjar($data);
            $this->session->set_flashdata("flash_message", "Berhasil tambah data tahun ajar.");
            redirect(site_url("controllerTahunAjar"));
        }
    }

    public function edit_TahunAjar($kode_tahunajar)
    {
        $data_tahunajar = $this->TahunAjarModel->get_by_id($kode_tahunajar);
        if($data_tahunajar){
            $data = [
                'action'         => site_url("controllerTahunAjar/edit_semester_action"),
                'kode_tahunajar' => set_value("kode_tahunajar", $data_tahunajar->kode_tahunajar),
                'nama_tahunajar' => set_value("nama_semester", $data_tahunajar->nama_tahunajar),
                'aktif'          => set_value("aktif", $data_tahunajar->aktif)
            ];
            $this->load->view("header");
            $this->load->view('tahun_ajar/formTahunAjar', $data);
            $this->load->view("footer");
        } else {
            redirect(site_url("controllerTahunAjar"));
        }
    }

    public function edit_semester_action()
    {
        $this->form_validation->set_rules('nama_tahunajar', 'Tahun Ajar', 'required');
        $this->form_validation->set_rules('aktif', 'Aktif', 'required');
        $this->form_validation->set_message('required', '* {field} Harus diisi');

        if ($this->form_validation->run() == FALSE){       
            $this->insert_tahunAjar_action();
        }else{
            $kode_tahunajar = $this->input->post("kode_tahunajar");
            $nama_tahunajar = $this->input->post("nama_tahunajar");
            $aktif          = $this->input->post("aktif");

            $data = [
                'nama_tahunajar' => $nama_tahunajar,
                'aktif'          => $aktif
            ];

            $this->TahunAjarModel->update_tahunAjar($kode_tahunajar, $data);
            $this->session->set_flashdata("flash_message", "Berhasil edit data tahun ajar.");

            redirect(site_url("controllerTahunAjar"));
        }
    }

    public function hapus_tahunAjar($kode_tahunajar)
    {
        $data_tahunAjar = $this->TahunAjarModel->get_by_id($kode_tahunajar);
        if($data_tahunAjar){
            $this->TahunAjarModel->delete_tahunAjar($kode_tahunajar);
            $this->session->set_flashdata("flash_message", "Berhasil hapus data tahun ajar.");
            redirect(site_url("controllerTahunAjar"));
        }else{
            $this->session->set_flashdata("error_message", "Gagal hapus data tahun ajar.");
            redirect(site_url("controllerTahunAjar"));
        }
    }
    
}
