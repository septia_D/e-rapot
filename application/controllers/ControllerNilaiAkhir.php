<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ControllerNilaiAkhir extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('NilaiAKhirModel');
        $this->load->library('form_validation');
        $this->load->library('Datatables');
        if (empty($this->session->session_login['username'])) {
            $this->session->set_flashdata("pesan", "Anda harus login terlebih dahulu.");
            redirect(site_url("controllerLogin"));
        }
    }

    public function json()
    {
        $kode_mapel = $this->input->post("kode_mapel");
        header('Content-Type: application/json');
        echo $this->NilaiAKhirModel->json($kode_mapel);
    }

    public function index()
    {
        $data['mata_pelajaran'] = $this->NilaiAKhirModel->get_all_mapel();
        $this->load->view('header');
        $this->load->view('nilai_akhir/listNilaiAkhir', $data);
        $this->load->view('footer');
    }

    public function atur_nilai_akhir($kode_kelas, $kode_mapel)
    {
        $get_kelas  = $this->NilaiAKhirModel->get_by_id_kelas($kode_kelas);
        $get_mapel  = $this->NilaiAKhirModel->get_by_id_mapel($kode_mapel);
        $data = [
            'mapel'      => $get_mapel->nama_mapel,
            'kode_mapel' => $get_mapel->kode_mapel,
            'tingkat'    => $get_mapel->tingkat,
            'rombel'     => $get_kelas->rombel,
            'semester'   => $get_kelas->semester,
            'tahun_ajar' => $get_kelas->tahun_ajar,
            'kode_kelas' => $kode_kelas
        ];

        $this->load->view("header");
        $this->load->view('nilai_akhir/formNilaiAkhir', $data);
        $this->load->view("footer");
    }

    public function json_input_nilai()
    {
        $kode_kelas = $this->input->post('kode_kelas');
        $kode_mapel = $this->input->post('kode_mapel');
        header('Content-Type: application/json');
        echo $this->NilaiAKhirModel->json_input_nilai($kode_kelas, $kode_mapel);
    }

    public function insert_nilai_harian()
    {
        $id_siswa = $this->input->post('id_siswa');
        $id_kelas = $this->input->post('id_kelas');
        $id_mapel = $this->input->post('id_mapel');
        $ph1      = $this->input->post('ph1');
        $ph2      = $this->input->post('ph2');
        $ph3      = $this->input->post('ph3');
        $ph4      = $this->input->post('ph4');
        $ph5      = $this->input->post('ph5');
        $ph6      = $this->input->post('ph6');

        if (!empty($ph1)) {
            $data = [
                'id_siswa'  => $id_siswa,
                'id_kelas'  => $id_kelas,
                'id_mapel'  => $id_mapel,
                'uh1'       => $ph1,
            ];
        }

        if (!empty($ph2)) {
            $data = [
                'id_siswa'  => $id_siswa,
                'id_kelas'  => $id_kelas,
                'id_mapel'  => $id_mapel,
                'uh2'       => $ph2,
            ];
        }

        if (!empty($ph3)) {
            $data = [
                'id_siswa'  => $id_siswa,
                'id_kelas'  => $id_kelas,
                'id_mapel'  => $id_mapel,
                'uh3'       => $ph3,
            ];
        }

        if (!empty($ph4)) {
            $data = [
                'id_siswa'  => $id_siswa,
                'id_kelas'  => $id_kelas,
                'id_mapel'  => $id_mapel,
                'uh4'       => $ph4,
            ];
        }

        if (!empty($ph5)) {
            $data = [
                'id_siswa'  => $id_siswa,
                'id_kelas'  => $id_kelas,
                'id_mapel'  => $id_mapel,
                'uh5'       => $ph5,
            ];
        }

        if (!empty($ph6)) {
            $data = [
                'id_siswa'  => $id_siswa,
                'id_kelas'  => $id_kelas,
                'id_mapel'  => $id_mapel,
                'uh6'       => $ph6,
            ];
        }

        $cek_siswa_nilai = $this->NilaiHarianModel->cek_siswa_nilai($id_siswa, $id_kelas, $id_mapel)->num_rows();

        if ($cek_siswa_nilai == 0) {
            $this->db->insert("nilai", $data);
        } else {
            $get_nilai = $this->NilaiHarianModel->cek_siswa_nilai($id_siswa, $id_kelas, $id_mapel)->row();
            $id = $get_nilai->id_nilai;
            $this->NilaiHarianModel->update($id, $data);
        }
        $result = [
            'info' => 'sukses'
        ];
        echo json_encode($result);
    }

    public function get_nilai_harian()
    {
        $get_nilai = $this->NilaiHarianModel->cek_siswa_nilai_mapel()->result();
        echo json_encode($get_nilai);
    }

    public function insert_nilai_akhir_action()
    {

        $this->form_validation->set_rules('uas', ' ', 'trim|required');
        $this->form_validation->set_rules('deskripsi_nilai_akhir', ' ', 'trim|required');

        $this->form_validation->set_message('required', '* {field} harus diisi ');
        $this->form_validation->set_error_delimiters('<i><span class="text-danger">', '</span></i>');

        if ($this->form_validation->run() == FALSE) {
            $result_error = [
                'status_alert'                    => 'error',
                'error_uas'                     => form_error('uas'),
                'error_deskripsi_nilai_akhir'     => form_error('deskripsi_nilai_akhir'),
            ];

            echo json_encode($result_error);
        } else {
            $id_siswa = $this->input->post('id_siswa');
            $id_kelas = $this->input->post('kode_kelas');
            $id_mapel = $this->input->post('kode_mapel');
            $id_nilai = $this->input->post('id_nilai');
            $deskripsi_nilai_akhir      = $this->input->post('deskripsi_nilai_akhir');

            $get_nilai = $this->NilaiAKhirModel->get_by_id_nilai($id_nilai);
            if ($get_nilai) {
                $uh1 = intval($get_nilai->uh1);
                $uh2 = intval($get_nilai->uh2);
                $uh3 = intval($get_nilai->uh3);
                $uh4 = intval($get_nilai->uh4);
                $uh5 = intval($get_nilai->uh5);
                $uh6 = intval($get_nilai->uh6);
                $uts = intval($get_nilai->uts);

                $rph   = round(($uh1 + $uh2 + $uh3 + $uh4 + $uh5 + $uh6 + $uts) / 7);
                $uas   = intval($this->input->post('uas'));

                $get_bobot = $this->NilaiAKhirModel->get_by_id_bobot($id_mapel);
                if ($get_bobot) {
                    $bobot_rph = ($get_bobot->bobot_rph / 100) * $rph;
                    $bobor_pat = ($get_bobot->bobot_pat / 100) * $uas;
                    $nilai_akhir = round($bobor_pat + $bobot_rph);

                    $kkm = intval($get_bobot->kkm);
                    $hitung_kkm = 100 - $kkm;
                    $mod = $hitung_kkm / 3;
                    $predikat_b = $kkm + $mod;
                    $predikat_a = $predikat_b + $mod;

                    if ($nilai_akhir >= $predikat_a && $nilai_akhir <= 100) {
                        $predikat_akhir = 'A';
                    } else if ($nilai_akhir >= $predikat_b && $nilai_akhir < $predikat_a) {
                        $predikat_akhir = 'B';
                    } else if ($nilai_akhir >= $kkm && $nilai_akhir < $predikat_b) {
                        $predikat_akhir = 'C';
                    } else if ($nilai_akhir < $kkm && $nilai_akhir >= 0) {
                        $predikat_akhir = 'D';
                    } else {
                        $predikat_akhir = '-';
                    }

                    $data = [
                        'uas'                   => $uas,
                        'rph'                   => $rph,
                        'nilai_akhir'           => $nilai_akhir,
                        'predikat_akhir'        => $predikat_akhir,
                        'deskripsi_nilai_akhir' => $deskripsi_nilai_akhir
                    ];

                    // print_r($data);die;
                    $this->NilaiAKhirModel->update($id_nilai, $data);

                    $result = [
                        'status_alert' => 'sukses'
                    ];

                    echo json_encode($result);
                } else {
                    $result = [
                        'status_alert' => 'validasi_bobot'
                    ];
    
                    echo json_encode($result);
                }
            } else {

                $result = [
                    'status_alert' => 'gagal'
                ];

                echo json_encode($result);
            }
        }
    }

    public function hitung_ulang_nilai()
    {
        $id_nilai = $this->input->post("id_nilai");
        $id_mapel = $this->input->post('id_mapel');

        $get_nilai = $this->NilaiAKhirModel->get_by_id_nilai($id_nilai);

        if ($get_nilai) {
            $uh1 = intval($get_nilai->uh1);
            $uh2 = intval($get_nilai->uh2);
            $uh3 = intval($get_nilai->uh3);
            $uh4 = intval($get_nilai->uh4);
            $uh5 = intval($get_nilai->uh5);
            $uh6 = intval($get_nilai->uh6);
            $uts = intval($get_nilai->uts);

            $rph   = round(($uh1 + $uh2 + $uh3 + $uh4 + $uh5 + $uh6 + $uts) / 7);
            $uas   = intval($get_nilai->uas);

            $get_bobot = $this->NilaiAKhirModel->get_by_id_bobot($id_mapel);
            $bobot_rph = ($get_bobot->bobot_rph / 100) * $rph;
            $bobor_pat = ($get_bobot->bobot_pat / 100) * $uas;
            $nilai_akhir = round($bobor_pat + $bobot_rph);

            $kkm = intval($get_bobot->kkm);
            $hitung_kkm = 100 - $kkm;
            $mod = $hitung_kkm / 3;
            $predikat_b = $kkm + $mod;
            $predikat_a = $predikat_b + $mod;

            if ($nilai_akhir >= $predikat_a && $nilai_akhir <= 100) {
                $predikat_akhir = 'A';
            } else if ($nilai_akhir >= $predikat_b && $nilai_akhir < $predikat_a) {
                $predikat_akhir = 'B';
            } else if ($nilai_akhir >= $kkm && $nilai_akhir < $predikat_b) {
                $predikat_akhir = 'C';
            } else if ($nilai_akhir < $kkm && $nilai_akhir >= 0) {
                $predikat_akhir = 'D';
            } else {
                $predikat_akhir = '-';
            }

            $data = [
                'rph'                   => $rph,
                'nilai_akhir'           => $nilai_akhir,
                'predikat_akhir'        => $predikat_akhir,
            ];
            $this->NilaiAKhirModel->update($id_nilai, $data);

            $result = [
                'status_alert' => 'sukses'
            ];

            echo json_encode($result);
        } else {

            $result = [
                'status_alert' => 'gagal'
            ];

            echo json_encode($result);
        }
    }
}
