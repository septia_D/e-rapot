<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ControllerNilaiHarian extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('NilaiHarianModel');
        $this->load->library('form_validation');
        $this->load->library('Datatables');
        if (empty($this->session->session_login['username'])) {
            $this->session->set_flashdata("pesan", "Anda harus login terlebih dahulu.");
            redirect(site_url("controllerLogin"));
        }
    }

    public function json()
    {
        $kode_mapel = $this->input->post("kode_mapel");
        header('Content-Type: application/json');
        echo $this->NilaiHarianModel->json($kode_mapel);
    }

    public function index()
    {
        $data['mata_pelajaran'] = $this->NilaiHarianModel->get_all_mapel();
        $this->load->view('header');
        $this->load->view('nilai_harian/listNilaiHarian', $data);
        $this->load->view('footer');
    }

    public function atur_nilai_harian($kode_kelas, $kode_mapel)
    {
        $get_kelas  = $this->NilaiHarianModel->get_by_id_kelas($kode_kelas);
        $get_mapel  = $this->NilaiHarianModel->get_by_id_mapel($kode_mapel);
        $data = [
            'mapel'      => $get_mapel->nama_mapel,
            'kode_mapel' => $get_mapel->kode_mapel,
            'tingkat'    => $get_mapel->tingkat,
            'rombel'     => $get_kelas->rombel,
            'semester'   => $get_kelas->semester,
            'tahun_ajar' => $get_kelas->tahun_ajar,
            'kode_kelas' => $kode_kelas
        ];

        $this->load->view("header");
        $this->load->view('nilai_harian/formNilaiHarian', $data);
        $this->load->view("footer");
    }

    public function json_input_nilai()
    {
        $kode_kelas = $this->input->post('kode_kelas');
        $kode_mapel = $this->input->post('kode_mapel');
        header('Content-Type: application/json');
        echo $this->NilaiHarianModel->json_input_nilai($kode_kelas, $kode_mapel);
    }

    public function insert_nilai_harian()
    {
        $id_siswa = $this->input->post('id_siswa');
        $id_kelas = $this->input->post('id_kelas');
        $id_mapel = $this->input->post('id_mapel');
        $ph1      = $this->input->post('ph1');
        $ph2      = $this->input->post('ph2');
        $ph3      = $this->input->post('ph3');
        $ph4      = $this->input->post('ph4');
        $ph5      = $this->input->post('ph5');
        $ph6      = $this->input->post('ph6');
        $uts      = $this->input->post('uts');

        if(!empty($ph1)){
            $data = [
                'id_siswa'  => $id_siswa,
                'id_kelas'  => $id_kelas,
                'id_mapel'  => $id_mapel,
                'uh1'       => $ph1,
            ];
        }
        
        if(!empty($ph2)){
            $data = [
                'id_siswa'  => $id_siswa,
                'id_kelas'  => $id_kelas,
                'id_mapel'  => $id_mapel,
                'uh2'       => $ph2,
            ];
        }

        if(!empty($ph3)){
            $data = [
                'id_siswa'  => $id_siswa,
                'id_kelas'  => $id_kelas,
                'id_mapel'  => $id_mapel,
                'uh3'       => $ph3,
            ];
        }

        if(!empty($ph4)){
            $data = [
                'id_siswa'  => $id_siswa,
                'id_kelas'  => $id_kelas,
                'id_mapel'  => $id_mapel,
                'uh4'       => $ph4,
            ];
        }

        if(!empty($ph5)){
            $data = [
                'id_siswa'  => $id_siswa,
                'id_kelas'  => $id_kelas,
                'id_mapel'  => $id_mapel,
                'uh5'       => $ph5,
            ];
        }

        if(!empty($ph6)){
            $data = [
                'id_siswa'  => $id_siswa,
                'id_kelas'  => $id_kelas,
                'id_mapel'  => $id_mapel,
                'uh6'       => $ph6,
            ];
        }

        if(!empty($uts)){
            $data = [
                'id_siswa'  => $id_siswa,
                'id_kelas'  => $id_kelas,
                'id_mapel'  => $id_mapel,
                'uts'       => $uts,
            ];
        }

        $cek_siswa_nilai = $this->NilaiHarianModel->cek_siswa_nilai($id_siswa, $id_kelas, $id_mapel)->num_rows();

        if($cek_siswa_nilai == 0){
            $this->db->insert("nilai", $data);
        } else {
            $get_nilai = $this->NilaiHarianModel->cek_siswa_nilai($id_siswa, $id_kelas, $id_mapel)->row();
            $id = $get_nilai->id_nilai;
            $this->NilaiHarianModel->update($id, $data);
        }
        $result = [
            'info' => 'sukses'
        ];
        echo json_encode($result);
    }

    public function get_nilai_harian()
    {
        $get_nilai = $this->NilaiHarianModel->cek_siswa_nilai_mapel()->result();
        echo json_encode($get_nilai);
    }
}
