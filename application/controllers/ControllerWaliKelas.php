<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ControllerWaliKelas extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->model('WaliMuridModel');
        $this->load->library('Datatables');
        $this->load->library('Pdf_new');
        if (empty($this->session->session_login['username'])) {
            $this->session->set_flashdata("pesan", "Anda harus login terlebih dahulu.");
            redirect(site_url("controllerLogin"));
        }
    }

    public function index()
    {
        $data['kelas'] = $this->WaliMuridModel->get_all_kelas();
        $this->load->view('header');
        $this->load->view('wali_kelas/dashboardWaliKelas', $data);
        $this->load->view('footer');
    }

    public function json_siswa()
    {
        header('Content-Type: application/json');
        echo $this->WaliMuridModel->json_siswa();
    }

    public function json_rapor()
    {
        header('Content-Type: application/json');
        echo $this->WaliMuridModel->json_rapor();
    }

    public function json_ranking()
    {
        $id_siswa = $this->input->post('id_siswa');
        $id_kelas = $this->input->post('id_kelas');
        header('Content-Type: application/json');
        echo $this->WaliMuridModel->json_ranking();
    }

    public function lihat_nilai($id_siswa, $id_kelas)
    {
        $get_siswa = $this->WaliMuridModel->get_by_id_siswa($id_siswa);
        $get_nilai = $this->WaliMuridModel->get_all_nilai($id_siswa, $id_kelas);

        $data = [
            'nis'           => $get_siswa->nis,
            'nama_lengkap'  => $get_siswa->nama_lengkap,
            'nilai'         => $get_nilai
        ];

        $this->load->view('header');
        $this->load->view('wali_kelas/lihatNilai', $data);
        $this->load->view('footer');
    }

    public function profil_siswa($nis)
    {
        $data['siswa'] = $this->WaliMuridModel->get_by_id_siswa($nis);
        $this->load->view('header');
        $this->load->view('wali_kelas/profilSiswa', $data);
        $this->load->view('footer');
    }

    public function input_catatan($id_siswa, $id_kelas)
    {
        $get_siswa = $this->WaliMuridModel->get_by_id_siswa($id_siswa);
        $cek_nilai_catatan = $this->WaliMuridModel->get_by_id($id_siswa, $id_kelas);
        if (empty($cek_nilai_catatan)) {
            $data_catatan = [
                'id_siswa' => $id_siswa,
                'id_kelas' => $id_kelas
            ];

            $this->db->insert("catatan_siswa", $data_catatan);
            redirect('controllerWaliKelas/input_catatan/' . $id_siswa . "/" . $id_kelas);
        } else {
            $data = [
                'nis'           => $get_siswa->nis,
                'nama_lengkap'  => $get_siswa->nama_lengkap,
                'kode_kelas'    => $id_kelas,
                'listEkstra'    => $this->WaliMuridModel->get_all_ekstra(),
                'sikap_spiritual_predikat'  => set_value('sikap_spiritual_predikat', $cek_nilai_catatan->sikap_spiritual_predikat),
                'sikap_spiritual_deskripsi' => set_value('sikap_spiritual_deskripsi', $cek_nilai_catatan->sikap_spiritual_deskripsi),
                'sikap_sosial_predikat'     => set_value('sikap_sosial_predikat', $cek_nilai_catatan->sikap_sosial_predikat),
                'sikap_sosial_deskripsi'    => set_value('sikap_sosial_deskripsi', $cek_nilai_catatan->sikap_sosial_deskripsi),
                'sakit'                     => set_value('sakit', $cek_nilai_catatan->sakit),
                'izin'                      => set_value('izin', $cek_nilai_catatan->izin),
                'tanpa_keterangan'          => set_value('tanpa_keterangan', $cek_nilai_catatan->tanpa_keterangan),
                'status_siswa'              => set_value('status_siswa', $cek_nilai_catatan->status_siswa),
                'catatan_wali'             => set_value('catatan_wali', $cek_nilai_catatan->catatan_wali),
            ];
        }


        $this->load->view('header');
        $this->load->view('wali_kelas/inputCatatan', $data);
        $this->load->view('footer');
    }

    public function json_ekstra()
    {
        $id_siswa = $this->input->post('id_siswa');
        $id_kelas = $this->input->post('id_kelas');
        header('Content-Type: application/json');
        echo $this->WaliMuridModel->json_ekstra($id_siswa, $id_kelas);
    }

    public function json_prestasi()
    {
        $id_siswa = $this->input->post('id_siswa');
        $id_kelas = $this->input->post('id_kelas');
        header('Content-Type: application/json');
        echo $this->WaliMuridModel->json_prestasi($id_siswa, $id_kelas);
    }

    public function update_catatan_wali()
    {
        $id_siswa = $this->input->post("id_siswa");
        $id_kelas = $this->input->post("id_kelas");
        $cek_nilai_catatan = $this->WaliMuridModel->get_by_id($id_siswa, $id_kelas);

        if (empty($cek_nilai_catatan)) {
            $data = [
                'sikap_spiritual_predikat'  => $this->input->post("sikap_spiritual_predikat"),
                'sikap_spiritual_deskripsi' => $this->input->post("sikap_spiritual_deskripsi"),
                'sikap_sosial_predikat'     => $this->input->post("sikap_sosial_predikat"),
                'sikap_sosial_deskripsi'    => $this->input->post("sikap_sosial_deskripsi"),
                'sakit'                     => $this->input->post("sakit"),
                'izin'                      => $this->input->post("izin"),
                'tanpa_keterangan'          => $this->input->post("tanpa_keterangan"),
                'status_siswa'              => $this->input->post("status_siswa"),
                'catatan_wali'              => $this->input->post("catatan_wali"),
            ];

            $this->db->insert("catatan_siswa", $data);
        } else {
            $data = [
                'sikap_spiritual_predikat'  => $this->input->post("sikap_spiritual_predikat"),
                'sikap_spiritual_deskripsi' => $this->input->post("sikap_spiritual_deskripsi"),
                'sikap_sosial_predikat'     => $this->input->post("sikap_sosial_predikat"),
                'sikap_sosial_deskripsi'    => $this->input->post("sikap_sosial_deskripsi"),
                'sakit'                     => $this->input->post("sakit"),
                'izin'                      => $this->input->post("izin"),
                'tanpa_keterangan'          => $this->input->post("tanpa_keterangan"),
                'status_siswa'              => $this->input->post("status_siswa"),
                'catatan_wali'              => $this->input->post("catatan_wali"),
            ];

            $this->WaliMuridModel->update_catatan($id_siswa, $id_kelas, $data);
        }
    }

    function insert_catatan_wali()
    {
        $id_siswa = $this->input->post("id_siswa");
        $id_kelas = $this->input->post("id_kelas");

        $data = [
            'sikap_spiritual_predikat'  => $this->input->post("sikap_spiritual_predikat"),
            'sikap_spiritual_deskripsi' => $this->input->post("sikap_spiritual_deskripsi"),
            'sikap_sosial_predikat'     => $this->input->post("sikap_sosial_predikat"),
            'sikap_sosial_deskripsi'    => $this->input->post("sikap_sosial_deskripsi"),
            'sakit'                     => $this->input->post("sakit"),
            'izin'                      => $this->input->post("izin"),
            'tanpa_keterangan'          => $this->input->post("tanpa_keterangan"),
            'status_siswa'              => $this->input->post("status_siswa"),
            'catatan_wali'              => $this->input->post("catatan_wali"),
        ];

        $this->WaliMuridModel->update_catatan($id_siswa, $id_kelas, $data);

        $result = [
            'status' => 'sukses'
        ];

        echo json_encode($result);
    }

    public function insert_nilai_ekstra()
    {
        $id_siswa           = $this->input->post("id_siswa");
        $id_kelas           = $this->input->post("id_kelas");
        $kode_ekstra        = $this->input->post("kode_ekstra");
        $predikat_ekstra    = $this->input->post("predikat_ekstra");
        $ekstra_deskripsi   = $this->input->post("ekstra_deskripsi");

        $data = [
            'id_siswa'          => $id_siswa,
            'id_kelas'          => $id_kelas,
            'kode_ekstra'       => $kode_ekstra,
            'predikat'          => $predikat_ekstra,
            'deskripsi'         => $ekstra_deskripsi,
        ];

        $this->db->insert("nilai_ekstra", $data);

        $result = [
            "status" => "sukses"
        ];

        echo json_encode($result);
    }

    public function hapus_nilai_ekstra($id)
    {
        $get_nilai_ekstra = $this->WaliMuridModel->cek_nilai_ekstra($id);
        if ($get_nilai_ekstra) {
            $this->WaliMuridModel->delete_nilai_ekstra($id);
            redirect('controllerWaliKelas/input_catatan/' . $get_nilai_ekstra->id_siswa . "/" . $get_nilai_ekstra->id_kelas);
        } else {
            $this->session->set_flashdata("error_message", "Gagal hapus.");
            redirect('controllerWaliKelas/input_catatan/' . $get_nilai_ekstra->id_siswa . "/" . $get_nilai_ekstra->id_kelas);
        }
    }

    public function insert_nilai_prestasi()
    {
        $id_siswa             = $this->input->post("id_siswa");
        $id_kelas             = $this->input->post("id_kelas");
        $nama_prestasi        = $this->input->post("nama_prestasi");
        $predikat_prestasi    = $this->input->post("predikat_prestasi");
        $prestasi_deskripsi   = $this->input->post("prestasi_deskripsi");

        $data = [
            'id_siswa'          => $id_siswa,
            'id_kelas'          => $id_kelas,
            'nama_prestasi'     => $nama_prestasi,
            'predikat'          => $predikat_prestasi,
            'deskripsi'         => $prestasi_deskripsi,
        ];

        $this->db->insert("prestasi", $data);

        $result = [
            "status" => "sukses"
        ];

        echo json_encode($result);
    }

    public function hapus_nilai_prestasi($id)
    {
        $get_nilai_prestasi = $this->WaliMuridModel->cek_nilai_prestasi($id);
        if ($get_nilai_prestasi) {
            $this->WaliMuridModel->delete_nilai_prestasi($id);
            redirect('controllerWaliKelas/input_catatan/' . $get_nilai_prestasi->id_siswa . "/" . $get_nilai_prestasi->id_kelas);
        } else {
            $this->session->set_flashdata("error_message", "Gagal hapus.");
            redirect('controllerWaliKelas/input_catatan/' . $get_nilai_prestasi->id_siswa . "/" . $get_nilai_prestasi->id_kelas);
        }
    }

    public function insert_ranking_siswa()
    {
        $id_siswa = $this->input->post("id_siswa");
        $id_kelas = $this->input->post("id_kelas");
        $ranking  = $this->input->post("ranking");

        $data = [
            'id_siswa'  => $id_siswa,
            'id_kelas'  => $id_kelas,
            'ranking'   => $ranking
        ];

        $cek_ranking = $this->WaliMuridModel->cek_ranking_siswa($id_siswa, $id_kelas);
        if (empty($cek_ranking)) {
            $this->db->insert("ranking_siswa", $data);
        } else {
            $data_update = [
                'ranking'   => $ranking
            ];
            $where = [
                'id_siswa'  => $id_siswa,
                'id_kelas'  => $id_kelas,
            ];

            $this->WaliMuridModel->update_ranking($where, $data_update);
        }

        $result = [
            "info" => "sukses"
        ];

        echo json_encode($result);
    }

    public function cetak_rapot_siswa($nis)
    {
        $data['get_data_siswa']   = $this->WaliMuridModel->get_by_id_siswa($nis);
        $data['get_data_kelas']   = $this->WaliMuridModel->get_by_id_kelas($nis);
        $id_siswa = $data['get_data_kelas']->nis;
        $id_kelas = $data['get_data_kelas']->kode_kelas;
        $tingkat  = $data['get_data_kelas']->tingkat;
        $data['get_catatan_siswa'] = $this->WaliMuridModel->get_by_catatan($id_siswa, $id_kelas);
        $data['mapel']             = $this->WaliMuridModel->get_all_mapel($tingkat, $id_siswa, $id_kelas);
        $data['jmlh_nilai']        = $this->WaliMuridModel->jumlah_nilai($id_siswa, $id_kelas);
        $data['get_ekstra']        = $this->WaliMuridModel->get_all_esktra($id_siswa, $id_kelas);
        $data['get_prestasi']      = $this->WaliMuridModel->get_all_prestasi($id_siswa, $id_kelas);
        // print_r($data['mapel']);die;
        $this->load->library('Pdf_new');
        $load_view   = 'wali_kelas/cetak_rapot_siswa';
        $size        = 'A4';
        $orientation = 'portrait';

        $this->pdf_new->load_view($load_view, $size, $orientation, $data);
        // DOM PDF
    }

    public function cek_status_catatan()
    {
        $id_siswa = $this->input->post('id_siswa');
        $id_kelas = $this->input->post('id_kelas');
        
        $cekStatusCatatan = $this->WaliMuridModel->cekStatusCatatan($id_siswa, $id_kelas);
        if(empty($cekStatusCatatan)){
            $result = [
                "status"    => "tidak ada"
            ];
        } else {
            $result = [
                "status"    => "ada"
            ];
        }
        echo json_encode($result);
    }
}
