<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ControllerHome extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('HomeModel');
        $this->load->library('form_validation');
        $this->load->library('upload');
        if (empty($this->session->session_login['username'])) {
            $this->session->set_flashdata("pesan", "Anda harus login terlebih dahulu.");
            redirect(site_url("controllerLogin"));
        }
    }

    public function index()
    {
        $nuptk = $this->session->session_login['id_guru'];
        $data['wali_kelas'] = $this->db->query("SELECT * FROM kelas WHERE kode_guru='$nuptk'")->row();
        $data['jumlah_guru'] = $this->HomeModel->jumlah_guru();
        $data['jumlah_ekstra'] = $this->HomeModel->jumlah_ekstra();
        $data['jumlah_siswa'] = $this->HomeModel->jumlah_siswa();
        $data['jumlah_rombel'] = $this->HomeModel->jumlah_rombel();
        $this->load->view("header");
        $this->load->view("dashboard", $data);
        $this->load->view("footer");
    }

    public function lihat_profil()
    {
        $nuptk = $this->session->session_login['id_guru'];
        $data['user'] = $this->HomeModel->get_by_id_user($nuptk);
        $this->load->view('header');
        $this->load->view('profilUser', $data);
        $this->load->view('footer');
    }

    public function edit_admin_action()
    {
        $id = $this->input->post('id_guru');

        // konfigurasi untuk melakukan upload photo
        $config['upload_path']   = './images/';    //path folder image
        $config['allowed_types'] = 'jpg|png|jpeg'; //type yang dapat diupload jpg|png|jpeg			
        $config['file_name']     = url_title($this->input->post('id_guru')); //nama file photo dirubah menjadi nama berdasarkan nim	
        $this->upload->initialize($config);

        // Jika file photo ada 
        if (!empty($_FILES['foto']['name'])) {
            if ($this->upload->do_upload('foto')) {
                $photo = $this->upload->data();
                $dataphoto = $photo['file_name'];
                $this->load->library('upload', $config);
                $data = [
                    "username"      => $this->input->post("username"),
                    "password"      => md5($this->input->post("password")),
                    'foto_admin'    => $dataphoto,
                ];
                $this->HomeModel->update_admin($id, $data);
            }
        } else {
            $data = [
                "username"  => $this->input->post("username"),
                "password"  => md5($this->input->post("password")),
            ];

            $this->HomeModel->update_admin($id, $data);
        }
        $this->session->set_flashdata("flash_message", "Berhasil update data admin.");
        redirect(site_url("controllerHome/lihat_profil"));
    }
}
