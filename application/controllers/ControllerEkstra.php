<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ControllerEkstra extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->model('EkstraModel');
        $this->load->library('form_validation');
        $this->load->library('Datatables');
        if(empty($this->session->session_login['username'])){
            $this->session->set_flashdata("pesan", "Anda harus login terlebih dahulu.");
            redirect(site_url("controllerLogin"));
        }
    }

    public function json()
    {
        header('Content-Type: application/json');
        echo $this->EkstraModel->json();
    }

	public function index()
	{   
        // $data['mata_pelajaran'] = $this->MataPelajaranModel->get_all();
        $this->load->view('header');
        $this->load->view('ekstra/listEkstra');
        $this->load->view('footer');
    }

    public function insert_ekstra()
    {
        $data = [
            'action'        => site_url("controllerEkstra/insert_ekstra_action"),         
            'kode_ekstra'   => set_value("kode_ekstra"),
            'nama_ekstra'   => set_value("nama_ekstra"),
        ];
        $this->load->view('header');
        $this->load->view('ekstra/formEkstra', $data);
        $this->load->view('footer');
    }

    public function insert_ekstra_action()
    {
        $this->form_validation->set_rules('nama_ekstra', 'Nama Ekstrakulikuler', 'required');
        $this->form_validation->set_message('required', '* {field} Harus diisi');

        if ($this->form_validation->run() == FALSE){       
            $this->insert_ekstra();
        }else{
            $nama_ekstra = $this->input->post("nama_ekstra");

            $data = [
                'nama_ekstra'    => $nama_ekstra,
            ];

            $this->EkstraModel->insert_Ekstra($data);
            $this->session->set_flashdata("flash_message", "Berhasil tambah data ekstrakulikuler.");
            redirect(site_url("controllerEkstra"));
        }
    }

    public function edit_ekstra($kode_ekstra)
    {
        $data_ekstra = $this->EkstraModel->get_by_id($kode_ekstra);
        if($data_ekstra){
            $data = [
                'action'        => site_url("controllerEkstra/edit_ekstra_action"),
                'kode_ekstra'   => set_value("kode_ekstra", $data_ekstra->kode_ekstra),
                'nama_ekstra'   => set_value("nama_ekstra", $data_ekstra->nama_ekstra),
            ];
            $this->load->view("header");
            $this->load->view('ekstra/formEkstra', $data);
            $this->load->view("footer");
        } else {
            $this->session->set_flashdata("error_message", "Gagal edit data ekstra.");
            redirect(site_url("controllerEkstra"));
        }
    }

    public function edit_ekstra_action()
    {
        $this->form_validation->set_rules('nama_ekstra', 'Nama Ekstrakulikuler', 'required');
        $this->form_validation->set_message('required', '* {field} Harus diisi');

        if ($this->form_validation->run() == FALSE){       
            $this->edit_ekstra($this->input->post("kode_ekstra"));
        }else{
            $kode_ekstra = $this->input->post("kode_ekstra");
            $nama_ekstra = $this->input->post("nama_ekstra");

            $data = [
                'nama_ekstra'    => $nama_ekstra,
            ];

            $this->EkstraModel->update_ekstra($kode_ekstra, $data);
            $this->session->set_flashdata("flash_message", "Berhasil edit data ekstra.");
            redirect(site_url("controllerEkstra"));
        }
    }

    public function hapus_ekstra($kode_ekstra)
    {
        $data_ekstra = $this->EkstraModel->get_by_id($kode_ekstra);
        if($data_ekstra){
            $this->EkstraModel->delete_ekstra($kode_ekstra);
            $this->session->set_flashdata("flash_message", "Berhasil hapus ekstra.");
            redirect(site_url("controllerEkstra"));
        }else{
            $this->session->set_flashdata("error_message", "Gagal hapus data ekstra.");
            redirect(site_url("controllerEkstra"));
        }
    }
    
}
