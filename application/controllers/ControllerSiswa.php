<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ControllerSiswa extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('SiswaModel');
        $this->load->model('RombelModel');
        $this->load->library('form_validation');
        $this->load->library('Datatables');
        $this->load->library('upload');
        $this->load->library(array('PHPExcel', 'PHPExcel/IOFactory'));
        $this->load->helper(array('form', 'url', 'download', 'file'));
        if (empty($this->session->session_login['username'])) {
            $this->session->set_flashdata("pesan", "Anda harus login terlebih dahulu.");
            redirect(site_url("controllerLogin"));
        }
    }

    public function json()
    {
        header('Content-Type: application/json');
        echo $this->SiswaModel->json();
    }

    public function index()
    {
        $data['rombel'] = $this->SiswaModel->get_all_rombel();
        $this->load->view('header');
        $this->load->view('siswa/listSiswa', $data);
        $this->load->view('footer');
    }

    public function unduh_template()
    {
        force_download('FORMAT_IMPORT_SISWA' . time() . '.xlsx', file_get_contents('assets/template_upload/FORMAT_IMPORT_SISWA.xlsx', NULL));
    }

    public function upload()
    {
        ini_set('max_execution_time', 10000); // or this way
        $fileName = $_FILES['file']['name'];

        $config['upload_path'] = './assets/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 100000;

        $this->load->library('upload');
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('file'))
            $this->upload->display_errors();
        $fileName = str_replace(' ', '_', $fileName);
        $media = $this->upload->data('file');
        $inputFileName = './assets/' . $fileName;
        //echo $inputFileName;
        try {
            $inputFileType = IOFactory::identify($inputFileName);
            $objReader = IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();  
        
        for ($row = 2; $row <= $highestRow; $row++) {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            if ($rowData[0][0] != NULL || $rowData[0][0] != '') {
                $nis = $rowData[0][0];
                $cek_data = $this->db->query("select * from siswa where nis='$nis'")->row();
                $data = array(
                    'nis'             => $rowData[0][0],
                    'nisn'            => ($rowData[0][1] == '' or $rowData[0][1] == NULL) ? "-" : $rowData[0][1],
                    'nama_lengkap'    => ($rowData[0][2] == '' or $rowData[0][2] == NULL) ? "-" : $rowData[0][2],
                    'tempat_lahir'    => ($rowData[0][3] == '' or $rowData[0][3] == NULL) ? "-" : $rowData[0][3],
                    'tanggal_lahir'   => ($rowData[0][4] == '' or $rowData[0][4] == NULL) ? date("Y-m-d") : date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($rowData[0][4])),
                    'jenis_kelamin'   => ($rowData[0][5] == '' or $rowData[0][5] == NULL) ? "-" : $rowData[0][5],
                    'agama'           => ($rowData[0][6] == '' or $rowData[0][6] == NULL) ? "-" : $rowData[0][6],
                    'alamat'          => ($rowData[0][7] == '' or $rowData[0][7] == NULL) ? "-" : $rowData[0][7],
                    'diterima'        => ($rowData[0][8] == '' or $rowData[0][8] == NULL) ? date("Y-m-d") : date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($rowData[0][8])),
                    'foto'            => ($rowData[0][9] == '' or $rowData[0][9] == NULL) ? "-" : $rowData[0][9],
                );

                if ($cek_data) {
                    $this->SiswaModel->update_siswa($nis, $data);
                } else {
                    $this->SiswaModel->insert_siswa($data);
                }
            }
        }
        $path=base_url('assets/'.$fileName);
        $this->load->helper("url");
        unlink(FCPATH . '/assets/' . $fileName);
        unlink($path);
        //echo "$path";
        $this->session->set_flashdata('flash_message', 'Berhasil Import Data Siswa dari excel');
        redirect(site_url('ControllerSiswa'));
    }

    public function insert_siswa()
    {
        $data = [
            'action'        => site_url("controllerSiswa/insert_siswa_action"),
            'nis'           => set_value("nis"),
            'nama_lengkap'  => set_value("nama_lengkap"),
            'tempat_lahir'  => set_value("tempat_lahir"),
            'tanggal_lahir' => set_value("tanggal_lahir"),
            'jenis_kelamin' => set_value("jenis_kelamin"),
            'agama'         => set_value("agama"),
            'alamat'        => set_value("alamat"),
            'diterima'      => set_value("diterima"),
            'foto'          => set_value('foto'),
            'nisn'          => set_value('nisn'),
        ];
        $this->load->view('header');
        $this->load->view('siswa/formSiswa', $data);
        $this->load->view('footer');
    }

    public function insert_siswa_action()
    {
        $this->form_validation->set_rules('nis', 'NIS', 'required');
        $this->form_validation->set_rules('nama_lengkap', 'Nama lengkap', 'required');
        $this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required');
        $this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required');
        $this->form_validation->set_rules('jenis_kelamin', 'Jenis kelamin', 'required');
        $this->form_validation->set_rules('agama', 'Agama', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('diterima', 'Tanggal diterima', 'required');
        $this->form_validation->set_message('required', '* {field} Harus diisi');

        if ($this->form_validation->run() == FALSE) {
            $this->insert_siswa();
        } else {

            $cek_nis_siswa = $this->SiswaModel->get_by_id($this->input->post("nis"));

            if ($cek_nis_siswa) {
                $this->session->set_flashdata("error_message", "Gagal tambah siswa. NIS sudah ada, atas nama " . $cek_nis_siswa->nama_lengkap);
                redirect(site_url("controllerSiswa"));
            }

            // konfigurasi untuk melakukan upload photo
            $config['upload_path']   = './images/';    //path folder image
            $config['allowed_types'] = 'jpg|png|jpeg'; //type yang dapat diupload jpg|png|jpeg			
            $config['file_name']     = url_title($this->input->post('nis')); //nama file photo dirubah menjadi nama berdasarkan nim	
            $this->upload->initialize($config);

            // Jika file photo ada 
            if (!empty($_FILES['foto']['name'])) {
                if ($this->upload->do_upload('foto')) {
                    $photo = $this->upload->data();
                    $dataphoto = $photo['file_name'];
                    $this->load->library('upload', $config);
                    $data = [
                        'nis'             => $this->input->post("nis"),
                        'nisn'            => $this->input->post("nisn"),
                        'nama_lengkap'    => $this->input->post("nama_lengkap"),
                        'tempat_lahir'    => $this->input->post("tempat_lahir"),
                        'tanggal_lahir'   => date('Y-m-d', strtotime($this->input->post("tanggal_lahir"))),
                        'jenis_kelamin'   => $this->input->post("jenis_kelamin"),
                        'agama'           => $this->input->post("agama"),
                        'alamat'          => $this->input->post("alamat"),
                        'diterima'        => date('Y-m-d', strtotime($this->input->post("diterima"))),
                        'foto'                 => $dataphoto,
                    ];

                    $this->SiswaModel->insert_siswa($data);
                }
                $this->session->set_flashdata("flash_message", "Berhasil tambah data siswa.");
                redirect(site_url("controllerSiswa"));
            } else {
                $data = [
                    'nis'             => $this->input->post("nis"),
                    'nisn'            => $this->input->post("nisn"),
                    'nama_lengkap'    => $this->input->post("nama_lengkap"),
                    'tempat_lahir'    => $this->input->post("tempat_lahir"),
                    'tanggal_lahir'   => date('Y-m-d', strtotime($this->input->post("tanggal_lahir"))),
                    'jenis_kelamin'   => $this->input->post("jenis_kelamin"),
                    'agama'           => $this->input->post("agama"),
                    'alamat'          => $this->input->post("alamat"),
                    'diterima'        => date('Y-m-d', strtotime($this->input->post("diterima"))),
                ];

                $this->SiswaModel->insert_siswa($data);
            }

            $this->session->set_flashdata("flash_message", "Berhasil tambah data siswa.");
            redirect(site_url("controllerSiswa"));
        }
    }

    public function atur_kelas_siswa()
    {
        $nis = $this->input->post('nis');
        $kode_kelas = $this->input->post('kode_kelas');

        $data = [
            'nis' => $nis,
            'kode_kelas' => $kode_kelas
        ];

        $data_update = [
            'kode_kelas' => $kode_kelas
        ];

        $cek_data_kelas = $this->RombelModel->get_by_id_siswa($nis);
        if ($cek_data_kelas) {
            $this->SiswaModel->update_kelas_siswa($nis, $data_update);
        } else {
            $this->SiswaModel->insert_kelas_siswa($data);
        }
        $this->session->set_flashdata("flash_message", "Berhasil tambah kelas.");
        redirect(site_url("controllerSiswa"));
    }

    public function lihat_siswa($nis)
    {
        $data['siswa'] = $this->SiswaModel->get_by_id($nis);
        $this->load->view('header');
        $this->load->view('siswa/profilSiswa', $data);
        $this->load->view('footer');
    }

    public function edit_siswa_action()
    {
        $nis  = $this->input->post("nis");

        // konfigurasi untuk melakukan upload photo
        $config['upload_path']   = './images/';    //path folder image
        $config['allowed_types'] = 'jpg|png|jpeg'; //type yang dapat diupload jpg|png|jpeg			
        $config['file_name']     = url_title($this->input->post('nis')); //nama file photo dirubah menjadi nama berdasarkan nim	
        $this->upload->initialize($config);

        // Jika file photo ada 
        if (!empty($_FILES['foto']['name'])) {
            if ($this->upload->do_upload('foto')) {
                $photo = $this->upload->data();
                $dataphoto = $photo['file_name'];
                $this->load->library('upload', $config);
                $data = [
                    'nis'             => $this->input->post("nis"),
                    'nisn'            => $this->input->post("nisn"),
                    'nama_lengkap'    => $this->input->post("nama_lengkap"),
                    'tempat_lahir'    => $this->input->post("tempat_lahir"),
                    'tanggal_lahir'   => date('Y-m-d', strtotime($this->input->post("tanggal_lahir"))),
                    'jenis_kelamin'   => $this->input->post("jenis_kelamin"),
                    'agama'           => $this->input->post("agama"),
                    'alamat'          => $this->input->post("alamat"),
                    'diterima'        => date('Y-m-d', strtotime($this->input->post("diterima"))),
                    'foto'                 => $dataphoto,
                ];

                $this->SiswaModel->update_siswa($nis, $data);
            }
            $this->session->set_flashdata("flash_message", "Berhasil update data siswa.");
            redirect(site_url("controllerSiswa/lihat_siswa/" . $nis));
        } else {
            $data = [
                'nisn'            => $this->input->post("nisn"),
                'nama_lengkap'    => $this->input->post("nama_lengkap"),
                'tempat_lahir'    => $this->input->post("tempat_lahir"),
                'tanggal_lahir'   => date('Y-m-d', strtotime($this->input->post("tanggal_lahir"))),
                'jenis_kelamin'   => $this->input->post("jenis_kelamin"),
                'agama'           => $this->input->post("agama"),
                'alamat'          => $this->input->post("alamat"),
                'diterima'        => date('Y-m-d', strtotime($this->input->post("diterima"))),
            ];

            $this->SiswaModel->update_siswa($nis, $data);
        }

        $this->session->set_flashdata("flash_message", "Berhasil update data siswa.");
        redirect(site_url("controllerSiswa/lihat_siswa/" . $nis));
    }

    public function hapus_siswa($nis)
    {
        $data_siswa = $this->SiswaModel->get_by_id($nis);
        if ($data_siswa) {
            $this->SiswaModel->delete_siswa($nis);
            $this->session->set_flashdata("flash_message", "Berhasil hapus data siswa.");
            redirect(site_url("controllerSiswa"));
        } else {
            $this->session->set_flashdata("error_message", "Gagal hapus data siswa.");
            redirect(site_url("controllerSiswa"));
        }
    }
}
