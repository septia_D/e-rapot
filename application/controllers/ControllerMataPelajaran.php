<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ControllerMataPelajaran extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('MataPelajaranModel');
        $this->load->library('form_validation');
        $this->load->library('Datatables');
        if (empty($this->session->session_login['username'])) {
            $this->session->set_flashdata("pesan", "Anda harus login terlebih dahulu.");
            redirect(site_url("controllerLogin"));
        }
    }

    public function json()
    {
        header('Content-Type: application/json');
        echo $this->MataPelajaranModel->json();
    }

    public function index()
    {
        // $data['mata_pelajaran'] = $this->MataPelajaranModel->get_all();
        $this->load->view('header');
        $this->load->view('mata_pelajaran/listMataPelajaran');
        $this->load->view('footer');
    }

    public function insert_mataPelajaran()
    {
        $data = [
            'action'        => site_url("controllerMataPelajaran/insert_mataPelajaran_action"),
            'kode_mapel'    => set_value("kode_mapel"),
            'tingkat'       => set_value("tingkat"),
            'nama_mapel'    => set_value("nama_mapel"),
            'deskripsi'     => set_value("deskripsi"),
        ];
        $this->load->view('header');
        $this->load->view('mata_pelajaran/formMataPelajaran', $data);
        $this->load->view('footer');
    }

    public function insert_mataPelajaran_action()
    {
        $this->form_validation->set_rules('kode_mapel', 'Kode Mapel', 'required');
        $this->form_validation->set_rules('tingkat', 'Tingkat', 'required');
        $this->form_validation->set_rules('nama_mapel', 'Nama Mapel', 'required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi Mapel', 'required');
        $this->form_validation->set_message('required', '* {field} Harus diisi');

        if ($this->form_validation->run() == FALSE) {
            $this->insert_mataPelajaran();
        } else {
            $kode_mapel     = $this->input->post("kode_mapel");
            $tingkat        = $this->input->post("tingkat");
            $nama_mapel     = $this->input->post("nama_mapel");
            $deskripsi      = $this->input->post("deskripsi");

            $data = [
                'kode_mapel'    => $kode_mapel,
                'tingkat'       => $tingkat,
                'nama_mapel'    => $nama_mapel,
                'deskripsi'     => $deskripsi,
            ];

            $cek_mapel = $this->MataPelajaranModel->cek_mapel($kode_mapel);
            if ($cek_mapel > 0) {
                $this->session->set_flashdata("error_message", "kode pelajaran " . $kode_mapel . " sudah ada");
                redirect(site_url("controllerMataPelajaran"));
            } else {
                $this->MataPelajaranModel->insert_MataPelajaran($data);
                $this->session->set_flashdata("flash_message", "Berhasil tambah data mata pelajaran.");
                redirect(site_url("controllerMataPelajaran"));
            }
        }
    }

    public function edit_mataPelajaran($kode_mapel)
    {
        $data_mataPelajaran = $this->MataPelajaranModel->get_by_id($kode_mapel);
        if ($data_mataPelajaran) {
            $data = [
                'action'        => site_url("controllerMataPelajaran/edit_mataPelajaran_action"),
                'kode_mapel'    => set_value("kode_mapel", $data_mataPelajaran->kode_mapel),
                'tingkat'       => set_value("tingkat", $data_mataPelajaran->tingkat),
                'nama_mapel'    => set_value("nama_mapel", $data_mataPelajaran->nama_mapel),
                'deskripsi'     => set_value("deskripsi", $data_mataPelajaran->deskripsi),
            ];
            $this->load->view("header");
            $this->load->view('mata_pelajaran/formMataPelajaran', $data);
            $this->load->view("footer");
        } else {
            $this->session->set_flashdata("error_message", "Gagal edit data mata pelajaran.");
            redirect(site_url("controllerMataPelajaran"));
        }
    }

    public function edit_mataPelajaran_action()
    {
        $this->form_validation->set_rules('kode_mapel', 'Kode Mapel', 'required');
        $this->form_validation->set_rules('tingkat', 'Tingkat', 'required');
        $this->form_validation->set_rules('nama_mapel', 'Nama Mapel', 'required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi Mapel', 'required');
        $this->form_validation->set_message('required', '* {field} Harus diisi');

        if ($this->form_validation->run() == FALSE) {
            $this->edit_mataPelajaran($this->input->post("kode_mapel"));
        } else {
            $kode_mapel      = $this->input->post("kode_mapel");
            $tingkat         = $this->input->post("tingkat");
            $nama_mapel      = $this->input->post("nama_mapel");
            $deskripsi       = $this->input->post("deskripsi");

            $data = [
                'tingkat'       => $tingkat,
                'nama_mapel'    => $nama_mapel,
                'deskripsi'     => $deskripsi,
            ];

            $this->MataPelajaranModel->update_mataPelajaran($kode_mapel, $data);
            $this->session->set_flashdata("flash_message", "Berhasil edit data mata pelajaran.");
            redirect(site_url("controllerMataPelajaran"));
        }
    }

    public function hapus_mataPelajaran($kode_mapel)
    {
        $data_mataPelajaran = $this->MataPelajaranModel->get_by_id($kode_mapel);
        if ($data_mataPelajaran) {
            $this->MataPelajaranModel->delete_mataPelajaran($kode_mapel);
            $this->session->set_flashdata("flash_message", "Berhasil hapus data mata pelajaran.");
            redirect(site_url("controllerMataPelajaran"));
        } else {
            $this->session->set_flashdata("error_message", "Gagal hapus data mata pelajaran.");
            redirect(site_url("controllerMataPelajaran"));
        }
    }
}
