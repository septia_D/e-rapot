<div class="table-responsive">
    <table id="mytable_rapor" class="table table-striped">
        <thead>
            <tr>
                <th width="5%">No</th>
                <th>NIS</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>Tanggal Lahir</th>
                <th>Aksi</th>
            </tr>
        </thead>
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#mytable_rapor").dataTable({
            "processing": true,
            "serverSide": true,
            "oLanguage": {
                sProcessing: "Loading. . ."
            },
            "ajax": {
                "url": "<?= site_url('ControllerWaliKelas/json_rapor') ?>",
                "type": "POST"
            },
            "columns": [{
                    "data": "nis",
                    "orderable": false,
                    "className": "text-center"
                },
                {
                    "data": "nis"
                },
                {
                    "data": "nama_lengkap"
                },
                {
                    "data": "jenis_kelamin",
                    "render": function(data, type, row, meta) {
                        if (data == 'L') {
                            return 'Laki-laki';
                        } else if (data == 'P') {
                            return 'Perempuan';
                        } else {
                            return '-';
                        }
                    }
                },
                {
                    "data": "tanggal_lahir"
                },
                {
                    "data": "action",
                    "orderable": false,
                    "className": "text-center"
                }
            ],
            order: [
                [0, 'desc']
            ],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>

<script>
    $('#mytable_rapor').on('click', '.cetak_rapot', function(e) {

        event.preventDefault();
        const href = $(this).attr('href');
        var id_siswa = $(this).data('nis');
        var id_kelas = $(this).data('kelas');

        $.ajax({
            url: "<?= site_url('controllerWaliKelas/cek_status_catatan') ?>",
            data: {
                id_siswa: id_siswa,
                id_kelas: id_kelas,
            },
            type: "POST",
            dataType: 'JSON',
            success: function(hasil) {
                if (hasil.status == "tidak ada") {
                    Swal.fire({
                        icon: 'error',
                        title: 'Silahkan lengkapi catatan siswa terlebih dahulu!',
                    });
                } else if (hasil.status == "ada") {
                    document.location.href = href;
                }
            }
        })

    });
</script>