<!-- <?php if ($this->session->session_login['username'] == 'admin') { ?>
    <div class="form-group">
        <label for="">Kelas </label>
        <p style="color: red"><i>*ketuk untuk pilih kelas</i></p>
        <select class="form-control select2" name="kode_kelas" id="kode_kelas" value="">
            <option value="" selected disabled>--Pilih--</option>
            <?php foreach ($kelas as $value) : ?>
                <option value="<?= $value->kode_kelas ?>"><?= $value->tingkat; ?> - <?= $value->rombel; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
<?php } ?> -->
<div class="table-responsive">
    <table id="mytable_ranking" class="table table-striped">
        <thead>
            <tr>
                <th width="5%">No</th>
                <th>NIS</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>Tanggal Lahir</th>
                <th>Ranking</th>
            </tr>
        </thead>
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#mytable_ranking").dataTable({
            "processing": true,
            "serverSide": true,
            "oLanguage": {
                sProcessing: "Loading. . ."
            },
            "ajax": {
                "url": "<?= site_url('ControllerWaliKelas/json_ranking') ?>",
                "type": "POST"
            },
            "columns": [{
                    "data": "nis",
                    "orderable": false,
                    "className": "text-center"
                },
                {
                    "data": "nis"
                },
                {
                    "data": "nama_lengkap"
                },
                {
                    "data": "jenis_kelamin",
                    "render": function(data, type, row, meta) {
                        if (data == 'L') {
                            return 'Laki-laki';
                        } else if (data == 'P') {
                            return 'Perempuan';
                        } else {
                            return '-';
                        }
                    }
                },
                {
                    "data": "tanggal_lahir"
                },
                {
                    "data": "ranking",
                    "render": function(data, type, row, meta) {
                        if (data == 0) {
                            return '<input class="form-control" type="text" value="" id="ranking" data-nis="' + row.nis + '" data-kode_kelas="' + row.kode_kelas + '">';
                        } else if (data != null) {
                            return '<input class="form-control" type="text" value="' + data + '" id="ranking" data-nis="' + row.nis + '" data-kode_kelas="' + row.kode_kelas + '">';
                        } else {
                            return '<input class="form-control" type="text" value="" id="ranking" data-nis="' + row.nis + '" data-kode_kelas="' + row.kode_kelas + '">';
                        }
                    }
                },
            ],
            order: [
                [0, 'desc']
            ],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });

        $('#mytable_ranking').on('change', '#ranking', function() {
            var id_siswa = $(this).data('nis');
            var id_kelas = $(this).data('kode_kelas');
            var ranking  = $(this).val();

            $.ajax({
                type: "POST",
                url: "<?= site_url('ControllerWaliKelas/insert_ranking_siswa') ?>",
                data: {
                    id_siswa: id_siswa,
                    id_kelas: id_kelas,
                    ranking: ranking,
                },
                dataType: "JSON",
                success: function(hasil) {
                    if (hasil.info == 'sukses') {
                        t.fnDraw();
                    } else {
                        alert('Gagal input nilai harian');
                    }
                }
            })
        });

    });
</script>