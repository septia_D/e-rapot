<div class="table-responsive">
    <table id="mytable_siswa" class="table table-striped">
        <thead>
            <tr>
                <th width="5%">No</th>
                <th>NIS</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>Tanggal Lahir</th>
                <th>Aksi</th>
            </tr>
        </thead>
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#mytable_siswa").dataTable({
            "processing": true,
            "serverSide": true,
            "oLanguage": {
                sProcessing: "Loading. . ."
            },
            "ajax": {
                "url": "<?= site_url('ControllerWaliKelas/json_siswa') ?>",
                "type": "POST"
            },
            "columns": [{
                    "data": "nis",
                    "orderable": false,
                    "className": "text-center"
                },
                {
                    "data": "nis"
                },
                {
                    "data": "nama_lengkap"
                },
                {
                    "data": "jenis_kelamin",
                    "render": function(data, type, row, meta) {
                        if(data == 'L'){
                            return 'Laki-laki';
                        } else if(data == 'P'){
                            return 'Perempuan';
                        } else {
                            return '-';
                        }
                    }
                },
                {
                    "data": "tanggal_lahir"
                },
                {
                    "data": "action",
                    "orderable": false,
                    "className": "text-center"
                }
            ],
            order: [
                [0, 'desc']
            ],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>