<!-- .row -->
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="white-box">
            <ul class="nav customtab nav-tabs" role="tablist">
                <li role="presentation" class="nav-item"><a href="#siswa" class="nav-link active" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="fa fa-home"></i></span><span class="hidden-xs"> Siswa</span></a></li>
                <li role="presentation" class="nav-item"><a href="#rapor" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Rapor</span></a></li>
                <li role="presentation" class="nav-item"><a href="#ranking" class="nav-link" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-envelope-o"></i></span> <span class="hidden-xs">Ranking</span></a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="siswa">
                    <?php $this->load->view('wali_kelas/siswa') ?>
                </div>
                <div class="tab-pane" id="rapor">
                    <?php $this->load->view('wali_kelas/rapor') ?>
                </div>
                <div class="tab-pane" id="ranking">
                    <?php $this->load->view('wali_kelas/ranking') ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->