<div class="row">
    <div class="col-md-2 col-xs-12">
        <div class="white-box">
            <div class="user-bg"> <img width="100%" height="100%" alt="user" src="<?= base_url('images/') . $siswa->foto ?>"> </div>
        </div>
    </div>

    <div class="col-sm-9">
        <div class="panel panel-default block1">
            <div class="panel-heading">
                <h3>Profil Siswa</h3>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td style="width: 30%;">Nama Lengkap</td>
                                    <td>:&nbsp;&nbsp; <?= $siswa->nama_lengkap; ?></td>
                                </tr>
                                <tr>
                                    <td style="width: 30%;">NIS</td>
                                    <td>:&nbsp;&nbsp; <?= $siswa->nis; ?></td>
                                </tr>
                                <tr>
                                    <td style="width: 30%;">Tempat Lahir</td>
                                    <td>:&nbsp;&nbsp; <?= $siswa->tempat_lahir; ?></td>
                                </tr>
                                <tr>
                                    <td style="width: 30%;">Tanggal Lahir</td>
                                    <td>:&nbsp;&nbsp; <?= $siswa->tanggal_lahir; ?></td>
                                </tr>
                                <tr>
                                    <?php
                                    if ($siswa->jenis_kelamin == "L") {
                                        $kelamin = "Laki-laki";
                                    } else {
                                        $kelamin = "Perempuan";
                                    }
                                    ?>
                                    <td style="width: 30%;">Jenis Kelamin</td>
                                    <td>:&nbsp;&nbsp; <?= $kelamin; ?></td>
                                </tr>
                                <tr>
                                    <td style="width: 30%;">Agama</td>
                                    <td>:&nbsp;&nbsp; <?= $siswa->agama; ?></td>
                                </tr>
                                <tr>
                                    <td style="width: 30%;">Alamat</td>
                                    <td>:&nbsp;&nbsp; <?= $siswa->alamat; ?></td>
                                </tr>
                                <tr>
                                    <td style="width: 30%;">Diterima</td>
                                    <td>:&nbsp;&nbsp; <?= $siswa->diterima; ?></td>
                                </tr>

                                <tr>
                                    <td style="width: 30%;"></td>
                                    <td>
                                        <a type="button" href="<?= site_url('controllerWaliKelas'); ?>" class="btn btn-inverse waves-effect waves-light">Kembali</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>