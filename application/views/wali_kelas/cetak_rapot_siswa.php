<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cetak Rapot</title>
    <!-- Bootstrap 3.3.7 -->
    <!-- <link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css"> -->
    <style>
        body {
            margin-top: 0cm;
            margin-left: 0cm;
            margin-right: 0cm;
            margin-bottom: 0cm;
            font-family: "Times New Roman";
            text-align: center;
        }

        input[type=checkbox]:checked {
            border-color: red !important;
            color: red !important;
        }

        table tr td {
            font-size: 15px;
        }
    </style>
</head>

<body>
    <!-- halaman pertama -->
    <table cellspacing="0" width="100%" style="margin-top: 50px;text-align: center;">
        <tr>
            <td><img src="<?php echo base_url('images/logo1.png') ?>" width="175px"></td>
        </tr>
        <tr>
            <td style="padding-top:20px;">
                <h2>LAPORAN HASIL BELAJAR</h2>
                <h2>MADRASAH TSANAWIYAH</h2>
                <h2>(MTs)</h2>
            </td>
        </tr>
        <tr>
            <td style="padding-top:60px;"><img src="<?php echo base_url('images/logo2.png') ?>" width="135px"></td>
        </tr>
        <tr>
            <td style="padding-top: 40px;">
                <h4>Nama Peserta didik:</h4>
            </td>
        </tr>
        <tr>
            <td style="padding-top: -30px;">
                <p><?= $get_data_siswa->nama_lengkap ?></p>
            </td>
        </tr>
        <tr>
            <td>
                <h4>NIS:</h4>
            </td>
        </tr>
        <tr>
            <td style="padding-top: -30px;">
                <p><?= $get_data_siswa->nis ?></p>
            </td>
        </tr>
        <tr>
            <td>
                <h4>NISN:</h4>
            </td>
        </tr>
        <tr>
            <td style="padding-top: -30px;">
                <p><?= ($get_data_siswa->nisn) ? $get_data_siswa->nisn : "-"; ?></p>
            </td>
        </tr>
        <tr>
            <td style="padding-top: 30px;">
                <h3>MTS MUHAMMADIYAH WANGON</h3>
                <h3>KEMENTERIAN AGAMA REPUBLIK INDONESIA</h3>
            </td>
        </tr>
    </table>
    <!-- akhir halaman pertama -->

    <!-- halaman kedua -->
    <table cellspacing="0" width="100%" style="page-break-before:always;text-align: center;">
        <tr>
            <td colspan="2" style="padding-top: 40px;">
                <h3>LAPORAN HASIL BELAJAR</h3>
                <h3>MADRASAH TSANAWIYAH</h3>
                <h3>(MTs)</h3>
            </td>
        </tr>
        <tr>
            <td style="text-align:left; padding-top: 70px;padding-left: 150px;">Nama Madrasah</td>
            <td style="text-align:left; ; padding-top: 70px;">&nbsp;&nbsp; : MTs Muhammadiyah Wangon </td>
        </tr>
        <tr>
            <td style="text-align:left; padding-top: 20px;padding-left: 150px;">NPSN</td>
            <td style="text-align:left; ; padding-top: 20px;">&nbsp;&nbsp; : 20363456 </td>
        </tr>
        <tr>
            <td style="text-align:left; padding-top: 20px;padding-left: 150px;">Alamat Madrasah</td>
            <td style="text-align:left; ; padding-top: 20px;">&nbsp;&nbsp; : Jalan Astana No 915 Wangon </td>
        </tr>
        <tr>
            <td style="text-align:left; padding-top: 20px;padding-left: 150px;">Desa/Kelurahan</td>
            <td style="text-align:left; ; padding-top: 20px;">&nbsp;&nbsp; : Wangon </td>
        </tr>
        <tr>
            <td style="text-align:left; padding-top: 20px;padding-left: 150px;">Kecamatan</td>
            <td style="text-align:left; ; padding-top: 20px;">&nbsp;&nbsp; : Wangon </td>
        </tr>
        <tr>
            <td style="text-align:left; padding-top: 20px;padding-left: 150px;">Kota/Kabupaten</td>
            <td style="text-align:left; ; padding-top: 20px;">&nbsp;&nbsp; : Kabupaten Banyumas </td>
        </tr>
        <tr>
            <td style="text-align:left; padding-top: 20px;padding-left: 150px;">Provinsi</td>
            <td style="text-align:left; ; padding-top: 20px;">&nbsp;&nbsp; : Jawa Tengah </td>
        </tr>
        <tr>
            <td style="text-align:left; padding-top: 20px;padding-left: 150px;">Website</td>
            <td style="text-align:left; ; padding-top: 20px;">&nbsp;&nbsp; : </td>
        </tr>
        <tr>
            <td style="text-align:left; padding-top: 20px;padding-left: 150px;">Email</td>
            <td style="text-align:left; ; padding-top: 20px;">&nbsp;&nbsp; : mtsmuhammadiyahwangon@gmail.com </td>
        </tr>
    </table>
    <table cellspacing="0" width="100%" style="page-break-before:always;text-align: center;">
        <tr>
            <td colspan="3" style="padding-top: 40px;">
                <h3>IDENTITAS PESERTA DIDIK</h3>
            </td>
        </tr>
        <tr>
            <td width="5%" style="padding-top: 40px;">1.</td>
            <td style="text-align: left;padding-top: 40px;">Nama Lengkap</td>
            <td style="text-align: left;padding-top: 40px;">: <?= $get_data_siswa->nama_lengkap ?></td>
        </tr>
        <tr>
            <td width="5%">2.</td>
            <td style="text-align: left">NIS</td>
            <td style="text-align: left">: <?= $get_data_siswa->nis ?></td>
        </tr>
        <tr>
            <td width="5%">3.</td>
            <td style="text-align: left">NISN</td>
            <td style="text-align: left">: </td>
        </tr>
        <tr>
            <td width="5%">4.</td>
            <td style="text-align: left">4. Tempat, Tanggal lahir</td>
            <td style="text-align: left">: <?= $get_data_siswa->tempat_lahir . ", " . $get_data_siswa->tanggal_lahir ?></td>
        </tr>
        <tr>
            <td width="5%">5.</td>
            <td style="text-align: left">Jenis Kelamin</td>
            <?php
            if ($get_data_siswa->jenis_kelamin == 'L') {
                $kelamin = "laki-laki";
            } else if ($get_data_siswa->jenis_kelamin == 'P') {
                $kelamin = "Perempuan";
            } else {
                $kelamin = "-";
            }
            ?>
            <td style="text-align: left">: <?= $kelamin ?></td>
        </tr>
        <tr>
            <td width="5%">6.</td>
            <td style="text-align: left">Agama</td>
            <td style="text-align: left">: <?= $get_data_siswa->agama ?></td>
        </tr>
        <tr>
            <td width="5%">7.</td>
            <td style="text-align: left">Status dalam Keluarga</td>
            <td style="text-align: left">: </td>
        </tr>
        <tr>
            <td width="5%">8.</td>
            <td style="text-align: left">Anak ke</td>
            <td style="text-align: left">: </td>
        </tr>
        <tr>
            <td width="5%">9.</td>
            <td style="text-align: left">Alamat</td>
            <td style="text-align: left">: <?= $get_data_siswa->alamat ?></td>
        </tr>
        <tr>
            <td width="5%">10.</td>
            <td style="text-align: left">Nomor Telepon Rumah</td>
            <td style="text-align: left">: </td>
        </tr>
        <tr>
            <td width="5%">11.</td>
            <td style="text-align: left">Madrasah Asal (SD/MI)</td>
            <td style="text-align: left">: </td>
        </tr>
        <tr>
            <td width="5%">12.</td>
            <td style="text-align: left">Diterima di madrasah ini</td>
            <td style="text-align: left"></td>
        </tr>
        <tr>
            <td width="5%"></td>
            <td style="text-align: left">a. Di kelas</td>
            <td style="text-align: left">:</td>
        </tr>
        <tr>
            <td width="5%"></td>
            <td style="text-align: left">b. Pada Tanggal</td>
            <td style="text-align: left">: <?= $get_data_siswa->diterima ?></td>
        </tr>
        <tr>
            <td width="5%">13.</td>
            <td style="text-align: left">Orang Tua</td>
            <td style="text-align: left"></td>
        </tr>
        <tr>
            <td width="5%"></td>
            <td style="text-align: left">a. Nama Ayah</td>
            <td style="text-align: left">:</td>
        </tr>
        <tr>
            <td width="5%"></td>
            <td style="text-align: left">b. Pekerjaan</td>
            <td style="text-align: left">: </td>
        </tr>
        <tr>
            <td width="5%"></td>
            <td style="text-align: left">c. Nomor Telepon/HP</td>
            <td style="text-align: left">: </td>
        </tr>
        <tr>
            <td width="5%"></td>
            <td style="text-align: left">d. Alamat</td>
            <td style="text-align: left">: </td>
        </tr>
        <tr>
            <td width="5%"></td>
            <td style="text-align: left">e. Nama Ibu</td>
            <td style="text-align: left">: </td>
        </tr>
        <tr>
            <td width="5%"></td>
            <td style="text-align: left">f. Pekerjaan</td>
            <td style="text-align: left">: </td>
        </tr>
        <tr>
            <td width="5%">14.</td>
            <td style="text-align: left">Wali</td>
            <td style="text-align: left"></td>
        </tr>
        <tr>
            <td width="5%"></td>
            <td style="text-align: left">a. Nama Wali</td>
            <td style="text-align: left">: </td>
        </tr>
        <tr>
            <td width="5%"></td>
            <td style="text-align: left">b. Pekerjaan</td>
            <td style="text-align: left">: </td>
        </tr>
        <tr>
            <td width="5%"></td>
            <td style="text-align: left">c. Nomor Telepon/HP</td>
            <td style="text-align: left">: </td>
        </tr>
        <tr>
            <td width="5%"></td>
            <td style="text-align: left">d. Alamat</td>
            <td style="text-align: left">: </td>
        </tr>
    </table>
    <table cellspacing="0" width="100%" style="padding-top: 50px;">
        <tr>
            <td colspan="3" style="padding-right: 80px;"><img src="<?php echo base_url('images/') . $get_data_siswa->foto ?>" width="100"></td>
            <td>
                <p>Wangon, <?= date('Y-m-d') ?> </p>
                <p style="padding-top: -15px;">Kepala Madrasah</p><br><br>
                <p><u><b>Riyo Hartini, S.Pd </b></u></p>
                <p style="padding-top: -15px;"><b>Nip. </b></p>
            </td>
        </tr>
    </table>
    <!-- akhir halaman kedua -->

    <!-- halaman ketiga -->
    <hr style="page-break-before:always;">
    <table cellspacing="0" width="100%">
        <tr>
            <td width="15%">Nama</td>
            <td width="35%">: <?= $get_data_siswa->nama_lengkap; ?></td>
            <td width="20%">Madrasah</td>
            <td width="30%">: MTs Muhammadiyah</td>
        </tr>
        <tr>
            <td width="15%">NIS</td>
            <td width="35%">: <?= $get_data_siswa->nis; ?></td>
            <td width="20%">Kelas/Semester</td>
            <td width="30%">: <?= $get_data_kelas->tingkat . " " . $get_data_kelas->rombel . "/Semester " . $get_data_kelas->semester  ?></td>
        </tr>
        <tr>
            <td width="15%">NISN</td>
            <td width="35%">:</td>
            <td width="20%">Tahun Pembelajaran</td>
            <td width="30%">: <?= $get_data_kelas->tahun_ajar ?></td>
        </tr>
    </table>
    <hr>
    <table cellspacing="0" width="100%" style="padding-top: 40px;">
        <tr>
            <td style="text-align: center">
                <h3>CAPAIAN HASIL BELAJAR</h3>
            </td>
        </tr>
        <tr>
            <td>
                <h3>A. Sikap</h3>
            </td>
        </tr>
        <tr>
            <td>
                <h4>1. Sikap Spiritual</h4>
            </td>
        </tr>
    </table>
    <table border="1" cellspacing="0" width="100%">
        <tr>
            <td style="text-align: center;padding:5px;">Predikat</td>
            <td style="text-align: center;padding:5px;">Deskripsi</td>
        </tr>
        <tr>
            <td style="text-align: center" width="30%"><?= $get_catatan_siswa->sikap_spiritual_predikat ?></td>
            <td style="padding: 5px;" width="70%"><?= $get_catatan_siswa->sikap_spiritual_deskripsi ?></td>
        </tr>
    </table>
    <table cellspacing="0" width="100%" style="padding-top: 10px;">
        <tr>
            <td>
                <h4>2. Sikap Sosial</h4>
            </td>
        </tr>
    </table>
    <table border="1" cellspacing="0" width="100%">
        <tr>
            <td style="text-align: center;padding:5px;">Predikat</td>
            <td style="text-align: center;padding:5px;">Deskripsi</td>
        </tr>
        <tr>
            <td style="text-align: center" width="30%"><?= $get_catatan_siswa->sikap_sosial_predikat ?></td>
            <td style="padding: 5px;" width="70%"><?= $get_catatan_siswa->sikap_sosial_deskripsi ?></td>
        </tr>
    </table>
    <!-- akhir halaman ketiga -->

    <!-- halaman keempat -->
    <hr style="page-break-before:always;">
    <table cellspacing="0" width="100%">
        <tr>
            <td width="15%">Nama</td>
            <td width="35%">: <?= $get_data_siswa->nama_lengkap; ?></td>
            <td width="20%">Madrasah</td>
            <td width="30%">: MTs Muhammadiyah</td>
        </tr>
        <tr>
            <td width="15%">NIS</td>
            <td width="35%">: <?= $get_data_siswa->nis; ?></td>
            <td width="20%">Kelas/Semester</td>
            <td width="30%">: <?= $get_data_kelas->tingkat . " " . $get_data_kelas->rombel . "/Semester " . $get_data_kelas->semester  ?></td>
        </tr>
        <tr>
            <td width="15%">NISN</td>
            <td width="35%">:</td>
            <td width="20%">Tahun Pembelajaran</td>
            <td width="30%">: <?= $get_data_kelas->tahun_ajar ?></td>
        </tr>
    </table>
    <hr>
    <table cellspacing="0" width="100%" style="padding-top: 20px;">
        <tr>
            <td>
                <h3>B. PENGETAHUAN DAN KETERAMPILAN</h3>
            </td>
        </tr>
    </table>
    <table border="1" cellspacing="0" width="100%" style="text-align: center;">
        <tr>
            <td style="padding: 5px;" width="5%">No</td>
            <td width="30%">Mata Pelajaran</td>
            <td width="10%">Nilai</td>
            <td width="10%">Predikat</td>
            <td width="40%">Deskripsi</td>
        </tr>
        <?php $no = 1; ?>
        <?php foreach ($mapel as $value) : ?>
            <tr>
                <td><?= $no++; ?></td>
                <td style="text-align: left;padding: 3px;"><?= $value->nama_mapel; ?></td>
                <td><?= $value->nilai_akhir ?></td>
                <td><?= $value->predikat ?></td>
                <td style="text-align: justify;padding: 3px;"><?= $value->deskripsi ?></td>
            </tr>
        <?php endforeach; ?>
        <tr>
            <td colspan="2" style="text-align: center;padding: 3px;">Jumlah</td>
            <td><?= $jmlh_nilai->nilai_akhir; ?></td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <!-- akhir halaman keempat -->

    <!-- halaman kelima -->
    <hr style="page-break-before:always;">
    <table cellspacing="0" width="100%">
        <tr>
            <td width="15%">Nama</td>
            <td width="35%">: <?= $get_data_siswa->nama_lengkap; ?></td>
            <td width="20%">Madrasah</td>
            <td width="30%">: MTs Muhammadiyah</td>
        </tr>
        <tr>
            <td width="15%">NIS</td>
            <td width="35%">: <?= $get_data_siswa->nis; ?></td>
            <td width="20%">Kelas/Semester</td>
            <td width="30%">: <?= $get_data_kelas->tingkat . " " . $get_data_kelas->rombel . "/Semester " . $get_data_kelas->semester  ?></td>
        </tr>
        <tr>
            <td width="15%">NISN</td>
            <td width="35%">:</td>
            <td width="20%">Tahun Pembelajaran</td>
            <td width="30%">: <?= $get_data_kelas->tahun_ajar ?></td>
        </tr>
    </table>
    <hr>
    <table cellspacing="0" width="100%" style="padding-top: 10px;">
        <tr>
            <td>
                <h3>D. Ekstrakurikuler</h3>
            </td>
        </tr>
    </table>
    <table border="1" cellspacing="0" width="100%" style="padding-top: 10px; text-align:center">
        <tr>
            <td width="5%" style="padding: 5px;">No.</td>
            <td width="35%">Kegiatan Ekstrakurikuler</td>
            <td width="10%">Predikat</td>
            <td width="50%">Deskripsi</td>
        </tr>
        <?php $no_ekstra = 1; ?>
        <?php foreach ($get_ekstra as $value) : ?>
            <tr>
                <td><?= $no_ekstra++; ?></td>
                <td style="text-align: left;padding:5px;"><?= $value->nama_ekstra ?></td>
                <td><?= $value->predikat ?></td>
                <td style="text-align: left;padding:5px;"><?= $value->deskripsi ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
    <table cellspacing="0" width="100%" style="padding-top: 10px;">
        <tr>
            <td>
                <h3>E. Prestasi</h3>
            </td>
        </tr>
    </table>
    <table border="1" cellspacing="0" width="100%" style="padding-top: 10px; text-align:center">
        <tr>
            <td width="5%" style="padding: 5px;">No.</td>
            <td width="35%">Jenis Kegiatan</td>
            <td width="60%">Deskripsi</td>
        </tr>
        <?php $no_prestasi = 1; ?>
        <?php foreach ($get_prestasi as $value) : ?>
            <tr>
                <td><?= $no_prestasi++; ?></td>
                <td style="text-align: left;padding:5px;"><?= $value->nama_prestasi ?></td>
                <td style="text-align: left;padding:5px;"><?= $value->deskripsi ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
    <table cellspacing="0" width="100%" style="padding-top: 10px;">
        <tr>
            <td>
                <h3>F. Ketidakhadiran</h3>
            </td>
        </tr>
    </table>
    <table border="1" cellspacing="0" style="padding-top: 10px;">
        <tr>
            <td style="padding: 5px;">Sakit</td>
            <td style="padding: 5px;">: <?= ($get_catatan_siswa->sakit) ? $get_catatan_siswa->sakit : "0"; ?> &nbsp;&nbsp; hari</td>
        </tr>
        <tr>
            <td style="padding: 5px;">Izin</td>
            <td style="padding: 5px;">: <?= ($get_catatan_siswa->izin) ? $get_catatan_siswa->izin : ""; ?> &nbsp;&nbsp; hari</td>
        </tr>
        <tr>
            <td style="padding: 5px;">Tanpa keterangan</td>
            <td style="padding: 5px;">: <?= ($get_catatan_siswa->tanpa_keterangan) ? $get_catatan_siswa->tanpa_keterangan : ""; ?> &nbsp;&nbsp; hari</td>
        </tr>
    </table>
    <table cellspacing="0" width="100%" style="padding-top: 10px;">
        <tr>
            <td>
                <h3>G. Catatan Wali Kelas</h3>
            </td>
        </tr>
    </table>
    <table border="1" cellspacing="0" width="100%" style="padding-top: 10px;">
        <tr>
            <td style="padding: 10px;"><?= $get_catatan_siswa->catatan_wali ?></td>
        </tr>
    </table>
    <table cellspacing="0" width="100%" style="padding-top: 20px;">
        <tr>
            <td>
                <h3>H. Tanggapan Orang tua/Wali</h3>
            </td>
        </tr>
    </table>
    <table border="1" cellspacing="0" width="100%" style="padding-top: 10px;">
        <tr>
            <td style="padding-top: 40px;"></td>
        </tr>
    </table>
    <table cellspacing="0" width="100%">
        <tr>
            <td colspan="3" style="padding-right: 80px;">
                <p >Orang Tua/Wali</p>
            </td>
            <td >
                <p>Wangon, <?= date('Y-m-d') ?> </p>  
            </td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td><p style="padding-top: -15px;">Wali Kelas</p><br><br>
                <p><u><b><?= $get_data_kelas->nama_lengkap ?> </b></u></p>
                <p style="padding-top: -15px;"><b>Nip. </b></p>
            </td>
        </tr>
    </table> 
    <!-- akhir halaman kelima -->
</body>

</html>