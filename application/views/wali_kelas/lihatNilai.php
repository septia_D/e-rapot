<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3><?= $nis ?> - <?= $nama_lengkap ?></h3>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">

                    <div class="table-responsive">
                        <table id="myTable" class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th>Mata Pelajaran</th>
                                    <th>UH1</th>
                                    <th>UH2</th>
                                    <th>UH3</th>
                                    <th>UH4</th>
                                    <th>UH5</th>
                                    <th>UH6</th>
                                    <th>RPH</th>
                                    <th>PAT</th>
                                    <th>Nilai Akhir</th>
                                    <th>Predikat Akhir</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; ?>
                                <?php foreach ($nilai as $value) : ?>
                                    <tr>
                                        <td><?= $no++; ?></td>
                                        <td><?= $value->nama_mapel ?></td>
                                        <td><?= $value->uh1 ?></td>
                                        <td><?= $value->uh2 ?></td>
                                        <td><?= $value->uh3 ?></td>
                                        <td><?= $value->uh4 ?></td>
                                        <td><?= $value->uh5 ?></td>
                                        <td><?= $value->uh6 ?></td>
                                        <td><?= $value->rph ?></td>
                                        <td><?= $value->uas ?></td>
                                        <td><?= $value->nilai_akhir ?></td>
                                        <td><?= $value->predikat_akhir ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>

                        </table>
                    </div>

                    <a type="button" href="<?= site_url('controllerWaliKelas'); ?>" class="btn btn-inverse waves-effect waves-light" style="margin-top: 30px;">Kembali</a>

                </div>
            </div>
        </div>
    </div>
</div>