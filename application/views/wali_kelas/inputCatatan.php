<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3><?= $nis ?> - <?= $nama_lengkap ?> - Input Catatan Rapot</h3>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">

                    <form class="form-horizontal" id="form_input_catatan">
                        <input type="hidden" name="id_siswa" id="id_siswa" value="<?= $nis ?>">
                        <input type="hidden" name="id_kelas" id="id_kelas" value="<?= $kode_kelas ?>">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-12">Sikap Spiritual - Predikat</label>
                                <div class="col-md-12">
                                    <select name="sikap_spiritual_predikat" id="" class="form-control">
                                        <option value="" disabled selected>-- Pilih --</option>
                                        <option value="Sangat Baik" <?= ($sikap_spiritual_predikat == "Sangat Baik") ? "selected" : "" ?>>Sangat Baik</option>
                                        <option value="Baik" <?= ($sikap_spiritual_predikat == "Baik") ? "selected" : "" ?>>Baik</option>
                                        <option value="Cukup" <?= ($sikap_spiritual_predikat == "Cukup") ? "selected" : "" ?>>Cukup</option>
                                        <option value="Kurang Baik" <?= ($sikap_spiritual_predikat == "Kurang Baik") ? "selected" : "" ?>>Kurang Baik</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Sikap Spiritual - Deskripsi</label>
                                <div class="col-md-12">
                                    <textarea name="sikap_spiritual_deskripsi" id="" cols="75" rows="4" class="form-control" placeholder="Ketuk untuk mengisi deskripsi"><?= $sikap_spiritual_deskripsi ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-12">Sikap Sosial - Predikat</label>
                                <div class="col-md-12">
                                    <select name="sikap_sosial_predikat" id="" class="form-control">
                                        <option value="" disabled selected>-- Pilih --</option>
                                        <option value="Sangat Baik" <?= ($sikap_sosial_predikat == "Sangat Baik") ? "selected" : "" ?>>Sangat Baik</option>
                                        <option value="Baik" <?= ($sikap_sosial_predikat == "Baik") ? "selected" : "" ?>>Baik</option>
                                        <option value="Cukup" <?= ($sikap_sosial_predikat == "Cukup") ? "selected" : "" ?>>Cukup</option>
                                        <option value="Kurang Baik" <?= ($sikap_sosial_predikat == "Kurang Baik") ? "selected" : "" ?>>Kurang Baik</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Sikap Sosial - Deskripsi</label>
                                <div class="col-md-12">
                                    <textarea name="sikap_sosial_deskripsi" id="" cols="75" rows="4" class="form-control" placeholder="Ketuk untuk mengisi deskripsi"><?= $sikap_sosial_deskripsi ?></textarea>
                                </div>
                            </div>
                        </div>
                        <p>*) Penilaian <b>Sikap Spiritual dan Sikap Sosial</b></p>
                        <br>
                        <hr>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-md-12">Kegiatan Ekstrakulikuler</label>
                                <div class="col-md-12">
                                    <select name="kode_ekstra" id="kode_ekstra" class="form-control">
                                        <option value="" disabled selected>-- Pilih --</option>
                                        <?php foreach ($listEkstra as $value) : ?>
                                            <option value="<?= $value->kode_ekstra ?>"><?= $value->nama_ekstra ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="col-md-12">Predikat</label>
                            <div class="col-md-12">
                                <select name="predikat_ekstra" id="predikat_ekstra" class="form-control">
                                    <option value="" disabled selected>-- Pilih --</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="C">C</option>
                                    <option value="D">D</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="col-md-12">Deskripsi</label>
                            <div class="col-md-12">
                                <textarea name="ekstra_deskripsi" id="ekstra_deskripsi" cols="75" rows="4" class="form-control" placeholder="Ketuk untuk mengisi deskripsi"></textarea>
                            </div>
                        </div>
                        <div class="col-md-2" style="padding-top: 25px;">
                            <button type="button" class="btn btn-primary waves-effect waves-light m-r-10" id="btn_simpan_ekstra"><i class="fa fa-plus"></i></button>
                        </div>

                        <div class="col-md-12" style="padding-top: 30px;">
                            <div class="table-responsive">
                                <table id="myTable_ekstra" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th width="5%">No</th>
                                            <th>Ekstrakulikuler</th>
                                            <th>Predikat</th>
                                            <th>Deskripsi</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <p>*) Penilaian <b>Ekstrakulikuler</b></p>
                            <br>
                            <hr>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-md-12">Jenis Prestasi</label>
                                <div class="col-md-12">
                                    <input type="text" name="nama_prestasi" id="nama_prestasi" class="form-control" placeholder="Ketuk untuk mengisi prestasi">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="col-md-12">Predikat</label>
                            <div class="col-md-12">
                                <select name="predikat_prestasi" id="predikat_prestasi" class="form-control">
                                    <option value="" disabled selected>-- Pilih --</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="C">C</option>
                                    <option value="D">D</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="col-md-12">Deskripsi</label>
                            <div class="col-md-12">
                                <textarea name="prestasi_deskripsi" id="prestasi_deskripsi" cols="75" rows="4" class="form-control" placeholder="Ketuk untuk mengisi deskripsi"></textarea>
                            </div>
                        </div>
                        <div class="col-md-2" style="padding-top: 25px;">
                        <button type="button" class="btn btn-primary" id="btn_simpan_prestasi"><i class="fa fa-plus"></i></button>
                        </div>

                        <div class="col-md-12" style="padding-top: 30px;">
                            <div class="table-responsive">
                                <table id="myTable_prestasi" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th width="5%">No</th>
                                            <th>Prestasi</th>
                                            <th>Predikat</th>
                                            <th>Deskripsi</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <p>*) Penilaian <b>Prestasi</b></p>
                            <br>
                            <hr>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-12">Keterangan Sakit (hari)</label>
                                <div class="col-md-12">
                                    <input type="number" name="sakit" id="" class="form-control" value="<?= $sakit ?>" placeholder="Ketuk untuk mengisi Sakit">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Keterangan Izin (hari)</label>
                                <div class="col-md-12">
                                    <input type="number" name="izin" id="" class="form-control" value="<?= $izin ?>" placeholder="Ketuk untuk mengisi Izin">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Tanpa Keterangan (hari)</label>
                                <div class="col-md-12">
                                    <input type="number" name="tanpa_keterangan" id="" class="form-control" value="<?= $tanpa_keterangan ?>" placeholder="Ketuk untuk mengisi Tanpa Keterangan">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="col-md-12">Status Siswa</label>
                            <div class="col-md-12">
                                <select name="status_siswa" id="" class="form-control">
                                    <option value="" disabled selected>-- Pilih --</option>
                                    <option value="Aktif" <?= ($status_siswa == "Aktif") ? "selected" : ""; ?>>Aktif</option>
                                    <option value="Non Aktif" <?= ($status_siswa == "Non Aktif") ? "selected" : ""; ?>>Non Aktif</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="col-md-12">Catatan Wali Kelas</label>
                            <div class="col-md-12">
                                <textarea name="catatan_wali" id="" cols="75" rows="4" class="form-control" placeholder="Ketuk untuk mengisi catatan"><?= $catatan_wali ?></textarea>
                            </div>
                        </div>

                        <div class="col-md-12" style="padding-top: 30px;">
                            <a type="button" href="<?= site_url('controllerWaliKelas'); ?>" class="btn btn-inverse waves-effect waves-light">Kembali</a>
                            <button type="button" class="btn btn-success waves-effect waves-light m-r-10 pull-right" id="simpan_catatan">Simpan</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var prestasi = $("#myTable_prestasi").dataTable({
            "processing": true,
            "serverSide": true,
            "oLanguage": {
                sProcessing: "Loading. . ."
            },
            "ajax": {
                "url": "<?= site_url('ControllerWaliKelas/json_prestasi') ?>",
                "type": "POST",
                "data": function(d) {
                    d.id_siswa = $('#id_siswa').val();
                    d.id_kelas = $('#id_kelas').val();
                }
            },
            "columns": [{
                    "data": "nama_prestasi",
                    "orderable": false,
                    "className": "text-center"
                },
                {
                    "data": "nama_prestasi"
                },
                {
                    "data": "predikat"
                },
                {
                    "data": "deskripsi"
                },
                {
                    "data": "action",
                    "orderable": false,
                    "className": "text-center"
                }
            ],
            order: [
                [0, 'desc']
            ],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });

        $('#btn_simpan_prestasi').click(function(e) {
            var id_siswa = $('#id_siswa').val();
            var id_kelas = $('#id_kelas').val();
            var nama_prestasi = $('#nama_prestasi').val();
            var predikat_prestasi = $('#predikat_prestasi').val();
            var prestasi_deskripsi = $('#prestasi_deskripsi').val();

            if (nama_prestasi == null || nama_prestasi == '') {
                Swal.fire({
                    icon: 'error',
                    title: 'Silahkan isi prestasi terlebih dahulu!',
                });
            } else {
                $.ajax({
                    url: "<?= site_url('controllerWaliKelas/insert_nilai_prestasi') ?>",
                    data: {
                        nama_prestasi: nama_prestasi,
                        predikat_prestasi: predikat_prestasi,
                        prestasi_deskripsi: prestasi_deskripsi,
                        id_siswa: id_siswa,
                        id_kelas: id_kelas,
                    },
                    type: "POST",
                    dataType: 'JSON',
                    success: function(hasil) {
                        if (hasil.status = "sukses") {
                            prestasi.fnDraw();
                        }
                    }
                })
            }


        });

    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var ekstra = $("#myTable_ekstra").dataTable({
            "processing": true,
            "serverSide": true,
            "oLanguage": {
                sProcessing: "Loading. . ."
            },
            "ajax": {
                "url": "<?= site_url('ControllerWaliKelas/json_ekstra') ?>",
                "type": "POST",
                "data": function(d) {
                    d.id_siswa = $('#id_siswa').val();
                    d.id_kelas = $('#id_kelas').val();
                }
            },
            "columns": [{
                    "data": "kode_ekstra",
                    "orderable": false,
                    "className": "text-center"
                },
                {
                    "data": "nama_ekstra"
                },
                {
                    "data": "predikat"
                },
                {
                    "data": "deskripsi"
                },
                {
                    "data": "action",
                    "orderable": false,
                    "className": "text-center"
                }
            ],
            order: [
                [0, 'desc']
            ],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });

        $('#btn_simpan_ekstra').click(function() {
            var id_siswa = $('#id_siswa').val();
            var id_kelas = $('#id_kelas').val();
            var kode_ekstra = $('#kode_ekstra').val();
            var predikat_ekstra = $('#predikat_ekstra').val();
            var ekstra_deskripsi = $('#ekstra_deskripsi').val();

            if (kode_ekstra == null || kode_ekstra == '') {
                Swal.fire({
                    icon: 'error',
                    title: 'Silahkan isi ekstra terlebih dahulu!',
                });
            } else {
                $.ajax({
                    url: "<?= site_url('controllerWaliKelas/insert_nilai_ekstra') ?>",
                    data: {
                        kode_ekstra: kode_ekstra,
                        predikat_ekstra: predikat_ekstra,
                        ekstra_deskripsi: ekstra_deskripsi,
                        id_siswa: id_siswa,
                        id_kelas: id_kelas,
                    },
                    type: "POST",
                    dataType: 'JSON',
                    success: function(hasil) {
                        if (hasil.status = "sukses") {
                            ekstra.fnDraw();
                        }
                    }
                })
            }

        });

    });
</script>

<script type="text/javascript">
    $(function() {

        function input() {
            $.ajax({
                url: "<?= site_url('controllerWaliKelas/update_catatan_wali') ?>",
                data: $("#form_input_catatan").serialize(),
                type: "POST",
                dataType: 'JSON',
                success: function(hasil) {

                }
            })
        }

        $("input[type='checkbox'], input[type='radio']").on("change", input);
        $("select").on("change", input);
        $("textarea").on("change", input);
        $("input[type='text']").on("change", input);
        $("input[type='date']").on("change", input);
        $("input[type='number']").on("change", input);

        $('#simpan_catatan').click(function() {
            $.ajax({
                url: "<?= site_url('controllerWaliKelas/insert_catatan_wali') ?>",
                data: $("#form_input_catatan").serialize(),
                type: "POST",
                dataType: 'JSON',
                success: function(hasil) {
                    location.href = "<?= site_url('controllerWaliKelas') ?>";
                }
            })
        })

    });
</script>