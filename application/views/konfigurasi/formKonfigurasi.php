<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default block1">
            <div class="panel-heading"><h3>Konfigurasi</h3></div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <form class="form-material form-horizontal" method="POST" action="<?= site_url('controllerKonfigurasi/insert_session_konfigurasi') ?>">
                        <div class="form-group">
                            <label class="col-md-12" for="bdate">Tahun Ajar</span>
                            </label>
                            <div class="col-md-12">
                                <select class="form-control select2" name="tahun_ajar">
                                    <option value="" selected disabled>--Pilih--</option>
                                    <?php foreach($listTahunAjar as $value) : ?>
                                        <option value="<?= $value->nama_tahunajar ?>"><?= $value->nama_tahunajar; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="text-danger"><?= form_error('tahun_ajar') ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12" for="bdate">Semester</span>
                            </label>
                            <div class="col-md-12">
                                <select class="form-control select2" name="semester">
                                    <option value="" selected disabled>--Pilih--</option>
                                    <?php foreach($listSemester as $value) : ?>
                                        <option value="<?= $value->nama_semester ?>"><?= $value->nama_semester; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="text-danger"><?= form_error('semester') ?></span>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">Atur</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>