<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
    <title>Aplikasi E-Rapot</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url() ?>theme_admin/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="<?= base_url() ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="<?= base_url() ?>theme_admin/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?= base_url() ?>theme_admin/css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?= base_url() ?>theme_admin/css/colors/megna.css" id="theme" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url() ?>plugins/bower_components/toast-master/css/jquery.toast.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?= base_url() ?>plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
    <!--Material Datetimepicker -->
    <link rel="stylesheet" href="<?= base_url() ?>plugins/material-datetimepicker/css/bootstrap-material-datetimepicker.css">

    <link href="<?php echo base_url(); ?>plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <script src="<?php echo base_url(); ?>plugins/js/jquery-1.11.0.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <!-- Date picker plugins css -->
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-19175540-9', 'auto');
        ga('send', 'pageview');
    </script>
</head>

<body class="fix-sidebar">
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        <!-- Top Navigation -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                <div class="top-left-part"><a class="logo" href="javascript:void(0)"><b><img src="<?= base_url() ?>images/logo.png" alt="home" style="width:50px;" /></b><span class="hidden-xs"><strong>E</strong>Rapot</span></a></div>
                <ul class="nav navbar-top-links navbar-left hidden-xs">
                    <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
                </ul>
                <ul class="nav navbar-top-links navbar-right pull-right">

                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <?= ($this->session->session_login['nama_guru']) ? $this->session->session_login['nama_guru'] : "Admin" ?> <i class="fa fa-power-off"></i></b> </a>
                        <ul class="dropdown-menu dropdown-user animated flipInY">
                            <li><a href="<?= site_url("controllerLogin/logout"); ?>" class="waves-effect"><i class="icon-logout fa-fw"></i> <span class="hide-menu">Log out</span></a></li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
            </div>

            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- End Top Navigation -->
        <!-- Left navbar-header -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                        <!-- input-group -->
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search..."> <span class="input-group-btn">
                                <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
                            </span> </div>
                        <!-- /input-group -->
                    </li>
                    <li class="user-pro">
                        <a href="#" class="waves-effect"><span class="hide-menu"><?= ($this->session->session_login['nama_guru']) ? $this->session->session_login['nama_guru'] : "Admin" ?><span class="fa arrow"></span></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li><a href="<?= site_url('controllerHome/lihat_profil') ?>"><i class="ti-user"></i> Profile</a></li>
                        </ul>
                    </li>
                    <li class="nav-small-cap m-t-10">--- Main Menu</li>
                    <li> <a href="<?= base_url("controllerHome"); ?>" class="waves-effect"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i> <span class="hide-menu"> Dashboard </span></a></li>
                    <?php
                    if ($this->session->session_login['level'] == 'admin') {
                    ?>
                        <li><a href="javascript:void(0);" class="waves-effect"><i data-icon=")" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Master Data<span class="fa arrow"></span></span></a>
                            <ul class="nav nav-second-level">
                                <li> <a href="<?= site_url('controllerSemester'); ?>">Semester</a></li>
                                <li> <a href="<?= site_url('controllerTahunAjar'); ?>">Tahun Ajar</a></li>
                            </ul>
                        </li>
                        <li><a href="<?= site_url("controllerKonfigurasi"); ?>" class="waves-effect"><i class="fa fa-gear fa-fw"></i> <span class="hide-menu">Konfigurasi</span></a></li>

                        <li><a href="<?= site_url('controllerMataPelajaran'); ?>" class="waves-effect"><i class="fa fa-book fa-fw"></i> <span class="hide-menu">Mata Pelajaran</span></a></li>

                        <li><a href="<?= site_url("controllerGuru"); ?>" class="waves-effect"><i class="icon-user fa-fw"></i> <span class="hide-menu">Guru</span></a></li>

                        <li><a href="<?= site_url("controllerSiswa"); ?>" class="waves-effect"><i class="icon-user fa-fw"></i> <span class="hide-menu">Siswa</span></a></li>

                        <li><a href="<?= site_url("controllerKelas"); ?>" class="waves-effect"><i class="fa fa-bank fa-fw"></i> <span class="hide-menu">Rombel</span></a></li>

                        <li><a href="<?= site_url("controllerEkstra"); ?>" class="waves-effect"><i class=" icon-game-controller fa-fw"></i> <span class="hide-menu">Ekstrakulikuler</span></a></li>

                        <li><a href="<?= site_url("controllerBobot"); ?>" class="waves-effect"><i class=" fa fa-percent fa-fw"></i> <span class="hide-menu">Bobot dan KKM</span></a></li>
                        <li><a href="<?= site_url("controllerNilaiHarian"); ?>" class="waves-effect"><i class=" fa  fa-file-archive-o fa-fw"></i> <span class="hide-menu">Input Nilai Harian</span></a></li>
                        <li><a href="<?= site_url("controllerNilaiAkhir"); ?>" class="waves-effect"><i class=" fa fa-history fa-fw"></i> <span class="hide-menu">Input Nilai Akhir</span></a></li>

                        <li><a href="<?= site_url("controllerWaliKelas"); ?>" class="waves-effect"><i class=" icon-user fa-fw"></i> <span class="hide-menu">Wali Kelas</span></a></li>

                    <?php
                    }
                    ?>

                    <?php
                    if ($this->session->session_login['level'] == 'guru') {
                    ?>

                        <li><a href="<?= site_url("controllerBobot"); ?>" class="waves-effect"><i class=" fa fa-percent fa-fw"></i> <span class="hide-menu">Bobot dan KKM</span></a></li>
                        <li><a href="<?= site_url("controllerNilaiHarian"); ?>" class="waves-effect"><i class=" fa  fa-file-archive-o fa-fw"></i> <span class="hide-menu">Input Nilai Harian</span></a></li>
                        <li><a href="<?= site_url("controllerNilaiAkhir"); ?>" class="waves-effect"><i class=" fa fa-history fa-fw"></i> <span class="hide-menu">Input Nilai Akhir</span></a></li>
                    <?php
                    }
                    ?>

                    <?php
                    if ($this->session->session_login['level'] == 'wali_murid') {
                    ?>

                        <li><a href="<?= site_url("controllerBobot"); ?>" class="waves-effect"><i class=" fa fa-percent fa-fw"></i> <span class="hide-menu">Bobot dan KKM</span></a></li>
                        <li><a href="<?= site_url("controllerNilaiHarian"); ?>" class="waves-effect"><i class=" fa  fa-file-archive-o fa-fw"></i> <span class="hide-menu">Input Nilai Harian</span></a></li>
                        <li><a href="<?= site_url("controllerNilaiAkhir"); ?>" class="waves-effect"><i class=" fa fa-history fa-fw"></i> <span class="hide-menu">Input Nilai Akhir</span></a></li>
                        <li><a href="<?= site_url("controllerWaliKelas"); ?>" class="waves-effect"><i class=" icon-user fa-fw"></i> <span class="hide-menu">Wali Kelas</span></a></li>

                    <?php
                    }
                    ?>

                    <li><a href="<?= site_url("controllerLogin/logout"); ?>" class="waves-effect"><i class="icon-logout fa-fw"></i> <span class="hide-menu">Log out</span></a></li>
            </div>
        </div>
        <!-- Left navbar-header end -->

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Selamat Datang di Aplikasi E-Rapot MTS Muhammadiyah Wangon</h4>
                    </div>

                    <!-- /.col-lg-12 -->
                </div>