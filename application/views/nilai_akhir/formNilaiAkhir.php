<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>Input Nilai Akhir</h3>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td style="width: 30%;">Mata Pelajaran</td>
                                    <td>: <?= $mapel ?></td>
                                    <td style="width: 30%;">Semester</td>
                                    <td>: <?= $semester ?></td>
                                </tr>
                                <tr>
                                    <td style="width: 30%;">Rombel</td>
                                    <td>: <?= $tingkat . " - " . $rombel ?></td>
                                    <td style="width: 30%;">Tahun Ajar</td>
                                    <td>: <?= $tahun_ajar ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="table-responsive">
                        <table id="mytable" class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th>NIS</th>
                                    <th>Nama</th>
                                    <th>Nilai RPH</th>
                                    <th>Nilai PAT</th>
                                    <th>Nilai Akhir</th>
                                    <th>Predikat</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- sample modal content -->
<div class="modal fade modal-lg" id="exampleModal" role="dialog" style="margin-left: 300px;">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel1">Input Nilai Akhir</h4>
        </div>
        <div class="modal-body">
            <form class="form-material form-horizontal" method="POST" action="" enctype="multipart/form-data" id="form_nilai_akhir">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">NIS:</label>
                    <input type="hidden" class="form-control" name="id_nilai" id="id_nilai">
                    <input type="hidden" id="kode_kelas" name="kode_kelas" value="<?= $kode_kelas ?>">
                    <input type="hidden" id="kode_mapel" name="kode_mapel" value="<?= $kode_mapel ?>">
                    <input type="text" class="form-control" name="id_siswa" id="id_siswa" readonly>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Nama:</label>
                    <input type="text" class="form-control" id="nama" readonly>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Nilai PAT:</label>
                    <input type="text" class="form-control" name="uas" id="uas">
                    <div class="uas-error"></div>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Penilaian Deskripsi:</label>
                    <textarea class="form-control" rows="5" name="deskripsi_nilai_akhir" style="padding-top: 10px;"></textarea>
                    <div class="deskripsi_nilai_akhir-error"></div>
                </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="btn_batal">Close</button>
            <button type="button" class="btn btn-primary" id="btn_simpan_nilai">Simpan</button>
        </div>
        </form>
    </div>
</div>
<!-- /.modal -->

<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#mytable").dataTable({
            "processing": true,
            "serverSide": true,
            "oLanguage": {
                sProcessing: "Loading. . ."
            },
            "ajax": {
                "url": "<?= site_url('ControllerNilaiAkhir/json_input_nilai') ?>",
                "type": "POST",
                "data": function(d) {
                    d.kode_kelas = $('#kode_kelas').val();
                    d.kode_mapel = $('#kode_mapel').val();
                }
            },
            "columns": [{
                    "data": "nis",
                    "orderable": false,
                    "className": "text-center"
                },
                {
                    "data": "nis",
                },
                {
                    "data": "nama_lengkap",
                },
                {
                    "data": "uts",
                    "render": function(data, type, row, meta) {
                        if (data == 0) {
                            return '<span class="label label-danger label-rouded">Kosong</label>';
                        } else if (data != null) {
                            // var bobot_rph = parseInt(row.bobot_rph);
                            // var bagi_bobot = bobot_rph / 100;
                            var uh1 = parseInt(row.uh1);
                            var uh2 = parseInt(row.uh2);
                            var uh3 = parseInt(row.uh3);
                            var uh4 = parseInt(row.uh4);
                            var uh5 = parseInt(row.uh5);
                            var uh6 = parseInt(row.uh6);
                            var uts = parseInt(row.uts);
                            var total_rph = uh1 + uh2 + uh3 + uh4 + uh5 + uh6 + uts;
                            var jumlah_rph = total_rph / 7;
                            // var hasil_rph = bagi_bobot * Math.round(jumlah_rph); 
                            return Math.round(jumlah_rph);
                        } else {
                            return '<span class="label label-danger label-rouded">Kosong</label>';
                        }
                    }
                },
                {
                    "data": "uas",
                    "render": function(data, type, row, meta) {
                        if (data == 0) {
                            return '<span class="label label-danger label-rouded">Kosong</label>';
                        } else if (data != null) {
                            return data;
                        } else {
                            return '<span class="label label-danger label-rouded">Kosong</label>';
                        }
                    }
                },
                {
                    "data": "nilai_akhir",
                    "render": function(data, type, row, meta) {
                        if (data == 0) {
                            return '<span class="label label-danger label-rouded">Kosong</label>';
                        } else if (data != null) {
                            return data;
                        } else {
                            return '<span class="label label-danger label-rouded">Kosong</label>';
                        }
                    }
                },
                {
                    "data": "predikat_akhir",
                    "render": function(data, type, row, meta) {
                        if (data == 0) {
                            return '<span class="label label-danger label-rouded">Kosong</label>';
                        } else if (data != null) {
                            return data;
                        } else {
                            return '<span class="label label-danger label-rouded">Kosong</label>';
                        }
                    }
                },
                {
                    "data": "action",
                    "orderable": false,
                    "className": "text-center"
                }
            ],
            order: [
                [0, 'desc']
            ],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });

        $('#mytable').on('click', '.btn_input_nilai', function(e) {
            $('#exampleModal').modal('show');
            // var tingkat = $(this).data('tingkat');
            // var rombel = $(this).data('rombel');
            var id_siswa = $(this).data('id_siswa');
            var id_nilai = $(this).data('id_nilai');
            var nama = $(this).data('nama');

            // $('#rombel').val(tingkat + " " + rombel);
            $('#id_nilai').val(id_nilai);
            $('#id_siswa').val(id_siswa);
            $('#nama').val(nama);
            // $('#kode_guru').val(kode_guru).prop("selected", true);
        });

        $('#mytable').on('click', '.btn_hitung_ulang', function(e) {
            var id_nilai = $(this).data('id_nilai');
            var id_mapel = $('#kode_mapel').val();

            $.ajax({
                data: {
                    id_nilai: id_nilai,
                    id_mapel: id_mapel
                },
                type: "POST",
                dataType: "JSON",
                url: "<?= site_url('ControllerNilaiAkhir/hitung_ulang_nilai') ?>",
                success: function(hasil) {
                    if (hasil.status_alert == "sukses") {
                        t.fnDraw();
                    } else if (hasil.status_alert == "gagal") {

                        Swal.fire({
                            icon: 'error',
                            title: 'Silahkan lengkapi nilai harian, RPH masih kosong!',
                        });
                    }
                },
                error: function(hasil) {

                }
            })
        })

        $('#btn_batal').click(function() {
            location.reload();
        });

        $("#btn_simpan_nilai").click(function(e) {
            $.ajax({
                data: $('#form_nilai_akhir').serialize(),
                type: "POST",
                dataType: "JSON",
                url: "<?= site_url('ControllerNilaiAkhir/insert_nilai_akhir_action') ?>",
                success: function(hasil) {
                    if (hasil.status_alert == "sukses") {
                        t.fnDraw();
                        $("#exampleModal").modal('hide');
                        Swal.fire({
                            title: "Tambah data nilai",
                            text: "Berhasil",
                            icon: 'success',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'OK'
                        })

                    } else if (hasil.status_alert == "error") {

                        Swal.fire({
                            icon: 'error',
                            title: 'Data belum lengkap',
                        });

                        $('.uas-error').html(hasil.error_uas);
                        $('.deskripsi_nilai_akhir-error').html(hasil.error_deskripsi_nilai_akhir);
                    } else if (hasil.status_alert == "gagal") {
                        Swal.fire({
                            icon: 'error',
                            title: 'Silahkan lengkapi nilai harian, RPH masih kosong!',
                        });
                    } else if(hasil.status_alert == "validasi_bobot"){
                        Swal.fire({
                            icon: 'error',
                            title: 'Silahkan isi bobot terlebih dahulu, Bobot masih kosong!',
                        });
                    }


                },
                error: function(hasil) {
                    console.log(hasil);
                }
            })
        });

    });
</script>