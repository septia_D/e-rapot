<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default block1">
            <div class="panel-heading">
                <h3>Tambah Data Guru</h3>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <form class="form-material form-horizontal" method="POST" action="<?= $action; ?>" enctype="multipart/form-data">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-12" for="bdate">Nama Lengkap</span>
                                    </label>
                                    <div class="col-md-12">
                                        <input type="text" id="nama_lengkap" name="nama_lengkap" value="<?= $nama_lengkap; ?>" class="form-control" placeholder="Ketikkan nama lengkap">
                                        <input type="hidden" id="kode_guru" name="kode_guru" value="<?= $kode_guru; ?>" class="form-control">
                                        <span class="text-danger"><?= form_error('nama_lengkap') ?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12" for="bdate">NIP</span>
                                    </label>
                                    <div class="col-md-12">
                                        <input type="text" id="nip" name="nip" value="<?= $nip; ?>" class="form-control" placeholder="Ketikkan nip">
                                        <span class="text-danger"><?= form_error('nip') ?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12" for="bdate">NUPTK</span>
                                    </label>
                                    <div class="col-md-12">
                                        <input type="number" id="nuptk" name="nuptk" value="<?= $nuptk; ?>" class="form-control" placeholder="Ketikkan nuptk">
                                        <span class="text-danger"><?= form_error('nuptk') ?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-12">Alamat</label>
                                    <div class="col-md-12">
                                        <textarea class="form-control" name="alamat" id="alamat" rows="5" placeholder="Ketikkan alamat"><?= $alamat ?></textarea>
                                        <span class="text-danger"><?= form_error('alamat') ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-12">Jenis Kelamin</label>
                                    <div class="col-sm-12">
                                        <select class="form-control" name="jenis_kelamin">
                                            <option value="" selected disabled>--Pilih--</option>
                                            <option value="L" <?= ($jenis_kelamin == "L") ? "selected" : ""; ?>>Laki-laki</option>
                                            <option value="P" <?= ($jenis_kelamin == "P") ? "selected" : ""; ?>>Perempuan</option>
                                        </select>
                                        <span class="text-danger"><?= form_error('jenis_kelamin') ?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-12">Agama</label>
                                    <div class="col-sm-12">
                                        <select class="form-control" name="agama">
                                            <option value="" selected disabled>--Pilih--</option>
                                            <option value="islam" <?= ($agama == "islam") ? "selected" : ""; ?>>Islam</option>
                                            <option value="kristen" <?= ($agama == "kristen") ? "selected" : ""; ?>>Kristen</option>
                                            <option value="katolik" <?= ($agama == "katolik") ? "selected" : ""; ?>>Katolik</option>
                                            <option value="hindu" <?= ($agama == "hindu") ? "selected" : ""; ?>>Hindu</option>
                                            <option value="budha" <?= ($agama == "budha") ? "selected" : ""; ?>>Budha</option>
                                            <option value="konghucu" <?= ($agama == "konghucu") ? "selected" : ""; ?>>Kong Hu Cu</option>
                                        </select>
                                        <span class="text-danger"><?= form_error('agama') ?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-12" for="photo">Foto (Resolusi 800x500)</label>
                                    <div class="col-sm-12">
                                        <?php
                                        if ($foto == "") {
                                            echo "<p class='help-block'>Silahkan upload foto guru </p>";
                                        } else {
                                        ?>
                                            <div>
                                                <img src="<?php echo base_url() ?>images/<?php echo $foto; ?>">
                                            </div><br />
                                        <?php
                                        }
                                        ?>
                                        <input type="file" name="foto" id="foto">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-12">Password</label>
                                    <div class="co-md-12">
                                        <input type="password" class="form-control" name="password" value="<?= $password ?>" placeholder="ketikkan Password">
                                    </div>
                                    <span class="text-danger"><?= form_error('password') ?></span>
                                </div>
                                <div class="form-group">
                                    <label for="">Konfirmasi Password</label>
                                    <div class="co-md-12">
                                        <input type="password" class="form-control" name="confirm_password" value="<?= $confirm_password ?>" placeholder="Konfirmasi Password">
                                    </div>
                                    <span class="text-danger"><?= form_error('confirm_password') ?></span>
                                </div>

                            </div>
                        </div>
                        <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">Simpan</button>
                        <a type="button" href="<?= site_url('controllerGuru'); ?>" class="btn btn-inverse waves-effect waves-light">Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>