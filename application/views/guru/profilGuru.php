<div class="row">
    <div class="col-md-2 col-xs-12">
        <div class="white-box">
            <div class="user-bg"> <img width="100%" height="100%" alt="user" src="<?= base_url('images/') . $guru->foto ?>"> </div>
        </div>
    </div>

    <div class="col-sm-9">
        <div class="panel panel-default block1">
            <div class="panel-heading">
                <h3>Profil Guru</h3>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td style="width: 30%;">Nama Lengkap</td>
                                    <td>:&nbsp;&nbsp; <?= $guru->nama_lengkap; ?></td>
                                </tr>
                                <tr>
                                    <td style="width: 30%;">NIP</td>
                                    <td>:&nbsp;&nbsp; <?= $guru->nip; ?></td>
                                </tr>
                                <tr>
                                    <td style="width: 30%;">NUPTK</td>
                                    <td>:&nbsp;&nbsp; <?= $guru->nuptk; ?></td>
                                </tr>
                                <tr>
                                    <?php
                                    if ($guru->jenis_kelamin == "L") {
                                        $kelamin = "Laki-laki";
                                    } else {
                                        $kelamin = "Perempuan";
                                    }
                                    ?>
                                    <td style="width: 30%;">Jenis Kelamin</td>
                                    <td>:&nbsp;&nbsp; <?= $kelamin; ?></td>
                                </tr>
                                <tr>
                                    <td style="width: 30%;">Agama</td>
                                    <td>:&nbsp;&nbsp; <?= $guru->agama; ?></td>
                                </tr>
                                <tr>
                                    <td style="width: 30%;">Alamat</td>
                                    <td>:&nbsp;&nbsp; <?= $guru->alamat; ?></td>
                                </tr>
                                <tr>
                                    <td style="width: 30%;"></td>
                                    <td>
                                        <button type="button" class="btn btn-info" data-toggle="modal" data-target=".bs-example-modal-lg"> <i class="fa fa-pencil"></i> Edit</button>
                                        <a type="button" href="<?= site_url('controllerGuru'); ?>" class="btn btn-inverse waves-effect waves-light">Cancel</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- sample modal content -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title" id="myLargeModalLabel">Edit Guru</h3>
            </div>
            <div class="modal-body">
                <form class="form-material form-horizontal" method="POST" action="<?= site_url("controllerGuru/edit_guru_action"); ?>" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-md-12" for="bdate">Nama Lengkap</span>
                        </label>
                        <div class="col-md-12">
                            <input type="text" id="nama_lengkap" name="nama_lengkap" value="<?= $guru->nama_lengkap; ?>" class="form-control" placeholder="Ketikkan nama lengkap">
                            <input type="hidden" id="kode_guru" name="kode_guru" value="<?= $guru->kode_guru; ?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12" for="bdate">NIP</span>
                        </label>
                        <div class="col-md-12">
                            <input type="number" id="nip" name="nip" value="<?= $guru->nip; ?>" class="form-control" placeholder="Ketikkan nip">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12" for="bdate">NUPTK</span>
                        </label>
                        <div class="col-md-12">
                            <input type="number" id="nuptk" name="nuptk" value="<?= $guru->nuptk; ?>" class="form-control" placeholder="Ketikkan nuptk">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-12">Jenis Kelamin</label>
                        <div class="col-sm-12">
                            <select class="form-control" name="jenis_kelamin">
                                <option value="" selected disabled>--Pilih--</option>
                                <option value="L" <?= ($guru->jenis_kelamin == "L") ? "selected" : ""; ?>>Laki-laki</option>
                                <option value="P" <?= ($guru->jenis_kelamin == "P") ? "selected" : ""; ?>>Perempuan</option>
                            </select>
                            <span class="text-danger"><?= form_error('jenis_kelamin') ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-12">Agama</label>
                        <div class="col-sm-12">
                            <select class="form-control" name="agama">
                                <option value="" selected disabled>--Pilih--</option>
                                <option value="islam" <?= ($guru->agama == "islam") ? "selected" : ""; ?>>Islam</option>
                                <option value="kristen" <?= ($guru->agama == "kristen") ? "selected" : ""; ?>>Kristen</option>
                                <option value="katolik" <?= ($guru->agama == "katolik") ? "selected" : ""; ?>>Katolik</option>
                                <option value="hindu" <?= ($guru->agama == "hindu") ? "selected" : ""; ?>>Hindu</option>
                                <option value="budha" <?= ($guru->agama == "budha") ? "selected" : ""; ?>>Budha</option>
                                <option value="konghucu" <?= ($guru->agama == "konghucu") ? "selected" : ""; ?>>Kong Hu Cu</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-md-12">Alamat</label>
                        <div class="col-md-12">
                            <textarea class="form-control" name="alamat" id="alamat" rows="5" placeholder="Ketikkan alamat"><?= $guru->alamat ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-md-12">Password Baru</label>
                        <div class="col-md-12">
                            <input type="hidden" class="form-control" name="id_user" value="<?= $user->id ?>">
                            <input type="password" class="form-control" name="password" value="" placeholder="ketikkan Password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-12" for="photo">Foto (Resolusi 800x500)</label>
                        <div class="col-sm-12">
                            <?php
                            if ($guru->foto == "") {
                                echo "<p class='help-block'>Silahkan upload foto guru </p>";
                            } else {
                            ?>
                                <div>
                                    <img style="width: 50px;" src="<?php echo base_url() ?>images/<?php echo $guru->foto; ?>">
                                </div><br />
                            <?php
                            }
                            ?>
                            <input type="file" name="foto" id="foto">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">Update</button>
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->