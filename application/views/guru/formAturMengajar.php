<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default block1">
            <div class="panel-heading"><h3>Atur Mengajar Guru</h3></div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <form class="form-material form-horizontal" method="POST" action="<?=$action; ?>" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="col-md-12" for="bdate">Nama Lengkap</span>
                            </label>
                            <div class="col-md-12">
                                <input type="hidden" id="kode_guru" name="kode_guru" value="<?= $kode_guru; ?>" class="form-control" readonly>
                                <input type="text" id="nama_lengkap" name="nama_lengkap" value="<?= $nama_lengkap; ?>" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-12">Mata Pelajaran</label>
                            <div class="col-sm-12">
                                <select class="form-control select2" name="kode_mapel">
                                    <option value="" selected disabled>--Pilih--</option>
                                    <?php foreach($kode_makul as $value) : ?>
                                        <option value="<?= $value->kode_mapel ?>"><?= $value->nama_mapel; ?> - Tingkat <?= $value->tingkat; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>                      
                        
                        <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">Simpan</button>
                        <a type="button" href="<?= site_url('controllerGuru'); ?>" class="btn btn-inverse waves-effect waves-light">Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default block1">
            <div class="panel-heading"><h3>Mengajar</h3></div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                        <div class="table-responsive">
                            <table id="mytable_mengajar" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th>Mata Pelajaran</th>
                                        <th>Tingkat</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
        {
            return {
                      "iStart": oSettings._iDisplayStart,
                       "iEnd": oSettings.fnDisplayEnd(),
                       "iLength": oSettings._iDisplayLength,
                       "iTotal": oSettings.fnRecordsTotal(),
                       "iFilteredTotal": oSettings.fnRecordsDisplay(),
                       "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                       "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                    };
            };

            var t = $("#mytable_mengajar").dataTable({
                       "processing"  : true,
                       "serverSide"  : true,
                       "oLanguage"   : { sProcessing : "Loading. . ." },
                       "ajax"        : { "url" : "<?= site_url('controllerGuru/json_mengajar') ?>", "type": "POST", "data": function ( d ){
                            d.kode_guru = $('#kode_guru').val();
                       }},
                       "columns"     : [
                            {
                                "data": "kode_mengajar",
                                "orderable": false,
                                "className" : "text-center"
                            },
                            {
                                "data": "nama_mapel"
                            },
                            {
                                "data": "tingkat"
                            },
                            {
                                "data": "action",
                                "orderable": false,
                                "className" : "text-center"
                            }
                        ],
                        order: [[0, 'desc']],
                        rowCallback: function(row, data, iDisplayIndex) {
                            var info = this.fnPagingInfo();
                            var page = info.iPage;
                            var length = info.iLength;
                            var index = page * length + (iDisplayIndex + 1);
                            $('td:eq(0)', row).html(index);
                        }
                    });
    });
</script>

<script>
        $('#mytable_mengajar').on('click', '.hapus', function(e){

            event.preventDefault();
            const href = $(this).attr('href');

            Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Data akan dihapus!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya'
                }).then((result) => {
                if (result.value) {
                    document.location.href = href;
                }
            })
        });
</script>