<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default block1">
            <div class="panel-heading"><h3>Tambah Data Rombel</h3></div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <form class="form-material form-horizontal" method="POST" action="<?=$action; ?>" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="col-md-12" for="bdate">Tingkat</span>
                            </label>
                            <div class="col-md-12">
                                <input type="hidden" id="kode_kelas" name="kode_kelas" value="" class="form-control">
                                <?php
                                    $kelas = ['7','8','9']
                                ?>
                                <select class="form-control select2" name="tingkat">
                                    <option value="" selected disabled>--Pilih--</option>
                                    <?php foreach($kelas as $value) : ?>
                                        <option value="<?= $value ?>"><?= $value; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="text-danger"><?= form_error('tingkat') ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12" for="bdate">Rombel</span>
                            </label>
                            <div class="col-md-12">
                                <input type="hidden" id="kode_kelas" name="kode_kelas" value="" class="form-control">
                                <?php
                                    $kelas = ['A','B','C','D','E']
                                ?>
                                <select class="form-control select2" name="rombel">
                                    <option value="" selected disabled>--Pilih--</option>
                                    <?php foreach($kelas as $value) : ?>
                                        <option value="<?= $value ?>"><?= $value; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="text-danger"><?= form_error('rombel') ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12" for="bdate">Wali Kelas</span>
                            </label>
                            <div class="col-md-12">
                                <select class="form-control select2" name="kode_guru">
                                    <option value="" selected disabled>--Pilih--</option>
                                    <?php foreach($listGuru as $value) : ?>
                                        <option value="<?= $value->nuptk ?>"><?= $value->nuptk; ?> - <?= $value->nama_lengkap; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="text-danger"><?= form_error('kode_guru') ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12" for="bdate">Tahun Ajar</span>
                            </label>
                            <div class="col-md-12">
                                <select class="form-control select2" name="tahun_ajar">
                                    <option value="" selected disabled>--Pilih--</option>
                                    <?php foreach($listTahunAjar as $value) : ?>
                                        <option value="<?= $value->nama_tahunajar ?>" <?= ($this->session->konfigurasi['tahun_ajar'] ==  $value->nama_tahunajar) ? "selected" : ""; ?>><?= $value->nama_tahunajar; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="text-danger"><?= form_error('tahun_ajar') ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12" for="bdate">Semester</span>
                            </label>
                            <div class="col-md-12">
                                <select class="form-control select2" name="semester">
                                    <option value="" selected disabled>--Pilih--</option>
                                    <?php foreach($listSemester as $value) : ?>
                                        <option value="<?= $value->nama_semester ?>" <?= ($this->session->konfigurasi['semester'] ==  $value->nama_semester) ? "selected" : ""; ?>><?= $value->nama_semester; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="text-danger"><?= form_error('semester') ?></span>
                            </div>
                        </div>
                        
                        <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">Simpan</button>
                        <a type="button" href="<?= site_url('controllerKelas'); ?>" class="btn btn-inverse waves-effect waves-light">Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>