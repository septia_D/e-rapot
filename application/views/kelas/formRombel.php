<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default block1">
            <div class="panel-heading"><h3>Masukkan Siswa</h3></div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <form class="form-material form-horizontal" method="POST" action="<?= $action ?>" enctype="multipart/form-data">
                        
                        <div class="form-group">
                            <label class="col-md-12" for="bdate">Siswa</span>
                            </label>
                            <div class="col-md-12">
                                <input type="hidden" id="kode_kelas" name="kode_kelas" value="<?= $kode_kelas; ?>" class="form-control">
                                <select class="form-control select2" name="nis">
                                    <option value="" selected disabled>--Pilih--</option>
                                    <?php foreach($listSiswa as $value) : ?>
                                        <option value="<?= $value->nis ?>"><?= $value->nis; ?> - <?= $value->nama_lengkap; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="text-danger"><?= form_error('nis') ?></span>
                            </div>
                        </div>
                        
                        <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">Simpan</button>
                        <a type="button" href="<?= site_url('controllerKelas'); ?>" class="btn btn-inverse waves-effect waves-light">Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default block1">
            <div class="panel-heading"><h3>Kelas <?= $tingkat ?> <?= $rombel ?> - <?= $nama_lengkap ?></h3><h5>Jumlah siswa : <?= $jumlahSiswa['jmlh_siswa']  ?></h5></div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                        <div class="table-responsive">
                            <table id="mytable_kelas_siswa" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th>Nama Siswa</th>
                                        <th>NIS</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
        {
            return {
                      "iStart": oSettings._iDisplayStart,
                       "iEnd": oSettings.fnDisplayEnd(),
                       "iLength": oSettings._iDisplayLength,
                       "iTotal": oSettings.fnRecordsTotal(),
                       "iFilteredTotal": oSettings.fnRecordsDisplay(),
                       "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                       "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                    };
            };

            var t = $("#mytable_kelas_siswa").dataTable({
                       "processing"  : true,
                       "serverSide"  : true,
                       "oLanguage"   : { sProcessing : "Loading. . ." },
                       "ajax"        : { "url" : "<?= site_url('controllerKelas/json_aturKelas') ?>", "type": "POST", "data": function ( d ){
                            d.kode_kelas = $('#kode_kelas').val();
                       }},
                       "columns"     : [
                            {
                                "data": "kode_rombel",
                                "orderable": false,
                                "className" : "text-center"
                            },
                            {
                                "data": "nama_lengkap"
                            },
                            {
                                "data": "nis"
                            },
                            {
                                "data": "action",
                                "orderable": false,
                                "className" : "text-center"
                            }
                        ],
                        order: [[0, 'desc']],
                        rowCallback: function(row, data, iDisplayIndex) {
                            var info = this.fnPagingInfo();
                            var page = info.iPage;
                            var length = info.iLength;
                            var index = page * length + (iDisplayIndex + 1);
                            $('td:eq(0)', row).html(index);
                        }
                    });
    });
</script>

<script>
        $('#mytable_kelas_siswa').on('click', '.hapus', function(e){

            event.preventDefault();
            const href = $(this).attr('href');
            var nama_siswa = $(this).data('nama_siswa');

            Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Data " + nama_siswa + " akan dihapus!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya'
                }).then((result) => {
                if (result.value) {
                    document.location.href = href;
                }
            })
        });
</script>