<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default block1">
            <div class="panel-heading">
                <h3>Rombongan Belajar</h3>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <div class="form-group">
                        <a type="button" href="<?= site_url("controllerKelas/insert_rombel"); ?>" class="btn btn-primary"> <i class="fa fa-plus"></i> Tambah</a>
                    </div>
                    <div class="table-responsive">
                        <table id="mytable_rombel" class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th>Rombel</th>
                                    <th>Wali Kelas</th>
                                    <th>Tahun Ajar</th>
                                    <th>Semester</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- sample modal content -->
<div class="modal fade modal-lg" id="exampleModal" role="dialog" style="margin-left: 300px;">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel1">Edit Wali Kelas</h4>
        </div>
        <div class="modal-body">
            <form class="form-material form-horizontal" method="POST" action="<?= site_url('ControllerKelas/edit_wali_kelas') ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Rombel:</label>
                    <input type="text" class="form-control" name="rombel" id="rombel">
                    <input type="hidden" class="form-control" name="kode_kelas" id="kode_kelas">
                    <input type="hidden" class="form-control" name="kode_guru_lama" id="kode_guru_lama">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Wali Kelas</label>
                    <select class="form-control select2" name="kode_guru" id="kode_guru">
                        <option value="" selected disabled>--Pilih--</option>
                        <?php foreach ($listGuru as $value) : ?>
                            <option value="<?= $value->nuptk ?>"><?= $value->nuptk; ?> - <?= $value->nama_lengkap; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        </form>
    </div>
</div>
<!-- /.modal -->

<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#mytable_rombel").dataTable({
            "processing": true,
            "serverSide": true,
            "oLanguage": {
                sProcessing: "Loading. . ."
            },
            "ajax": {
                "url": "<?= site_url('controllerKelas/json') ?>",
                "type": "POST"
            },
            "columns": [{
                    "data": "tingkat",
                    "orderable": false,
                    "className": "text-center"
                },
                {
                    "data" : "tingkat",
                    "render": function(data, type, row, meta) {
                        return row.tingkat + " - " + row.rombel;
                    }
                },
                {
                    "data": "nama_lengkap"
                },
                {
                    "data": "tahun_ajar"
                },
                {
                    "data": "semester"
                },
                {
                    "data": "action",
                    "orderable": false,
                    "className": "text-center"
                }
            ],
            order: [
                [0, 'desc']
            ],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>

<script>
    $('#mytable_rombel').on('click', '.hapus', function(e) {

        event.preventDefault();
        const href = $(this).attr('href');
        var tingkat = $(this).data('tingkat');
        var rombel = $(this).data('rombel');

        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Data  " + tingkat + " " + rombel + " akan dihapus!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya'
        }).then((result) => {
            if (result.value) {
                document.location.href = href;
            }
        })
    });

    $('#mytable_rombel').on('click', '.btn_edit_wali_kelas', function(e) {
        $('#exampleModal').modal('show');
        var tingkat = $(this).data('tingkat');
        var rombel = $(this).data('rombel');
        var kode_kelas = $(this).data('kode_kelas');
        var kode_guru = $(this).data('guru');

        $('#rombel').val(tingkat + " " + rombel);
        $('#kode_kelas').val(kode_kelas);
        $('#kode_guru_lama').val(kode_guru);
        $('#kode_guru').val(kode_guru).prop("selected", true);
    });
</script>