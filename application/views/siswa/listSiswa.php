<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default block1">
            <div class="panel-heading">
                <h3>Data Siswa</h3>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <div class="form-group">
                        <a type="button" href="<?= site_url("controllerSiswa/insert_siswa"); ?>" class="btn btn-primary"> <i class="fa fa-plus"></i> Tambah</a>
                        <button type="button" class="btn btn-secondary" id="buttonImport">Import Data</button>

                    </div>
                    <div class="table-responsive">
                        <table id="mytable_siswa" class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th>NIS</th>
                                    <th>Nama</th>
                                    <th>Rombel</th>
                                    <th>Diterima</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- sample modal content -->
<div class="modal fade modal-lg" id="exampleModal" role="dialog" style="margin-left: 300px;">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel1">Atur Kelas</h4>
        </div>
        <div class="modal-body">
            <form class="form-material form-horizontal" method="POST" action="<?= site_url('ControllerSiswa/atur_kelas_siswa') ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">NIS:</label>
                    <input type="text" class="form-control" name="nis" id="nis">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Nama Siswa:</label>
                    <input type="text" class="form-control" name="nama_siswa" id="nama_siswa">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Kelas</label>
                    <select class="form-control select2" name="kode_kelas" id="kode_kelas">
                        <option value="" selected disabled>--Pilih--</option>
                        <?php foreach ($rombel as $value) : ?>
                            <option value="<?= $value->kode_kelas ?>"><?= $value->tingkat; ?> <?= $value->rombel; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        </form>
    </div>
</div>
<!-- /.modal -->

<!-- sample modal content -->
<div class="modal fade modal-lg" id="importModal" role="dialog" style="margin-left: 300px;">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel1">Import Data Siswa</h4>
        </div>
        <div class="modal-body">
            <form action="<?= site_url('controllerSiswa/upload') ?>" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="">Ket :<span class="text-danger">Silahkan download template terlebih dahulu</span></label>
                </div>
                <div class="form-group" style="margin-top: -20px;">
                    <a type="button" href="<?= site_url("controllerSiswa/unduh_template"); ?>" class="btn btn-warning"> <i class="fa fa-download"></i> Template</a>
                </div>
                <div class="form-group">
                    <input type="file" name="file" class="form-control" required>
                </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-info">Import</button>
        </div>
        </form>
    </div>
    <!-- /.modal -->
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#mytable_siswa").dataTable({
            "processing": true,
            "serverSide": true,
            "oLanguage": {
                sProcessing: "Loading. . ."
            },
            "ajax": {
                "url": "<?= site_url('controllerSiswa/json') ?>",
                "type": "POST"
            },
            "columns": [{
                    "data": "nis",
                    "orderable": false,
                    "className": "text-center"
                },
                {
                    "data": "nis"
                },
                {
                    "data": "nama_lengkap"
                },
                {
                    "data": "rombel",
                    "render": function(data, type, row, meta) {
                        if (data != null || data == '') {
                            return row.tingkat + " - " + row.rombel;
                        } else {
                            return '<span class="label label-danger">Belum Memiliki Kelas</span>';
                        }
                    }
                },
                {
                    "data": "diterima"
                },
                {
                    "data": "action",
                    "orderable": false,
                    "className": "text-center"
                }
            ],
            order: [
                [0, 'desc']
            ],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>

<script>
    $('#buttonImport').click(function(e) {
        $('#importModal').modal('show');
    })

    $('#mytable_siswa').on('click', '.hapus', function(e) {

        event.preventDefault();
        const href = $(this).attr('href');
        var nama_siswa = $(this).data('nama_siswa');

        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Data " + nama_siswa + " akan dihapus!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya'
        }).then((result) => {
            if (result.value) {
                document.location.href = href;
            }
        })
    });

    $('#mytable_siswa').on('click', '.btn_atur_kelas', function(e) {
        $('#exampleModal').modal('show');
        var nama_siswa = $(this).data('siswa');
        var nis = $(this).data('nis');

        $('#nama_siswa').val(nama_siswa);
        $('#nis').val(nis);
    });
</script>