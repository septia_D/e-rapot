<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default block1">
            <div class="panel-heading">
                <h3>Rombongan Belajar</h3>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <div class="form-group">
                        <a type="button" href="<?= site_url("controllerKelas/insert_rombel"); ?>" class="btn btn-primary"> <i class="fa fa-plus"></i> Tambah</a>
                    </div>
                    <div class="table-responsive">
                        <table id="mytable_rombel" class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th>Rombel</th>
                                    <th>Wali Kelas</th>
                                    <th>Tahun Ajar</th>
                                    <th>Semester</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#mytable_rombel").dataTable({
            "processing": true,
            "serverSide": true,
            "oLanguage": {
                sProcessing: "Loading. . ."
            },
            "ajax": {
                "url": "<?= site_url('controllerKelas/json') ?>",
                "type": "POST"
            },
            "columns": [{
                    "data": "tingkat",
                    "orderable": false,
                    "className": "text-center"
                },
                {
                    "render": function(data, type, row, meta) {
                        return row.tingkat + " " + row.rombel;
                    }
                },
                {
                    "data": "nama_lengkap"
                },
                {
                    "data": "tahun_ajar"
                },
                {
                    "data": "semester"
                },
                {
                    "data": "action",
                    "orderable": false,
                    "className": "text-center"
                }
            ],
            order: [
                [0, 'desc']
            ],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>

<script>
    $('#mytable_rombel').on('click', '.hapus', function(e) {

        event.preventDefault();
        const href = $(this).attr('href');

        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Data akan dihapus!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya'
        }).then((result) => {
            if (result.value) {
                document.location.href = href;
            }
        })
    });
</script>