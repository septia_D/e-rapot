<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default block1">
            <div class="panel-heading">
                <h3>Tambah Data Siswa</h3>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <form class="form-material form-horizontal" method="POST" action="<?= $action; ?>" enctype="multipart/form-data">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-12" for="bdate">NIS</span>
                                </label>
                                <div class="col-md-12">
                                    <input type="text" id="nis" name="nis" value="<?= $nis; ?>" class="form-control" placeholder="Ketikkan nis">
                                    <span class="text-danger"><?= form_error('nis') ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12" for="bdate">Nama Lengkap</span>
                                </label>
                                <div class="col-md-12">
                                    <input type="text" id="nama_lengkap" name="nama_lengkap" value="<?= $nama_lengkap; ?>" class="form-control" placeholder="Ketikkan nama lengkap">
                                    <span class="text-danger"><?= form_error('nama_lengkap') ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12" for="bdate">Tempat Lahir</span>
                                </label>
                                <div class="col-md-12">
                                    <input type="text" id="tempat_lahir" name="tempat_lahir" value="<?= $tempat_lahir; ?>" class="form-control" placeholder="Ketikkan tempat lahir">
                                    <span class="text-danger"><?= form_error('tempat_lahir') ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12" for="bdate">Tanggal Lahir</span>
                                </label>
                                <div class="col-md-12">
                                    <input type="text" id="datepicker" name="tanggal_lahir" value="<?= $tanggal_lahir; ?>" class="form-control" placeholder="Ketuk tanggal lahir">
                                    <span class="text-danger"><?= form_error('tanggal_lahir') ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-md-12">Alamat</label>
                                <div class="col-md-12">
                                    <textarea class="form-control" name="alamat" id="alamat" rows="5" placeholder="Ketikkan alamat"><?= $alamat ?></textarea>
                                    <span class="text-danger"><?= form_error('alamat') ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-12" for="bdate">NISN</span>
                                </label>
                                <div class="col-md-12">
                                    <input type="text" id="nisn" name="nisn" value="<?= $nisn; ?>" class="form-control" placeholder="Ketikkan nisn">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12">Jenis Kelamin</label>
                                <div class="col-sm-12">
                                    <select class="form-control" name="jenis_kelamin">
                                        <option value="" selected disabled>--Pilih--</option>
                                        <option value="L" <?= ($jenis_kelamin == "L") ? "selected" : ""; ?>>Laki-laki</option>
                                        <option value="P" <?= ($jenis_kelamin == "P") ? "selected" : ""; ?>>Perempuan</option>
                                    </select>
                                    <span class="text-danger"><?= form_error('jenis_kelamin') ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12">Agama</label>
                                <div class="col-sm-12">
                                    <select class="form-control" name="agama">
                                        <option value="" selected disabled>--Pilih--</option>
                                        <option value="islam" <?= ($agama == "islam") ? "selected" : ""; ?>>Islam</option>
                                        <option value="kristen" <?= ($agama == "kristen") ? "selected" : ""; ?>>Kristen</option>
                                        <option value="katolik" <?= ($agama == "katolik") ? "selected" : ""; ?>>Katolik</option>
                                        <option value="hindu" <?= ($agama == "hindu") ? "selected" : ""; ?>>Hindu</option>
                                        <option value="budha" <?= ($agama == "budha") ? "selected" : ""; ?>>Budha</option>
                                        <option value="konghucu" <?= ($agama == "konghucu") ? "selected" : ""; ?>>Kong Hu Cu</option>
                                    </select>
                                    <span class="text-danger"><?= form_error('agama') ?></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-12" for="bdate">Diterima</span>
                                </label>
                                <div class="col-md-12">
                                    <input type="text" id="datepicker" name="diterima" value="<?= $diterima; ?>" class="form-control" placeholder="Ketuk tanggal diterima">
                                    <span class="text-danger"><?= form_error('diterima') ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12" for="photo">Foto (Resolusi 800x500)</label>
                                <div class="col-sm-12">
                                    <?php
                                    if ($foto == "") {
                                        echo "<p class='help-block'>Silahkan upload foto siswa </p>";
                                    } else {
                                    ?>
                                        <div>
                                            <img src="<?php echo base_url() ?>images/<?php echo $foto; ?>">
                                        </div><br />
                                    <?php
                                    }
                                    ?>
                                    <input type="file" name="foto" id="foto">
                                </div>
                            </div>

                        </div>

                        <div class="col-md-12">
                            <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">Simpan</button>
                            <a type="button" href="<?= site_url('controllerSiswa'); ?>" class="btn btn-inverse waves-effect waves-light">Cancel</a>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>