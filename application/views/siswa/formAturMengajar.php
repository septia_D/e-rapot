<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default block1">
            <div class="panel-heading"><h3>Atur Mengajar Guru</h3></div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <form class="form-material form-horizontal" method="POST" action="<?=$action; ?>" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="col-md-12" for="bdate">Nama Lengkap</span>
                            </label>
                            <div class="col-md-12">
                                <input type="hidden" id="kode_guru" name="kode_guru" value="<?= $kode_guru; ?>" class="form-control" readonly>
                                <input type="text" id="nama_lengkap" name="nama_lengkap" value="<?= $nama_lengkap; ?>" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-12">Mata Pelajaran</label>
                            <div class="col-sm-12">
                                <select class="form-control select2" name="kode_mapel">
                                    <option value="" selected disabled>--Pilih--</option>
                                    <?php foreach($kode_makul as $value) : ?>
                                        <option value="<?= $value->kode_mapel ?>"><?= $value->nama_mapel ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>                      
                        
                        <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">Simpan</button>
                        <a type="button" href="<?= site_url('controllerGuru'); ?>" class="btn btn-inverse waves-effect waves-light">Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>