<div class="row">
    <div class="col-md-2 col-xs-12">
        <div class="white-box">
            <div class="user-bg"> <img width="100%" height="100%" alt="user" src="<?= base_url('images/') . $siswa->foto ?>"> </div>
        </div>
    </div>

    <div class="col-sm-9">
        <div class="panel panel-default block1">
            <div class="panel-heading">
                <h3>Profil Siswa</h3>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td style="width: 30%;">Nama Lengkap</td>
                                    <td>:&nbsp;&nbsp; <?= $siswa->nama_lengkap; ?></td>
                                </tr>
                                <tr>
                                    <td style="width: 30%;">NIS</td>
                                    <td>:&nbsp;&nbsp; <?= $siswa->nis; ?></td>
                                </tr>
                                <tr>
                                    <td style="width: 30%;">NISN</td>
                                    <td>:&nbsp;&nbsp; <?= ($siswa->nisn) ? $siswa->nisn : "-"; ?></td>
                                </tr>
                                <tr>
                                    <td style="width: 30%;">Tempat Lahir</td>
                                    <td>:&nbsp;&nbsp; <?= $siswa->tempat_lahir; ?></td>
                                </tr>
                                <tr>
                                    <td style="width: 30%;">Tanggal Lahir</td>
                                    <td>:&nbsp;&nbsp; <?= $siswa->tanggal_lahir; ?></td>
                                </tr>
                                <tr>
                                    <?php
                                    if ($siswa->jenis_kelamin == "L") {
                                        $kelamin = "Laki-laki";
                                    } else {
                                        $kelamin = "Perempuan";
                                    }
                                    ?>
                                    <td style="width: 30%;">Jenis Kelamin</td>
                                    <td>:&nbsp;&nbsp; <?= $kelamin; ?></td>
                                </tr>
                                <tr>
                                    <td style="width: 30%;">Agama</td>
                                    <td>:&nbsp;&nbsp; <?= $siswa->agama; ?></td>
                                </tr>
                                <tr>
                                    <td style="width: 30%;">Alamat</td>
                                    <td>:&nbsp;&nbsp; <?= $siswa->alamat; ?></td>
                                </tr>
                                <tr>
                                    <td style="width: 30%;">Diterima</td>
                                    <td>:&nbsp;&nbsp; <?= $siswa->diterima; ?></td>
                                </tr>

                                <tr>
                                    <td style="width: 30%;"></td>
                                    <td>
                                        <button type="button" class="btn btn-info" data-toggle="modal" data-target=".bs-example-modal-lg"> <i class="fa fa-pencil"></i> Edit</button>
                                        <a type="button" href="<?= site_url('controllerSiswa'); ?>" class="btn btn-inverse waves-effect waves-light">Cancel</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- sample modal content -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title" id="myLargeModalLabel">Edit Guru</h3>
            </div>
            <div class="modal-body">
                <form class="form-material form-horizontal" method="POST" action="<?= site_url('ControllerSiswa/edit_siswa_action') ?>" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-md-12" for="bdate">NIS</span>
                        </label>
                        <div class="col-md-12">
                            <input type="text" id="nis" name="nis" value="<?= $siswa->nis; ?>" class="form-control" placeholder="Ketikkan nis">
                            <span class="text-danger"><?= form_error('nis') ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12" for="bdate">NISN</span>
                        </label>
                        <div class="col-md-12">
                            <input type="text" id="nisn" name="nisn" value="<?= $siswa->nisn; ?>" class="form-control" placeholder="Ketikkan nisn">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12" for="bdate">Nama Lengkap</span>
                        </label>
                        <div class="col-md-12">
                            <input type="text" id="nama_lengkap" name="nama_lengkap" value="<?= $siswa->nama_lengkap; ?>" class="form-control" placeholder="Ketikkan nama lengkap">
                            <span class="text-danger"><?= form_error('nama_lengkap') ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12" for="bdate">Tempat Lahir</span>
                        </label>
                        <div class="col-md-12">
                            <input type="text" id="tempat_lahir" name="tempat_lahir" value="<?= $siswa->tempat_lahir; ?>" class="form-control" placeholder="Ketikkan tempat lahir">
                            <span class="text-danger"><?= form_error('tempat_lahir') ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12" for="bdate">Tanggal Lahir</span>
                        </label>
                        <div class="col-md-12">
                            <input type="text" id="datepicker" name="tanggal_lahir" value="<?= $siswa->tanggal_lahir; ?>" class="form-control" placeholder="Ketuk tanggal lahir">
                            <span class="text-danger"><?= form_error('tanggal_lahir') ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-12">Jenis Kelamin</label>
                        <div class="col-sm-12">
                            <select class="form-control" name="jenis_kelamin">
                                <option value="" selected disabled>--Pilih--</option>
                                <option value="L" <?= ($siswa->jenis_kelamin == "L") ? "selected" : ""; ?>>Laki-laki</option>
                                <option value="P" <?= ($siswa->jenis_kelamin == "P") ? "selected" : ""; ?>>Perempuan</option>
                            </select>
                            <span class="text-danger"><?= form_error('jenis_kelamin') ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-12">Agama</label>
                        <div class="col-sm-12">
                            <select class="form-control" name="agama">
                                <option value="" selected disabled>--Pilih--</option>
                                <option value="islam" <?= ($siswa->agama == "islam") ? "selected" : ""; ?>>Islam</option>
                                <option value="kristen" <?= ($siswa->agama == "kristen") ? "selected" : ""; ?>>Kristen</option>
                                <option value="katolik" <?= ($siswa->agama == "katolik") ? "selected" : ""; ?>>Katolik</option>
                                <option value="hindu" <?= ($siswa->agama == "hindu") ? "selected" : ""; ?>>Hindu</option>
                                <option value="budha" <?= ($siswa->agama == "budha") ? "selected" : ""; ?>>Budha</option>
                                <option value="konghucu" <?= ($siswa->agama == "konghucu") ? "selected" : ""; ?>>Kong Hu Cu</option>
                            </select>
                            <span class="text-danger"><?= form_error('agama') ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-md-12">Alamat</label>
                        <div class="col-md-12">
                            <textarea class="form-control" name="alamat" id="alamat" rows="5" placeholder="Ketikkan alamat"><?= $siswa->alamat ?></textarea>
                            <span class="text-danger"><?= form_error('alamat') ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12" for="bdate">Diterima</span>
                        </label>
                        <div class="col-md-12">
                            <input type="text" id="datepicker" name="diterima" value="<?= $siswa->diterima; ?>" class="form-control" placeholder="Ketuk tanggal diterima">
                            <span class="text-danger"><?= form_error('diterima') ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-12" for="photo">Foto (Resolusi 800x500)</label>
                        <div class="col-sm-12">
                            <?php
                            if ($siswa->foto == "") {
                                echo "<p class='help-block'>Silahkan upload foto siswa </p>";
                            } else {
                            ?>
                                <div>
                                    <img style="width: 50px;" src="<?php echo base_url() ?>images/<?php echo $siswa->foto; ?>">
                                </div><br />
                            <?php
                            }
                            ?>
                            <input type="file" name="foto" id="foto">
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">Update</button>
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->