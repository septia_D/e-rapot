<div class="row">
    <div class="col-md-2 col-xs-12">
        <div class="white-box">
            <?php
            if (!empty($user->foto)) {
                $foto = $user->foto;
            } else {
                $foto = $user->foto_admin;
            }
            ?>
            <div class="user-bg"> <img width="100%" height="100%" alt="user" src="<?= base_url('images/') . $foto ?>"> </div>
        </div>
    </div>

    <div class="col-sm-9">
        <div class="panel panel-default block1">
            <div class="panel-heading">
                <h3>Profil Users</h3>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <?php if ($this->session->session_login['level'] == 'admin') { ?>
                                    <form method="POST" action="<?= site_url("ControllerHome/edit_admin_action"); ?>" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Username</label>
                                            <input type="text" class="form-control" name="username" id="exampleInputEmail1" placeholder="Enter Username" value="<?= $user->username ?>">
                                            <input type="hidden" class="form-control" name="id_guru" id="exampleInputEmail1" placeholder="Enter Username" value="<?= $user->id_guru ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Password Baru</label>
                                            <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password" required>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-12" for="photo">Foto (Resolusi 800x500)</label>
                                            <div class="col-sm-12">
                                                <?php
                                                if ($foto == "") {
                                                    echo "<p class='help-block'>Silahkan upload foto admin </p>";
                                                } else {
                                                ?>
                                                    <div>
                                                        <img style="width: 50px;" src="<?php echo base_url() ?>images/<?php echo $foto; ?>">
                                                    </div><br />
                                                <?php
                                                }
                                                ?>
                                                <input type="file" name="foto" id="foto">
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                    </form>
                                <?php } ?>

                                <?php if ($this->session->session_login['level'] == 'guru' || $this->session->session_login['level'] == 'wali_murid') { ?>
                                    <tr>
                                        <td style="width: 30%;">Username</td>
                                        <td>:&nbsp;&nbsp; <?= $user->username; ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 30%;">NUPTK</td>
                                        <td>:&nbsp;&nbsp; <?= ($user->nuptk) ? $user->nuptk : "-"; ?></td>
                                    </tr>
                                    <?php
                                    if ($user->jenis_kelamin == "L") {
                                        $kelamin = "Laki-laki";
                                    } else if ($user->jenis_kelamin == "P") {
                                        $kelamin = "Perempuan";
                                    } else {
                                        $kelamin = "-";
                                    }
                                    ?>
                                    <tr>
                                        <td style="width: 30%;">Jenis Kelamin</td>
                                        <td>:&nbsp;&nbsp; <?= $kelamin; ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 30%;">Agama</td>
                                        <td>:&nbsp;&nbsp; <?= $user->agama; ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 30%;">Alamat</td>
                                        <td>:&nbsp;&nbsp; <?= $user->alamat; ?></td>
                                    </tr>

                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>