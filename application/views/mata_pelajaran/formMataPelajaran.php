<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default block1">
            <div class="panel-heading">
                <h3>Tambah Data Mata Pelajaran</h3>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <form class="form-material form-horizontal" id="formInputMapel" method="POST" action="<?= $action; ?>">
                        <div class="form-group">
                            <label class="col-md-12" for="bdate">kode Mapel</span>
                            </label>
                            <div class="col-md-12">
                                <input type="text" id="kode_mapel" name="kode_mapel" value="<?= $kode_mapel; ?>" class="form-control" placeholder="Ketikkan kode mapel">
                                <span class="text-danger"><?= form_error('kode_mapel') ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-12">Tingkat</label>
                            <div class="col-sm-12">
                                <select class="form-control" name="tingkat">
                                    <option value="" selected disabled>--Pilih--</option>
                                    <option value="7" <?= ($tingkat == "7") ? "selected" : ""; ?>>7</option>
                                    <option value="8" <?= ($tingkat == "8") ? "selected" : ""; ?>>8</option>
                                    <option value="9" <?= ($tingkat == "9") ? "selected" : ""; ?>>9</option>
                                </select>
                                <span class="text-danger"><?= form_error('tingkat') ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12" for="bdate">Nama Mapel</span>
                            </label>
                            <div class="col-md-12">
                                <input type="text" id="nama_mapel" name="nama_mapel" value="<?= $nama_mapel; ?>" class="form-control" placeholder="Ketikkan nama mapel">
                                <span class="text-danger"><?= form_error('nama_mapel') ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12" for="bdate">Deskripsi Mapel</span>
                            </label>
                            <div class="col-md-12">
                                <textarea class="form-control" name="deskripsi" id="" cols="30" rows="10" placeholder="Ketikka deskripsi"><?= $deskripsi ?></textarea>
                                <span class="text-danger"><?= form_error('deskripsi') ?></span>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">Simpan</button>
                        <a type="button" href="<?= site_url('controllerMataPelajaran'); ?>" class="btn btn-inverse waves-effect waves-light">Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function() {

        function ubahTypeKodeMapel()
        {
            var action = $('#formInputMapel').attr('action');
            if(action == 'http://localhost/e_rapot/controllerMataPelajaran/edit_mataPelajaran_action'){
                $('#kode_mapel').prop('readonly',true);
            } else {
                $('#kode_mapel').prop('readonly',false);
            }
            
        }
        ubahTypeKodeMapel();

    });
</script>