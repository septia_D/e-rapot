<!--row -->
<div class="row">
    <div class="col-md-3 col-sm-6">
        <div class="white-box">
            <div class="r-icon-stats">
                <i class="ti-user bg-megna"></i>
                <div class="bodystate">
                    <h4><?= $jumlah_guru['count']; ?></h4>
                    <span class="text-muted">Guru</span>
                </div>
            </div>
        </div>
    </div>                    
    <div class="col-md-3 col-sm-6">
        <div class="white-box">
            <div class="r-icon-stats">
                <i class="ti-user bg-info"></i>
                <div class="bodystate">
                    <h4><?= $jumlah_siswa['count']; ?></h4>
                    <span class="text-muted">Siswa</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6">
        <div class="white-box">
            <div class="r-icon-stats">
                <i class="ti-plus bg-success"></i>
                <div class="bodystate">
                    <h4><?= $jumlah_ekstra['count']; ?></h4>
                    <span class="text-muted">Ekstrakulikuler</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6">
        <div class="white-box">
            <div class="r-icon-stats">
                <i class="ti-home bg-inverse"></i>
                <div class="bodystate">
                    <h4><?= $jumlah_rombel['count']; ?></h4>
                    <span class="text-muted">Rombel</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/row -->

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default block1">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <?php if(!empty($this->session->konfigurasi['semester'])){ ?>
                            <h2>Semester : <?= $this->session->konfigurasi['semester'] ?> - <?= $this->session->konfigurasi['tahun_ajar'] ?></h2>
                        <?php } ?>
                        <p><b>Selamat Datang Proktor Assalamu'alaikum.</b></p>
                        <?php
                            if($this->session->session_login['level'] == 'admin'){
                                $level = "Admin";
                            } else if($this->session->session_login['level'] == 'wali_murid'){
                                $level = "Wali Kelas";
                            } else if($this->session->session_login['level'] == 'guru'){
                                $level = "Guru";
                            } else {
                                $level = "";
                            }
                        ?>
                        <p>Anda login sebagai <b><?= $level; ?> 
                    <?php 
                        if($this->session->session_login['username'] != 'admin' || $this->session->session_login['level'] == 'wali_murid'){
                            if(!empty($wali_kelas)){
                                echo "- ". $wali_kelas->tingkat. " " .$wali_kelas->rombel;                 
                            }
                        }
                    ?>
                        </b></p>
                    </div>
            </div>
        </div>
    </div>
</div>
       
            
