<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default block1">
            <div class="panel-heading">
                <h3>Bobot dan KKM Mata Pelajaran</h3>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="mytable_bobot" class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th>Kode Mata Pelajaran</th>
                                    <th>Tingkat</th>
                                    <th>Mata Pelajaran</th>
                                    <th>KKM</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#mytable_bobot").dataTable({
            "processing": true,
            "serverSide": true,
            "oLanguage": {
                sProcessing: "Loading. . ."
            },
            "ajax": {
                "url": "<?= site_url('ControllerBobot/json') ?>",
                "type": "POST"
            },
            "columns": [{
                    "data": "kode_mapel",
                    "orderable": false,
                    "className": "text-center"
                },
                {
                    "data": "kode_mapel"
                },
                {
                    "data": "tingkat"
                },
                {
                    "data": "nama_mapel"
                },
                {
                    "searching": false,
                    "data": "kkm",
                    "render": function(data, type, row, meta) {
                        if(data == null || data == ''){
                            return '<span class="label label-info label-rouded">Kosong</label>';
                        } else {
                            return row.kkm;
                        }
                    }
                },
                {
                    "data": "action",
                    "orderable": false,
                    "className": "text-center"
                }
            ],
            order: [
                [2, 'asc']
            ],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>

<script>
    $('#mytable_mapel').on('click', '.hapus', function(e) {

        event.preventDefault();
        const href = $(this).attr('href');
        var nama_mapel = $(this).data('nama_mapel');

        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Data " + nama_mapel + " akan dihapus!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya'
        }).then((result) => {
            if (result.value) {
                document.location.href = href;
            }
        })
    });
</script>