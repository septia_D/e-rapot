<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default block1">
            <div class="panel-heading">
                <h3>Bobot dan KKM Mata Pelajaran - <?= $mapel ?> - <?= $tingkat ?></h3>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <form method="POST" action="<?= $action; ?>">
                        <input type="hidden" class="form-control" name="kode_bobot_kkm" value="<?= $kode_bobot_kkm ?>">
                        <input type="hidden" class="form-control" name="kode_mapel" value="<?= $kode_mapel ?>">
                        <div class="row">
                            <div class="col-lg-12">
                                <h3>Bobot Penilaian Pengetahuan</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <label>Bobot RPH</label>
                                <div class="input-group">
                                    <input type="number" class="form-control" name="bobot_rph" id="bobot_rph" value="<?= $bobot_rph ?>">
                                    <span class="input-group-addon" id="basic-addon2">%</span>
                                </div>
                                <span class="text-danger"><?= form_error('bobot_rph') ?></span>

                            </div>
                            <div class="col-lg-4">
                                <label>Bobot PAT</label>
                                <div class="input-group">
                                    <input type="number" class="form-control" name="bobot_pat" id="bobot_pat" value="<?= $bobot_pat ?>">
                                    <span class="input-group-addon" id="basic-addon2">%</span>
                                </div>
                                <span class="text-danger"><?= form_error('bobot_pat') ?></span>

                            </div>
                            <div class="col-lg-4">
                                <label>Total Bobot</label>
                                <div class="input-group">
                                    <input type="number" class="form-control" name="total_bobot" id="total_bobot" readonly value="<?= $total_bobot ?>">
                                    <span class="input-group-addon" id="basic-addon2">%</span>
                                </div>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-lg-12">
                                <p>*) Jumlah persentase <b>RPH</b> + <b>PAT</b> = 100 (dalam persen)</p>
                            </div>
                        </div><hr>
                        <div class="row">
                            <div class="col-lg-12">
                                <h3>Kriteria Ketuntasan Minimal</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <label>KKM (0-100)</label>
                                <div class="input-group">
                                    <input type="number" class="form-control" name="kkm" id="kkm" value="<?= $kkm ?>">
                                    <span class="input-group-addon" id="basic-addon2">%</span>
                                </div>
                                <span class="text-danger"><?= form_error('kkm') ?></span>
                            </div>
                        </div><hr><br><br>

                        <button type="submit" class="btn btn-info waves-effect waves-light m-r-10 pull-right">Simpan</button>
                        <a type="button" href="<?= site_url('controllerBobot'); ?>" class="btn btn-inverse waves-effect waves-light">Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {

        $('#bobot_rph').change(function() {
            var bobot_rph = parseInt($('#bobot_rph').val());
            var bobot_pat = parseInt($('#bobot_pat').val());
            var total_bobot_ujian = bobot_rph + bobot_pat;
            $('#total_bobot').val(total_bobot_ujian);
        });

        $('#bobot_pat').change(function() {
            var bobot_rph = parseInt($('#bobot_rph').val());
            var bobot_pat = parseInt($('#bobot_pat').val());
            var total_bobot_ujian = bobot_rph + bobot_pat;
            if (total_bobot_ujian == 100) {
                $('#total_bobot').val(total_bobot_ujian);
            } else {
                $('#total_bobot').val(total_bobot_ujian);
                Swal.fire({
                    icon: 'error',
                    title: 'Info',
                    text: 'Bobot penilaian pengetahuan belum mencapai 100 %',
                });
            }
        });

        $('#bobot_praktik').change(function() {
            var bobot_praktik    = parseInt($('#bobot_praktik').val());
            var bobot_portofolio = parseInt($('#bobot_portofolio').val());
            var bobot_projek     = parseInt($('#bobot_projek').val());
            var total_bobot_ketrampilan = bobot_praktik + bobot_portofolio + bobot_projek;
            $('#total_bobot_ketrampilan').val(total_bobot_ketrampilan);
        });

        $('#bobot_portofolio').change(function() {
            var bobot_praktik    = parseInt($('#bobot_praktik').val());
            var bobot_portofolio = parseInt($('#bobot_portofolio').val());
            var bobot_projek     = parseInt($('#bobot_projek').val());
            var total_bobot_ketrampilan = bobot_praktik + bobot_portofolio + bobot_projek;
            $('#total_bobot_ketrampilan').val(total_bobot_ketrampilan);
        });

        $('#bobot_projek').change(function() {
            var bobot_praktik    = parseInt($('#bobot_praktik').val());
            var bobot_portofolio = parseInt($('#bobot_portofolio').val());
            var bobot_projek     = parseInt($('#bobot_projek').val());
            var total_bobot_ketrampilan = bobot_praktik + bobot_portofolio + bobot_projek;
            $('#total_bobot_ketrampilan').val(total_bobot_ketrampilan);
            if (total_bobot_ketrampilan == 100) {
                $('#total_bobot_ketrampilan').val(total_bobot_ketrampilan);
            } else {
                $('#total_bobot_ketrampilan').val(total_bobot_ketrampilan);
                Swal.fire({
                    icon: 'error',
                    title: 'Info',
                    text: 'Bobot penilaian ketrampilan belum mencapai 100 %',
                });
            }
        });

    });
</script>