<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default block1">
            <div class="panel-heading">
                <h3>Data Semester</h3>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <div class="form-group">
                        <a type="button" href="<?= site_url("controllerSemester/insert_semester"); ?>" class="btn btn-primary"> <i class="fa fa-plus"></i> Tambah</a>
                    </div>
                    <div class="table-responsive">
                        <table id="myTable" class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="10%">No</th>
                                    <th width="30%">Semester</th>
                                    <th width="30%">Aktif</th>
                                    <th width="30%">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; ?>
                                <?php foreach ($semester as $value) : ?>
                                    <tr>
                                        <td><?= $no++; ?></td>
                                        <td><?= $value['nama_semester'] ?></td>
                                        <td><?= ($value['aktif'] == 'Y') ? "Ya" : "Tidak"; ?></td>
                                        <td>
                                            <a type="button" href="<?= site_url("controllerSemester/edit_semester/" . $value['kode_semester']); ?>" class="btn btn-success"> <i class="fa  fa-external-link"></i> Edit</a>
                                            <a type="button" href="<?= site_url("controllerSemester/hapus_semester/" . $value['kode_semester']); ?>" class="btn btn-danger hapus"> <i class="fa   fa-archive"></i> Hapus</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>