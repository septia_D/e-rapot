<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default block1">
            <div class="panel-heading"><h3>Tambah Data Semester</h3></div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <form class="form-material form-horizontal" method="POST" action="<?=$action; ?>">
                        <div class="form-group">
                            <label class="col-md-12" for="bdate">Semester</span>
                            </label>
                            <div class="col-md-12">
                                <input type="hidden" id="kode_semester" name="kode_semester" value="<?= $kode_semester; ?>">
                                <input type="text" id="nama_semester" name="nama_semester" value="<?= $nama_semester; ?>" class="form-control" placeholder="Ketikkan nama semester">
                                <span class="text-danger"><?= form_error('nama_semester') ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-12">Aktif</label>
                            <div class="col-sm-12">
                                <select class="form-control" name="aktif">
                                    <option value="" selected disabled>--Pilih--</option>
                                    <option value="Y" <?= ($aktif == "Y") ? "selected" : ""; ?>>Ya</option>
                                    <option value="T" <?= ($aktif == "T") ? "selected" : ""; ?>>Tidak</option>
                                </select>
                                <span class="text-danger"><?= form_error('aktif') ?></span>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">Simpan</button>
                        <a type="button" href="<?= site_url('controllerSemester'); ?>" class="btn btn-inverse waves-effect waves-light">Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>