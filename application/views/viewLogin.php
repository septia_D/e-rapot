<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>theme_admin/plugins/images/favicon.png">
    <title>Halaman Login</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url() ?>theme_admin/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>theme_admin/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="<?= base_url() ?>theme_admin/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?= base_url() ?>theme_admin/css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?= base_url() ?>theme_admin/css/colors/default.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

<![endif]-->

    <style>
        .login-register {
            background: url('<?= base_url("images/login-register.jpeg") ?>') center center/cover no-repeat !important;
            height: 100%;
            position: fixed;
        }
    </style>
</head>

<body>
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <section id="wrapper" class="login-register">
        <div class="login-box login-sidebar">
            <div class="white-box">
                <form class="form-horizontal form-material" id="loginform" action="<?= base_url('controllerLogin/cekStatusLogin'); ?>" method="POST">
                    <a href="javascript:void(0)" class="text-center db"><img src="<?= base_url() ?>images/logo.png" alt="Home" style="width:100px; margin-bottom:5px;" /><br /><b>MTS Muhammadiyah Wangon</b></a><br>

                    <?php if (!empty($this->session->userdata('pesan'))) { ?>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <div class="alert alert-danger"><?= $this->session->userdata('pesan'); ?></div>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="form-group">
                        <label class="col-md-12">Username</label>
                        <div class="col-xs-12">
                            <input class="form-control" name="username" type="text" placeholder="Ketikkan Username">
                            <span class="text-danger"><?= form_error('username') ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Password</label>
                        <div class="col-xs-12">
                            <input class="form-control" id="password" name="password" type="password" placeholder="Ketikkan Password">
                            <span class="text-danger"><?= form_error('password') ?></span>
                        </div>
                    </div>
                    <div class="form-check" style="margin-top: -20px;">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="showPassword">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Show Password</span>
                        </label>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Login Masuk</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </section>
    <!-- jQuery -->
    <script src="<?= base_url() ?>theme_admin/plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?= base_url() ?>theme_admin/bootstrap/dist/js/tether.min.js"></script>
    <script src="<?= base_url() ?>theme_admin/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>theme_admin/plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="<?= base_url() ?>theme_admin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="<?= base_url() ?>theme_admin/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?= base_url() ?>theme_admin/js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?= base_url() ?>theme_admin/js/custom.min.js"></script>
    <!-- Sparkline chart JavaScript -->
    <script src="<?= base_url() ?>theme_admin/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
    <script src="<?= base_url() ?>theme_admin/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
    <!--Style Switcher -->
    <script src="<?= base_url() ?>theme_admin/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#showPassword').click(function() {
                if ($(this).is(':checked')) {
                    $('#password').attr('type', 'text');
                } else {
                    $('#password').attr('type', 'password');
                }
            });
        });
    </script>
</body>

</html>