<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default block1">
            <div class="panel-heading"><h3>Tambah Data Ekstrakulikuler</h3></div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <form class="form-material form-horizontal" method="POST" action="<?=$action; ?>">
                        <div class="form-group">
                            <label class="col-md-12" for="bdate">Ekstrakulikuler</span>
                            </label>
                            <div class="col-md-12">
                                <input type="text" id="nama_ekstra" name="nama_ekstra" value="<?= $nama_ekstra; ?>" class="form-control" placeholder="Ketikkan nama ekstra">
                                <input type="hidden" id="kode_ekstra" name="kode_ekstra" value="<?= $kode_ekstra; ?>" class="form-control">
                                <span class="text-danger"><?= form_error('nama_ekstra') ?></span>
                            </div>
                        </div>
                        
                        <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">Simpan</button>
                        <a type="button" href="<?= site_url('controllerEkstra'); ?>" class="btn btn-inverse waves-effect waves-light">Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>