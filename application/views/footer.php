            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2020 &copy; MTS Muhammadiyah Wangon </footer>
            </div>
            <!-- /#page-wrapper -->
            </div>
            <!-- /#wrapper -->
            <!-- jQuery -->
            <script src="<?= base_url() ?>plugins/bower_components/jquery/dist/jquery.min.js"></script>
            <!-- Bootstrap Core JavaScript -->
            <script src="<?= base_url() ?>theme_admin/bootstrap/dist/js/tether.min.js"></script>
            <script src="<?= base_url() ?>theme_admin/bootstrap/dist/js/bootstrap.min.js"></script>
            <script src="<?= base_url() ?>plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
            <!-- Menu Plugin JavaScript -->
            <script src="<?= base_url() ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
            <!--slimscroll JavaScript -->
            <script src="<?= base_url() ?>theme_admin/js/jquery.slimscroll.js"></script>
            <!--Wave Effects -->
            <script src="<?= base_url() ?>theme_admin/js/waves.js"></script>
            <!-- Custom Theme JavaScript -->
            <script src="<?= base_url() ?>theme_admin/js/custom.min.js"></script>
            <!--Style Switcher -->
            <script src="<?= base_url() ?>theme_admin/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

            <script src="<?php echo base_url(); ?>plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
            <!-- start - This is for export functionality only -->
            <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
            <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
            <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
            <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
            <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
            <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
            <!-- end - This is for export functionality only -->

            <!-- Date Picker Plugin JavaScript -->
            <script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>

            <script src="<?php echo base_url(); ?>plugins/sweetalert/package/dist/sweetalert2.all.min.js"></script>

            <script src="<?php echo base_url(); ?>plugins/bower_components/toast-master/js/jquery.toast.js"></script>

            <script>
                jQuery('.mydatepicker, #datepicker').datepicker({
                    autoclose: true,
                    todayHighlight: true,
                    format: 'yyyy-mm-dd'
                });
            </script>
            <?php if (($this->session->flashdata('flash_message')) != "") : ?>
                <script type="text/javascript">
                    $(document).ready(function() {
                        $.toast({
                            heading: 'Sukses!!!',
                            text: '<?php echo $this->session->flashdata('flash_message'); ?>',
                            position: 'top-right',
                            loaderBg: '#ff6849',
                            icon: 'success',
                            hideAfter: 3500,
                            stack: 6
                        })
                    });
                </script>
            <?php endif; ?>

            <?php if (($this->session->flashdata('error_message')) != "") : ?>
                <script type="text/javascript">
                    $(document).ready(function() {
                        $.toast({

                            text: '<?php echo $this->session->flashdata('error_message'); ?>',
                            position: 'top-right',
                            loaderBg: '#f56954',
                            icon: 'warning',
                            hideAfter: 3500,
                            stack: 6
                        })
                    });
                </script>
            <?php endif; ?>

            <script>
                $(document).ready(function() {
                    $('#myTable').DataTable();
                    $(document).ready(function() {
                        var table = $('#example').DataTable({
                            "columnDefs": [{
                                "visible": false,
                                "targets": 2
                            }],
                            "order": [
                                [2, 'asc']
                            ],
                            "displayLength": 25,
                            "drawCallback": function(settings) {
                                var api = this.api();
                                var rows = api.rows({
                                    page: 'current'
                                }).nodes();
                                var last = null;

                                api.column(2, {
                                    page: 'current'
                                }).data().each(function(group, i) {
                                    if (last !== group) {
                                        $(rows).eq(i).before(
                                            '<tr class="group"><td colspan="5">' + group + '</td></tr>'
                                        );

                                        last = group;
                                    }
                                });
                            }
                        });

                        // Order by the grouping
                        $('#example tbody').on('click', 'tr.group', function() {
                            var currentOrder = table.order()[0];
                            if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                                table.order([2, 'desc']).draw();
                            } else {
                                table.order([2, 'asc']).draw();
                            }
                        });
                    });
                });
                $('#example23').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                });
            </script>

            <!-- jQuery file upload -->
            <script>
                $(document).ready(function() {

                    $(".select2").select2({
                        placeholder: "Pilih",
                    });
                });
            </script>

            <script>
                $('.hapus').on('click', function(e) {

                    event.preventDefault();
                    const href = $(this).attr('href');

                    Swal.fire({
                        title: 'Apakah anda yakin?',
                        text: "Data akan dihapus!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya'
                    }).then((result) => {
                        if (result.value) {
                            document.location.href = href;
                        }
                    })
                });
            </script>

            </body>

            </html>