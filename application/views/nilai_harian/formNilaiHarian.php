<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>Input Nilai Harian</h3>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <input type="hidden" id="kode_kelas" value="<?= $kode_kelas ?>">
                    <input type="hidden" id="kode_mapel" value="<?= $kode_mapel ?>">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td style="width: 30%;">Mata Pelajaran</td>
                                    <td>: <?= $mapel ?></td>
                                    <td style="width: 30%;">Semester</td>
                                    <td>: <?= $semester ?></td>
                                </tr>
                                <tr>
                                    <td style="width: 30%;">Rombel</td>
                                    <td>: <?= $tingkat . " - " . $rombel ?></td>
                                    <td style="width: 30%;">Tahun Ajar</td>
                                    <td>: <?= $tahun_ajar ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="table-responsive">
                        <table id="mytable" class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th>NIS</th>
                                    <th>Nama</th>
                                    <th>UH 1</th>
                                    <th>UH 2</th>
                                    <th>UH 3</th>
                                    <th>UH 4</th>
                                    <th>UH 5</th>
                                    <th>UH 6</th>
                                    <th>UTS</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#mytable").dataTable({
            "processing": true,
            "serverSide": true,
            "oLanguage": {
                sProcessing: "Loading. . ."
            },
            "ajax": {
                "url": "<?= site_url('ControllerNilaiHarian/json_input_nilai') ?>",
                "type": "POST",
                "data": function(d) {
                    d.kode_kelas = $('#kode_kelas').val();
                    d.kode_mapel = $('#kode_mapel').val();
                }
            },
            "columns": [{
                    "data": "nis",
                    "orderable": false,
                    "className": "text-center"
                },
                {
                    "data": "nis",
                },
                {
                    "data": "nama_lengkap",
                },
                {
                    "data": "uh1",
                    "render": function(data, type, row, meta) {
                        if(data == 0){
                            return '<input class="form-control" type="text" value="" id="ph1" data-nis="'+ row.nis +'" data-kode_kelas="'+ row.kode_kelas +'">';
                        } else if(data != null){
                            return '<input class="form-control" type="text" value="'+ data +'" id="ph1" data-nis="'+ row.nis +'" data-kode_kelas="'+ row.kode_kelas +'">';
                        } else {
                            return '<input class="form-control" type="text" value="" id="ph1" data-nis="'+ row.nis +'" data-kode_kelas="'+ row.kode_kelas +'">';
                        }
                    }
                },
                {
                    "data": "uh2",
                    "render": function(data, type, row, meta) {
                        if(data == 0){
                            return '<input class="form-control" type="text" value="" id="ph2" data-nis="'+ row.nis +'" data-kode_kelas="'+ row.kode_kelas +'">';
                        } else if(data != null){
                            return '<input class="form-control" type="text" value="'+ data +'" id="ph2" data-nis="'+ row.nis +'" data-kode_kelas="'+ row.kode_kelas +'">';
                        } else {
                            return '<input class="form-control" type="text" value="" id="ph2" data-nis="'+ row.nis +'" data-kode_kelas="'+ row.kode_kelas +'">';
                        }
                    }
                },
                {
                    "data": "uh3",
                    "render": function(data, type, row, meta) {
                        if(data == 0){
                            return '<input class="form-control" type="text" value="" id="ph3" data-nis="'+ row.nis +'" data-kode_kelas="'+ row.kode_kelas +'">';
                        } else if(data != null){
                            return '<input class="form-control" type="text" value="'+ data +'" id="ph3" data-nis="'+ row.nis +'" data-kode_kelas="'+ row.kode_kelas +'">';
                        } else {
                            return '<input class="form-control" type="text" value="" id="ph3" data-nis="'+ row.nis +'" data-kode_kelas="'+ row.kode_kelas +'">';
                        }
                    }
                },
                {
                    "data": "uh4",
                    "render": function(data, type, row, meta) {
                        if(data == 0){
                            return '<input class="form-control" type="text" value="" id="ph4" data-nis="'+ row.nis +'" data-kode_kelas="'+ row.kode_kelas +'">';
                        } else if(data != null){
                            return '<input class="form-control" type="text" value="'+ data +'" id="ph4" data-nis="'+ row.nis +'" data-kode_kelas="'+ row.kode_kelas +'">';
                        } else {
                            return '<input class="form-control" type="text" value="" id="ph4" data-nis="'+ row.nis +'" data-kode_kelas="'+ row.kode_kelas +'">';
                        }
                    }
                },
                {
                    "data": "uh5",
                    "render": function(data, type, row, meta) {
                        if(data == 0){
                            return '<input class="form-control" type="text" value="" id="ph5" data-nis="'+ row.nis +'" data-kode_kelas="'+ row.kode_kelas +'">';
                        } else if(data != null){
                            return '<input class="form-control" type="text" value="'+ data +'" id="ph5" data-nis="'+ row.nis +'" data-kode_kelas="'+ row.kode_kelas +'">';
                        } else {
                            return '<input class="form-control" type="text" value="" id="ph5" data-nis="'+ row.nis +'" data-kode_kelas="'+ row.kode_kelas +'">';
                        }
                    }
                },
                {
                    "data": "uh6",
                    "render": function(data, type, row, meta) {
                        if(data == 0){
                            return '<input class="form-control" type="text" value="" id="ph6" data-nis="'+ row.nis +'" data-kode_kelas="'+ row.kode_kelas +'">';
                        } else if(data != null){
                            return '<input class="form-control" type="text" value="'+ data +'" id="ph6" data-nis="'+ row.nis +'" data-kode_kelas="'+ row.kode_kelas +'">';
                        } else {
                            return '<input class="form-control" type="text" value="" id="ph6" data-nis="'+ row.nis +'" data-kode_kelas="'+ row.kode_kelas +'">';
                        }
                    }
                },
                {
                    "data": "uts",
                    "render": function(data, type, row, meta) {
                        if(data == 0){
                            return '<input class="form-control" type="text" value="" id="uts" data-nis="'+ row.nis +'" data-kode_kelas="'+ row.kode_kelas +'">';
                        } else if(data != null){
                            return '<input class="form-control" type="text" value="'+ data +'" id="uts" data-nis="'+ row.nis +'" data-kode_kelas="'+ row.kode_kelas +'">';
                        } else {
                            return '<input class="form-control" type="text" value="" id="uts" data-nis="'+ row.nis +'" data-kode_kelas="'+ row.kode_kelas +'">';
                        }
                    }
                },
                
            ],
            order: [
                [0, 'desc']
            ],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });       

        $('#mytable').on('change', '#ph1', function (){
            var id_siswa = $(this).data('nis');
            var id_kelas = $(this).data('kode_kelas');
            var ph1      = $(this).val();
            var id_mapel = $('#kode_mapel').val();
            
            $.ajax({
                type: "POST",
                url : "<?= site_url('ControllerNilaiHarian/insert_nilai_harian') ?>",
                data : {
                    id_siswa : id_siswa,
                    id_kelas : id_kelas,
                    id_mapel : id_mapel,
                    ph1      : ph1,
                },
                dataType : "JSON",
                success: function(hasil){
                    if(hasil.info == 'sukses'){
                       t.fnDraw();
                    } else {
                        alert('Gagal input nilai harian');
                    }
                }
            })

        });

        $('#mytable').on('change', '#ph2', function (){
            var id_siswa = $(this).data('nis');
            var id_kelas = $(this).data('kode_kelas');
            var ph2      = $(this).val();
            var id_mapel = $('#kode_mapel').val();
            
            $.ajax({
                type: "POST",
                url : "<?= site_url('ControllerNilaiHarian/insert_nilai_harian') ?>",
                data : {
                    id_siswa : id_siswa,
                    id_kelas : id_kelas,
                    id_mapel : id_mapel,
                    ph2      : ph2,
                },
                dataType : "JSON",
                success: function(hasil){
                    if(hasil.info == 'sukses'){
                        t.fnDraw();

                    } else {
                        alert('Gagal input nilai harian');
                    }
                }
            })
        });

        $('#mytable').on('change', '#ph3', function (){
            var id_siswa = $(this).data('nis');
            var id_kelas = $(this).data('kode_kelas');
            var ph3      = $(this).val();
            var id_mapel = $('#kode_mapel').val();
            
            $.ajax({
                type: "POST",
                url : "<?= site_url('ControllerNilaiHarian/insert_nilai_harian') ?>",
                data : {
                    id_siswa : id_siswa,
                    id_kelas : id_kelas,
                    id_mapel : id_mapel,
                    ph3      : ph3,
                },
                dataType : "JSON",
                success: function(hasil){
                    if(hasil.info == 'sukses'){
                        t.fnDraw();

                    } else {
                        alert('Gagal input nilai harian');
                    }
                }
            })
        });

        $('#mytable').on('change', '#ph4', function (){
            var id_siswa = $(this).data('nis');
            var id_kelas = $(this).data('kode_kelas');
            var ph4      = $(this).val();
            var id_mapel = $('#kode_mapel').val();
            
            $.ajax({
                type: "POST",
                url : "<?= site_url('ControllerNilaiHarian/insert_nilai_harian') ?>",
                data : {
                    id_siswa : id_siswa,
                    id_kelas : id_kelas,
                    id_mapel : id_mapel,
                    ph4      : ph4,
                },
                dataType : "JSON",
                success: function(hasil){
                    if(hasil.info == 'sukses'){
                        t.fnDraw();

                    } else {
                        alert('Gagal input nilai harian');
                    }
                }
            })
        });

        $('#mytable').on('change', '#ph5', function (){
            var id_siswa = $(this).data('nis');
            var id_kelas = $(this).data('kode_kelas');
            var ph5      = $(this).val();
            var id_mapel = $('#kode_mapel').val();
            
            $.ajax({
                type: "POST",
                url : "<?= site_url('ControllerNilaiHarian/insert_nilai_harian') ?>",
                data : {
                    id_siswa : id_siswa,
                    id_kelas : id_kelas,
                    id_mapel : id_mapel,
                    ph5      : ph5,
                },
                dataType : "JSON",
                success: function(hasil){
                    if(hasil.info == 'sukses'){
                        t.fnDraw();

                    } else {
                        alert('Gagal input nilai harian');
                    }
                }
            })
        });

        $('#mytable').on('change', '#ph6', function (){
            var id_siswa = $(this).data('nis');
            var id_kelas = $(this).data('kode_kelas');
            var ph6      = $(this).val();
            var id_mapel = $('#kode_mapel').val();
            
            $.ajax({
                type: "POST",
                url : "<?= site_url('ControllerNilaiHarian/insert_nilai_harian') ?>",
                data : {
                    id_siswa : id_siswa,
                    id_kelas : id_kelas,
                    id_mapel : id_mapel,
                    ph6      : ph6,
                },
                dataType : "JSON",
                success: function(hasil){
                    if(hasil.info == 'sukses'){
                        // alert('Berhasil input nilai harian');
                        t.fnDraw();

                    } else {
                        alert('Gagal input nilai harian');
                    }
                }
            })
        });

        $('#mytable').on('change', '#uts', function (){
            var id_siswa = $(this).data('nis');
            var id_kelas = $(this).data('kode_kelas');
            var uts      = $(this).val();
            var id_mapel = $('#kode_mapel').val();
            
            $.ajax({
                type: "POST",
                url : "<?= site_url('ControllerNilaiHarian/insert_nilai_harian') ?>",
                data : {
                    id_siswa : id_siswa,
                    id_kelas : id_kelas,
                    id_mapel : id_mapel,
                    uts      : uts,
                },
                dataType : "JSON",
                success: function(hasil){
                    if(hasil.info == 'sukses'){
                        // alert('Berhasil input nilai harian');
                        t.fnDraw();

                    } else {
                        alert('Gagal input nilai harian');
                    }
                }
            })
        });
    });
</script>