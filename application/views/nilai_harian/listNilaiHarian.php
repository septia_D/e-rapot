<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default block1">
            <div class="panel-heading">
                <h3>Input Nilai Harian</h3>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <div class="form-group">
                        <label for="">Mata Pelajaran </label>
                        <p style="color: red"><i>*ketuk untuk pilih mapel</i></p>
                        <select class="form-control select2" name="kode_mapel" id="kode_mapel" value="">
                            <option value="" selected disabled>--Pilih--</option>
                            <?php foreach ($mata_pelajaran as $value) : ?>
                                <option value="<?= $value->kode_mapel ?>"><?= $value->tingkat; ?> - <?= $value->nama_mapel; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="table-responsive">
                        <table id="mytable" class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th>Rombongan Belajar</th>
                                    <th>Wali Kelas</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#mytable").dataTable({
            "processing": true,
            "serverSide": true,
            "oLanguage": {
                sProcessing: "Loading. . ."
            },
            "ajax": {
                "url": "<?= site_url('ControllerNilaiHarian/json') ?>",
                "type": "POST",
                "data": function(d) {
                    d.kode_mapel = $('#kode_mapel').val();
                }
            },
            "columns": [{
                    "data": "tingkat",
                    "orderable": false,
                    "className": "text-center"
                },
                {
                    "data": "tingkat",
                    "render": function(data, type, row, meta) {
                        return data + " - " + row.rombel
                    }
                },
                {
                    "data": "nama_lengkap"
                },
                {
                    "data": "action",
                    "orderable": false,
                    "className": "text-center"
                }
            ],
            order: [
                [0, 'desc']
            ],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });

        $('#kode_mapel').change(function() {
            t.fnDraw();
        });


    });
</script>