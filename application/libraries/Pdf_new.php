<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * CodeIgniter DomPDF Library
 *
 * Generate PDF's from HTML in CodeIgniter
 *
 * @packge        CodeIgniter
 * @subpackage        Libraries
 * @category        Libraries
 * @author        Ardianta Pargo
 * @license        MIT License
 * @link        https://github.com/ardianta/codeigniter-dompdf
 */
require_once(dirname(__FILE__) . '/dompdf/autoload.inc.php');
use Dompdf\Dompdf;
class Pdf_new extends Dompdf{
    /**
     * PDF filename
     * @var String
     */
    public $filename;
    public function __construct(){
        parent::__construct();
        $this->filename = "cetak_rapot.pdf";
    }
    /**
     * Get an instance of CodeIgniter
     *
     * @access    protected
     * @return    void
     */
    protected function ci()
    {
        return get_instance();
    }
    /**
     * Load a CodeIgniter view into domPDF
     *
     * @access    public
     * @param    string    $view The view to load
     * @param    array    $data The view data
     * @return    void
     */
    public function load_view($view, $size = array() , $orientation, $data = array()){
        $html = $this->ci()->load->view($view, $data, TRUE);
        $this->set_option('isRemoteEnabled', TRUE);
        $this->set_option('isHtml5ParserEnabled', true);        
        $this->set_option("isJavascriptEnabled", true);        
        $this->load_html($html);        
        // $this->setPaper('f4', 'Potrait'); 
        // $customPaper = array(420, 800,0,0);
		$this->setPaper($size, $orientation);     
        // Render the PDF
        $this->render();
            // Output the generated PDF to Browser
               $this->stream($this->filename, array("Attachment" => false));
    }

    public function load_view_kasir($view, $size = array() , $orientation, $data = array()){
        $html = $this->ci()->load->view($view, $data, TRUE);
        $this->set_option('isRemoteEnabled', TRUE);
        $this->set_option('isHtml5ParserEnabled', true);        
        $this->set_option("isJavascriptEnabled", true);        
        $this->load_html($html);        
        // $this->setPaper('f4', 'Potrait'); 
        // $customPaper = array(420, 800,0,0);
		$this->setPaper($size, $orientation);     
        // Render the PDF
        $this->render();
        $canvas = $this->get_canvas();
        // $font = Font_Metrics::get_font("helvetica", "bold");
        $canvas->page_text(582, 729, "Halaman: {PAGE_NUM} of {PAGE_COUNT}", 'sans-serif', 12, array(0,0,0));
            // Output the generated PDF to Browser
               $this->stream($this->filename, array("Attachment" => false));
    }

    public function createPdf($nama,$html,$ukuran,$orientasi,$lampirkan=false){
        $CI = get_instance();
        $this->set_option('isRemoteEnabled', TRUE);
        $this->set_option('isHtml5ParserEnabled', true);        
        $this->set_option("isJavascriptEnabled", true);    
        $this->load_html($html);      
		$this->setPaper($ukuran, $orientasi);     
        $this->render();
        $this->stream($nama, array("Attachment" => $lampirkan)); 
    }
}