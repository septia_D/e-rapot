<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MengajarModel extends CI_Model {
    
    // datatables
    function json($kode_guru) {
        $this->datatables->select('a.kode_mengajar, a.kode_guru,a.kode_mapel,b.nama_mapel,b.tingkat');
        $this->datatables->where('a.kode_guru', $kode_guru);
        $this->datatables->from('mengajar as a');
        //add this line for join
        $this->datatables->join('mata_pelajaran as b','a.kode_mapel=b.kode_mapel');
        $this->datatables->add_column('action',anchor(site_url('controllerGuru/hapus_mengajar/$1'),'<i class="fa   fa-archive"></i> Hapus','class="btn btn-danger hapus" title="Hapus Data"'), 'kode_mengajar');
        return $this->datatables->generate();
    }

    // get total rows
    function total_rows($q = NULL) {
        $this->db->like("kode_guru", $q);
        $this->db->or_like("kode_guru", $q);
        return $this->db->count_all_results();
    }

     // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by('kode_guru', 'DESC');
        $this->db->like("kode_guru", $q);
        $this->db->limit($limit, $start);
        return $this->db->get('mengajar')->result();
    }

    public function get_by_id($kode)
    {
        $this->db->where('kode_mengajar',$kode);
        return $this->db->get('mengajar')->row();
    }

    public function delete_mengajar($kode)
    {
        $this->db->where('kode_mengajar', $kode);
        $this->db->delete('mengajar');
    }

}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */