<?php
defined('BASEPATH') or exit('No direct script access allowed');

class WaliMuridModel extends CI_Model
{

    // datatables
    function json_siswa()
    {
        $this->datatables->select('a.nis, a.nama_lengkap, a.jenis_kelamin, a.tanggal_lahir, b.kode_kelas');
        $this->datatables->from('siswa as a');
        $this->datatables->join('rombel as b', 'a.nis=b.nis');
        $this->datatables->join('kelas as c', 'b.kode_kelas=c.kode_kelas');
        //add this line for join
        if ($this->session->session_login['username'] != 'admin') {
            $this->datatables->where('kode_guru', $this->session->session_login['id_guru']);
        }
        $this->datatables->add_column('action', anchor(site_url('ControllerWaliKelas/lihat_nilai/$1/$2'), '<i class="fa fa-eye"></i> Lihat Nilai', array('class' => 'btn btn-success', 'title' => 'Lihat Nilai')) . " " . anchor(site_url('ControllerWaliKelas/profil_siswa/$1'), '<i class="fa fa-eye"></i> Profil Siswa', array('class' => 'btn btn-info', 'title' => 'Profil Siswa')) . " " . anchor(site_url('ControllerWaliKelas/input_catatan/$1/$2'), '<i class="fa fa-external-link-square"></i> Input Catatan', 'class="btn btn-warning" title="Input Catatan"'), 'nis, kode_kelas');
        return $this->datatables->generate();
    }

    function json_rapor()
    {
        $this->datatables->select('a.nis, a.nama_lengkap, a.jenis_kelamin, a.tanggal_lahir, b.kode_kelas');
        $this->datatables->from('siswa as a');
        $this->datatables->join('rombel as b', 'a.nis=b.nis');
        $this->datatables->join('kelas as c', 'b.kode_kelas=c.kode_kelas');
        //add this line for join
        if ($this->session->session_login['username'] != 'admin') {
            $this->datatables->where('kode_guru', $this->session->session_login['id_guru']);
        }
        $this->datatables->add_column('action', anchor(site_url('ControllerWaliKelas/cetak_rapot_siswa/$1'), '<i class="fa fa-external-link-square"></i> Cetak Rapot', array('data-nis="$1" data-kelas="$2" class' => 'btn btn-success cetak_rapot', 'title' => 'Cetak Rapot')), 'nis, kode_kelas');
        return $this->datatables->generate();
    }

    function json_ranking()
    {
        $this->datatables->select("a.nis, a.nama_lengkap, a.jenis_kelamin, a.tanggal_lahir, b.kode_kelas,
        (select ranking from ranking_siswa as d where d.id_siswa=a.nis and d.id_kelas=b.kode_kelas) as ranking
        ");
        $this->datatables->from('siswa as a');
        $this->datatables->join('rombel as b', 'a.nis=b.nis');
        $this->datatables->join('kelas as c', 'b.kode_kelas=c.kode_kelas');
        //add this line for join
        if ($this->session->session_login['username'] != 'admin') {
            $this->datatables->where('kode_guru', $this->session->session_login['id_guru']);
        }
        //add this line for join
        return $this->datatables->generate();
    }

    function json_ekstra($id_siswa, $id_kelas)
    {
        $this->datatables->select('a.id_nilai_ekstra, a.kode_ekstra, a.predikat, a.deskripsi, b.nama_ekstra');
        $this->datatables->from('nilai_ekstra as a');
        $this->datatables->where('a.id_siswa', $id_siswa);
        $this->datatables->where('a.id_kelas', $id_kelas);
        //add this line for join
        $this->datatables->join('ekstrakulikuler as b', 'a.kode_ekstra=b.kode_ekstra');
        $this->datatables->add_column('action', anchor(site_url('ControllerWaliKelas/hapus_nilai_ekstra/$1'), '<i class="fa   fa-archive"></i> Hapus', 'class="btn btn-danger hapus" title="Hapus Data"'), 'id_nilai_ekstra,nama_ekstra');
        return $this->datatables->generate();
    }

    function json_prestasi($id_siswa, $id_kelas)
    {
        $this->datatables->select('a.id_prestasi, a.nama_prestasi, a.predikat, a.deskripsi');
        $this->datatables->from('prestasi as a');
        $this->datatables->where('a.id_siswa', $id_siswa);
        $this->datatables->where('a.id_kelas', $id_kelas);
        //add this line for join
        $this->datatables->add_column('action', anchor(site_url('ControllerWaliKelas/hapus_nilai_prestasi/$1'), '<i class="fa   fa-archive"></i> Hapus', 'class="btn btn-danger hapus" title="Hapus Data"'), 'id_prestasi');
        return $this->datatables->generate();
    }

    function get_by_id($id_siswa, $id_kelas)
    {
        $where = [
            'id_siswa'  => $id_siswa,
            'id_kelas'  => $id_kelas
        ];
        $this->db->where($where);
        return $this->db->get("catatan_siswa")->row();
    }

    function update_catatan($id_siswa, $id_kelas, $data)
    {
        $this->db->where("id_siswa", $id_siswa);
        $this->db->where("id_kelas", $id_kelas);
        $this->db->update("catatan_siswa", $data);
    }

    function get_all_kelas()
    {
        $this->db->select('*');
        $query = $this->db->get('kelas');
        return $query->result();
    }

    function get_by_id_siswa($nis)
    {
        $this->db->where('nis', $nis);
        return $this->db->get("siswa")->row();
    }

    function get_all_nilai($id_siswa, $id_kelas)
    {
        $this->db->select('a.*, b.nama_mapel');
        $this->db->from('nilai as a');
        $this->db->where('id_siswa', $id_siswa);
        $this->db->where('id_kelas', $id_kelas);

        $this->db->join('mata_pelajaran as b', 'a.id_mapel=b.kode_mapel');
        return $this->db->get()->result();
    }

    function get_all_ekstra()
    {
        $this->db->select('*');
        $query = $this->db->get('ekstrakulikuler');
        return $query->result();
    }

    function cek_nilai_ekstra($id)
    {
        $this->db->where('id_nilai_ekstra', $id);
        return $this->db->get("nilai_ekstra")->row();
    }

    function delete_nilai_ekstra($id)
    {
        $this->db->where("id_nilai_ekstra", $id);
        $this->db->delete("nilai_ekstra");
    }

    function cek_nilai_prestasi($id)
    {
        $this->db->where('id_prestasi', $id);
        return $this->db->get("prestasi")->row();
    }

    function delete_nilai_prestasi($id)
    {
        $this->db->where("id_prestasi", $id);
        $this->db->delete("prestasi");
    }

    function cek_ranking_siswa($id_siswa, $id_kelas)
    {
        $this->db->where('id_siswa', $id_siswa);
        $this->db->where('id_kelas', $id_kelas);
        return $this->db->get("ranking_siswa")->row();
    }

    function update_ranking($where, $data_update)
    {
        $this->db->where($where);
        $this->db->update("ranking_siswa", $data_update);
    }

    function get_by_id_kelas($nis)
    {
        $this->db->select('*');
        $this->db->from('rombel as a');
        $this->db->join('kelas as b', 'a.kode_kelas=b.kode_kelas');
        $this->db->join('guru as c', 'b.kode_guru=c.nuptk');
        $this->db->where('a.nis', $nis);
        return $this->db->get()->row();
    }

    function get_by_catatan($id_siswa, $id_kelas)
    {
        $this->db->where('id_siswa', $id_siswa);
        $this->db->where('id_kelas', $id_kelas);
        return $this->db->get("catatan_siswa")->row();
    }

    function get_all_mapel($tingkat, $id_siswa, $id_kelas)
    {
        $this->db->select("a.nama_mapel, a.deskripsi,
        (SELECT nilai_akhir FROM nilai as b WHERE a.kode_mapel=b.id_mapel AND id_siswa='$id_siswa') as nilai_akhir,
        (SELECT predikat_akhir FROM nilai as b WHERE a.kode_mapel=b.id_mapel AND id_siswa='$id_siswa') as predikat");
        $this->db->from('mata_pelajaran as a');
        $this->db->where('a.tingkat', $tingkat);
        return $this->db->get()->result();
    }

    function jumlah_nilai($id_siswa, $id_kelas)
    {
        $this->db->select_sum('nilai_akhir');
        $this->db->where('id_siswa', $id_siswa);
        $this->db->where('id_kelas', $id_kelas);
        return $this->db->get('nilai')->row();
    }

    function get_all_esktra($id_siswa, $id_kelas)
    {
        $this->db->select('*');
        $this->db->from('nilai_ekstra as a');
        $this->db->join('ekstrakulikuler as b', 'a.kode_ekstra=b.kode_ekstra');
        $this->db->where('a.id_siswa', $id_siswa);
        $this->db->where('a.id_kelas', $id_kelas);
        return $this->db->get()->result();
    }

    function get_all_prestasi($id_siswa, $id_kelas)
    {
        $this->db->select('*');
        $this->db->from('prestasi as a');
        $this->db->where('a.id_siswa', $id_siswa);
        $this->db->where('a.id_kelas', $id_kelas);
        return $this->db->get()->result();
    }

    function cekStatusCatatan($id_siswa, $id_kelas)
    {
        $this->db->where('id_siswa', $id_siswa);
        $this->db->where('id_kelas', $id_kelas);
        return $this->db->get("catatan_siswa")->row();
    }
}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */
