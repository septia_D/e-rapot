<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeModel extends CI_Model {
    
    public function jumlah_guru()
    {
        $query = $this->db->get('guru');
        if($query->num_rows() > 0){
            $records = $query->result_array();
            $data['count'] = count($records);
            $data['all_records'] = $records;
            
            return $data;
        }
    }

    public function jumlah_ekstra()
    {
        $query = $this->db->get('ekstrakulikuler');
        if($query->num_rows() > 0){
            $records = $query->result_array();
            $data['count'] = count($records);
            $data['all_records'] = $records;
            
            return $data;
        }
    }

    public function jumlah_siswa()
    {
        $query = $this->db->get('siswa');
        if($query->num_rows() > 0){
            $records = $query->result_array();
            $data['count'] = count($records);
            $data['all_records'] = $records;
            
            return $data;
        }
    }

    public function jumlah_rombel()
    {
        $query = $this->db->get('kelas');
        if($query->num_rows() > 0){
            $records = $query->result_array();
            $data['count'] = count($records);
            $data['all_records'] = $records;
            
            return $data;
        }
    }

    public function get_by_id_user($id_guru)
    {
        $this->db->select('*');
        $this->db->from('users');
        if($id_guru != null){
            $this->db->join('guru', 'users.id_guru=guru.nuptk', 'left');
            $this->db->where('id_guru', $id_guru);
        }
        return $this->db->get()->row();
        
    }

    function update_admin($id, $data)
    {
        $this->db->where("id_guru", $id);
        $this->db->update("users", $data);
    }
    
}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */