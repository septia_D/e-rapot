<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EkstraModel extends CI_Model {
    
    // datatables
    function json() {
        $this->datatables->select('a.kode_ekstra, a.nama_ekstra');
        $this->datatables->from('ekstrakulikuler as a');
        //add this line for join
        $this->datatables->add_column('action',anchor(site_url('controllerEkstra/edit_ekstra/$1'),'<i class="fa  fa-external-link"></i> Edit', array('class' => 'btn btn-success', 'title' => 'Ubah Data'))." ".anchor(site_url('controllerEkstra/hapus_ekstra/$1'),'<i class="fa   fa-archive"></i> Hapus','data-nama_ekstra="$2" class="btn btn-danger hapus" title="Hapus Data"'), 'kode_ekstra,nama_ekstra');
        return $this->datatables->generate();
    }

    // get total rows
    function total_rows($q = NULL) {
        $this->db->like("nama_ekstra", $q);
        $this->db->or_like("nama_ekstra", $q);
        $this->db->from("ekstrakulikuler");
        return $this->db->count_all_results();
     }
 
     // get data with limit and search
     function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by('nama_ekstra', 'DESC');
        $this->db->like("nama_ekstra", $q);
        $this->db->or_like("nama_ekstra", $q);
        $this->db->limit($limit, $start);
        return $this->db->get('ekstrakulikuler')->result();
     }

    function get_all()
    {
        $this->db->select('kode_guru, nama_lengkap');
        $query = $this->db->get('guru');
        return $query->result();
    }

    function insert_Ekstra($data)
    {
        $this->db->insert('ekstrakulikuler', $data);
    }

    function get_by_id($kode_ekstra)
    {
        $this->db->where('kode_ekstra', $kode_ekstra);
        return $this->db->get("ekstrakulikuler")->row();
    }

    function update_ekstra($kode_ekstra, $data)
    {
        $this->db->where("kode_ekstra", $kode_ekstra);
        $this->db->update("ekstrakulikuler", $data);
    }

    function delete_ekstra($kode_ekstra)
    {
        $this->db->where("kode_ekstra", $kode_ekstra);
        $this->db->delete("ekstrakulikuler");
    }

}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */