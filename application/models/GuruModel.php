<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GuruModel extends CI_Model {
    
    // datatables
    function json() {
        $this->datatables->select('a.kode_guru, a.nama_lengkap, a.nuptk');
        $this->datatables->from('guru as a');
        //add this line for join
        $this->datatables->add_column('action',anchor(site_url('controllerGuru/atur_mengajar/$2'),'<i class="fa  fa-external-link"></i> Atur Mengajar', array('class' => 'btn btn-success', 'title' => 'Atur Mengajar'))." ".anchor(site_url('controllerGuru/lihat_guru/$1'),'<i class="fa   fa-eye"></i> LIhat', array('class' => 'btn btn-info', 'title' => 'Lihat Data'))." ".anchor(site_url('controllerGuru/hapus_guru/$2'),'<i class="fa   fa-archive"></i> Hapus','data-nama_guru="$2" class="btn btn-danger hapus" title="Hapus Data"'), 'kode_guru,nuptk,nama_lengkap');
        return $this->datatables->generate();
    }

    // get total rows
    function total_rows($q = NULL) {
        $this->db->like("kode_guru", $q);
        $this->db->or_like("nuptk", $q);
        $this->db->or_like("nama_lengkap", $q);
        return $this->db->count_all_results();
     }
 
     // get data with limit and search
     function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by('nuptk', 'DESC');
        $this->db->like("nuptk", $q);
        $this->db->or_like("nama_lengkap", $q);
        $this->db->limit($limit, $start);
        return $this->db->get('guru')->result();
     }

    function insert_Guru($data)
    {
        $this->db->insert('guru', $data);
    }

    function insert_user($data)
    {
        $this->db->insert('users', $data);
    }

    function insert_aturMengajar($data)
    {
        $this->db->insert('mengajar', $data);
    }

    function get_by_id($kode_guru)
    {
        $this->db->where('kode_guru', $kode_guru);
        return $this->db->get("guru")->row();
    }

    function get_by_id_nuptk($kode_guru)
    {
        $this->db->where('nuptk', $kode_guru);
        return $this->db->get("guru")->row();
    }

    function get_by_id_user($nuptk)
    {
        $this->db->where('id_guru', $nuptk);
        return $this->db->get("users")->row();
    }

    function update_guru($kode_guru, $data)
    {
        $this->db->where("kode_guru", $kode_guru);
        $this->db->update("guru", $data);
    }

    function update_user($id, $data)
    {
        $this->db->where("id", $id);
        $this->db->update("users", $data);
    }

    function delete_guru($kode_guru)
    {
        $this->db->where("kode_guru", $kode_guru);
        $this->db->delete("guru");
    }

    function delete_user($nuptk)
    {
        $this->db->where("id_guru", $nuptk);
        $this->db->delete("users");
    }

    function get_all()
    {
        $this->db->select('*');
        $query = $this->db->get('mata_pelajaran');
        return $query->result();
    }

    

}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */