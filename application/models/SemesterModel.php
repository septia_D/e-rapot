<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SemesterModel extends CI_Model {
    
    function get_all()
    {
        $this->db->select('kode_semester, nama_semester, aktif');
        $query = $this->db->get('semester');
        return $query->result_array();
    }

    function insert_semester($data)
    {
        $this->db->insert('semester', $data);
    }

    function get_by_id($kode_semester)
    {
        $this->db->where('kode_semester', $kode_semester);
        return $this->db->get("semester")->row();
    }

    function update_semester($kode_semester, $data)
    {
        $this->db->where("kode_semester", $kode_semester);
        $this->db->update("semester", $data);
    }

    function delete_semester($kode_semester)
    {
        $this->db->where("kode_semester", $kode_semester);
        $this->db->delete("semester");
    }

}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */