<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MataPelajaranModel extends CI_Model {
    
    // datatables
    function json() {
        $this->datatables->select('kode_mapel, tingkat, nama_mapel');
        $this->datatables->from('mata_pelajaran');
        //add this line for join
        //$this->datatables->join('table2', 'generalia.jenjang_pendidikan.field = table2.field');
        $this->datatables->add_column('action',anchor(site_url('controllerMataPelajaran/edit_mataPelajaran/$1'),'<i class="fa  fa-external-link"></i> Edit', array('class' => 'btn btn-success', 'title' => 'Ubah Data'))." ".anchor(site_url('controllerMataPelajaran/hapus_mataPelajaran/$1'),'<i class="fa fa-archive"></i> Hapus',' data-nama_mapel="$2" class="btn btn-danger hapus" title="Hapus Data"'), 'kode_mapel,nama_mapel');
        return $this->datatables->generate();
    }

    // get total rows
    function total_rows($q = NULL) {
        $this->db->like("kode_mapel", $q);
        $this->db->or_like("kode_mapel", $q);
        $this->db->or_like("nama_mapel", $q);
        $this->db->or_like("tingkat", $q);
        $this->db->from("mata_pelajaran");
        return $this->db->count_all_results();
     }
 
     // get data with limit and search
     function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by('kode_mapel', 'DESC');
        $this->db->like("kode_mapel", $q);
        $this->db->or_like("kode_mapel", $q);
        $this->db->or_like("nama_mapel", $q);
        $this->db->or_like("tingkat", $q);
        $this->db->limit($limit, $start);
        return $this->db->get('mata_pelajaran')->result();
     }

    function insert_MataPelajaran($data)
    {
        $this->db->insert('mata_pelajaran', $data);
    }

    function get_by_id($kode_mapel)
    {
        $this->db->where('kode_mapel', $kode_mapel);
        return $this->db->get("mata_pelajaran")->row();
    }

    function cek_mapel($kode_mapel)
    {
        $this->db->where('kode_mapel', $kode_mapel);
        return $this->db->get("mata_pelajaran")->num_rows();
    }

    function update_mataPelajaran($kode_mapel, $data)
    {
        $this->db->where("kode_mapel", $kode_mapel);
        $this->db->update("mata_pelajaran", $data);
    }

    function delete_mataPelajaran($kode_mapel)
    {
        $this->db->where("kode_mapel", $kode_mapel);
        $this->db->delete("mata_pelajaran");
    }

}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */