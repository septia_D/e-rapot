<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginModel extends CI_Model {

	function validasi($table,$where){		
		return $this->db->get_where($table,$where);
	}
	
	function get_by_id_guru($nuptk){
		$this->db->select('*');
		$this->db->from('guru');
		$this->db->where('nuptk', $nuptk);
		$query = $this->db->get();
		return $query->row();
	}

}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */