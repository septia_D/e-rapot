<?php
defined('BASEPATH') or exit('No direct script access allowed');

class BobotModel extends CI_Model
{

    // datatables
    function json()
    {
        $this->datatables->select('a.kode_mapel, a.tingkat, a.nama_mapel, b.kkm');
        $this->datatables->from('mata_pelajaran as a');
        //add this line for join
        $this->datatables->join('bobot_kkm as b', 'a.kode_mapel = b.kode_mapel', 'left');

        if ($this->session->session_login['username'] != 'admin') {
            $this->datatables->join('mengajar as c', 'a.kode_mapel=c.kode_mapel');
            $this->datatables->join('guru as d', 'c.kode_guru=d.nuptk');
            $this->datatables->where('d.nuptk', $this->session->session_login['id_guru']);
        }
        $this->datatables->add_column('action', anchor(site_url('controllerBobot/atur_bobot_kkm/$1'), '<i class="fa  fa-external-link"></i> Atur', array('class' => 'btn btn-success', 'title' => 'Ubah Data')), 'kode_mapel,nama_mapel');
        return $this->datatables->generate();
    }

    // // get total rows
    // function total_rows($q = NULL) {
    //     $this->db->like("kode_mapel", $q);
    //     $this->db->or_like("kode_mapel", $q);
    //     $this->db->or_like("nama_mapel", $q);
    //     $this->db->or_like("tingkat", $q);
    //     $this->db->from("mata_pelajaran");
    //     return $this->db->count_all_results();
    //  }

    //  // get data with limit and search
    //  function get_limit_data($limit, $start = 0, $q = NULL) {
    //     $this->db->order_by('kode_mapel', 'DESC');
    //     $this->db->like("kode_mapel", $q);
    //     $this->db->or_like("kode_mapel", $q);
    //     $this->db->or_like("nama_mapel", $q);
    //     $this->db->or_like("tingkat", $q);
    //     $this->db->limit($limit, $start);
    //     return $this->db->get('mata_pelajaran')->result();
    //  }

    function insert($data)
    {
        $this->db->insert('bobot_kkm', $data);
    }

    function get_by_id($kode_mapel)
    {
        $this->db->where('kode_mapel', $kode_mapel);
        return $this->db->get("mata_pelajaran")->row();
    }

    function get_by_id_bobot($kode_mapel)
    {
        $this->db->where('kode_mapel', $kode_mapel);
        return $this->db->get("bobot_kkm");
    }

    function update($kode_bobot_kkm, $data)
    {
        $this->db->where("kode_bobot_kkm", $kode_bobot_kkm);
        $this->db->update("bobot_kkm", $data);
    }

    function delete_mataPelajaran($kode_mapel)
    {
        $this->db->where("kode_mapel", $kode_mapel);
        $this->db->delete("mata_pelajaran");
    }
}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */