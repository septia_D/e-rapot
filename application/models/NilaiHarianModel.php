<?php
defined('BASEPATH') or exit('No direct script access allowed');

class NilaiHarianModel extends CI_Model
{

    // datatables
    function json($kode_mapel)
    {
        $this->datatables->select('a.kode_kelas,a.tingkat, a.rombel, b.nama_lengkap, c.kode_mapel');
        $this->datatables->from('kelas as a');
        $this->datatables->where('c.kode_mapel', $kode_mapel);
        //add this line for join
        $this->datatables->join('guru as b', 'a.kode_guru = b.nuptk');
        $this->datatables->join('mata_pelajaran as c', 'a.tingkat = c.tingkat');

        $this->datatables->add_column('action', anchor(site_url('controllerNilaiHarian/atur_nilai_harian/$1/$2'), '<i class="fa  fa-external-link"></i> Input Nilai', array('class' => 'btn btn-info', 'title' => 'Input Nilai')), 'kode_kelas, kode_mapel');
        return $this->datatables->generate();
    }

    function json_input_nilai($kode_kelas, $kode_mapel)
    {
        // print_r($kode_mapel);die;
        $this->datatables->select("a.nis, a.kode_kelas, b.nama_lengkap, 
        (select uh1 from nilai where id_siswa=a.nis and id_kelas='$kode_kelas' and id_mapel='$kode_mapel') as uh1, 
        (select uh2 from nilai where id_siswa=a.nis and id_kelas='$kode_kelas' and id_mapel='$kode_mapel') as uh2, 
        (select uh3 from nilai where id_siswa=a.nis and id_kelas='$kode_kelas' and id_mapel='$kode_mapel') as uh3, 
        (select uh4 from nilai where id_siswa=a.nis and id_kelas='$kode_kelas' and id_mapel='$kode_mapel') as uh4, 
        (select uh5 from nilai where id_siswa=a.nis and id_kelas='$kode_kelas' and id_mapel='$kode_mapel') as uh5, 
        (select uh6 from nilai where id_siswa=a.nis and id_kelas='$kode_kelas' and id_mapel='$kode_mapel') as uh6,
        (select uts from nilai where id_siswa=a.nis and id_kelas='$kode_kelas' and id_mapel='$kode_mapel') as uts");
        $this->datatables->from('rombel as a');
        $this->datatables->where('a.kode_kelas', $kode_kelas);

        //add this line for join
        $this->datatables->join('siswa as b', 'a.nis=b.nis');
        return $this->datatables->generate();
    }

    function insert($data)
    {
        $this->db->insert('bobot_kkm', $data);
    }

    function get_by_id_mapel($kode_mapel)
    {
        $this->db->where('kode_mapel', $kode_mapel);
        return $this->db->get("mata_pelajaran")->row();
    }

    function get_by_id_kelas($kode_kelas)
    {
        $this->db->where('kode_kelas', $kode_kelas);
        return $this->db->get("kelas")->row();
    }

    function get_all_mapel()
    {
        if ($this->session->session_login['username'] != 'admin') {
            $kode_guru = $this->session->session_login['id_guru'];
            $query = $this->db->query("SELECT a.*, b.* from mata_pelajaran as a JOIN mengajar as b ON a.kode_mapel=b.kode_mapel WHERE b.kode_guru='$kode_guru'");
            return $query->result();
        } else {
            return $this->db->get("mata_pelajaran")->result();
        }
    }

    function update($id, $data)
    {
        $this->db->where("id_nilai", $id);
        $this->db->update("nilai", $data);
    }

    function cek_siswa_nilai($id_siswa, $id_kelas, $id_mapel)
    {
        $this->db->where('id_siswa', $id_siswa);
        $this->db->where('id_kelas', $id_kelas);
        $this->db->where('id_mapel', $id_mapel);
        return $this->db->get("nilai");
    }

    function cek_siswa_nilai_mapel()
    {
        return $this->db->get("nilai");
    }
}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */
