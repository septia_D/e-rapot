<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RombelModel extends CI_Model {
    
    // datatables
    function json($kode_kelas) {
        $this->datatables->select('a.kode_rombel, a.kode_kelas, a.nis, b.nama_lengkap');
        $this->datatables->where('a.kode_kelas', $kode_kelas);
        $this->datatables->from('rombel as a');
        $this->datatables->join('siswa as b', 'a.nis=b.nis');
        //add this line for join
        $this->datatables->add_column('action',anchor(site_url('controllerKelas/hapus_siswa_rombel/$1'),'<i class="fa   fa-archive"></i> Hapus','data-nama_siswa="$2" class="btn btn-danger hapus" title="Hapus Data"'), 'kode_rombel,nama_lengkap');
        return $this->datatables->generate();
    }

    // get total rows
    function total_rows($q = NULL) {
        $this->db->like("nama_lengkap", $q);
        $this->db->or_like("nama_lengkap", $q);
        $this->db->or_like("nis", $q);
        return $this->db->count_all_results();
     }
 
     // get data with limit and search
     function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by('nama_lengkap', 'DESC');
        $this->db->like("nama_lengkap", $q);
        $this->db->or_like("nis", $q);
        $this->db->limit($limit, $start);
        return $this->db->get('rombel')->result();
     }

     function get_by_id($kode_rombel)
    {
        $this->db->where('kode_rombel', $kode_rombel);
        return $this->db->get("rombel")->row();
    }

    function delete_siswa($kode_rombel)
    {
        $this->db->where("kode_rombel", $kode_rombel);
        $this->db->delete("rombel");
    }

    function get_by_id_siswa($nis){
        $this->db->select('*');
        $this->db->from('rombel');
        $this->db->where('nis', $nis);
        $query = $this->db->get();
        return $query->row();
    }

}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */