<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KelasModel extends CI_Model {
    
    // datatables
    function json() {
        $this->datatables->select('a.kode_guru, a.kode_kelas, a.tingkat, a.rombel, b.nama_lengkap,a.tahun_ajar,a.semester');
        $this->datatables->from('kelas as a');
        $this->datatables->join('guru as b', 'a.kode_guru=b.nuptk', 'left');
        $this->datatables->where('a.semester', $this->session->konfigurasi['semester']);
        $this->datatables->where('a.tahun_ajar', $this->session->konfigurasi['tahun_ajar']);
        //add this line for join
        $this->datatables->add_column('action',anchor(site_url('controllerKelas/atur_kelas/$1'),'<i class="fa  fa-external-link"></i> Atur Kelas', array('class' => 'btn btn-success', 'title' => 'Atur Kelas'))." ".'<button class="btn btn-info btn_edit_wali_kelas" type="button" data-tingkat="$2" data-rombel="$3" data-kode_kelas="$1" data-guru="$4">Edit Wali Kelas</button>'." ".anchor(site_url('controllerKelas/hapus_kelas/$1'),'<i class="fa   fa-archive"></i> Hapus','data-tingkat="$2" data-rombel="$3" class="btn btn-danger hapus" title="Hapus Data"'), 'kode_kelas,tingkat,rombel,kode_guru');
        return $this->datatables->generate();
    }

    // get total rows
    function total_rows($q = NULL) {
        $this->db->like("nama_kelas", $q);
        $this->db->or_like("nama_kelas", $q);
        $this->db->or_like("kode_guru", $q);
        return $this->db->count_all_results();
     }
 
     // get data with limit and search
     function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by('nama_kelas', 'DESC');
        $this->db->like("nama_kelas", $q);
        $this->db->or_like("nama_kelas", $q);
        $this->db->or_like("kode_guru", $q);
        $this->db->limit($limit, $start);
        return $this->db->get('kelas')->result();
     }

    function insert_kelas($data)
    {
        $this->db->insert('kelas', $data);
    }

    function insert_aturSiswa($data)
    {
        $this->db->insert('rombel', $data);
    }

    function get_by_id($kode_kelas)
    {
        $this->db->select("a.*,b.nama_lengkap");
        $this->db->where('a.kode_kelas', $kode_kelas);
        $this->db->from("kelas as a");
        $this->db->join("guru as b", "a.kode_guru=b.nuptk");
        return $this->db->get()->row();
    }

    function get_by_id_guru($kode_guru)
    {
        $this->db->where('nuptk', $kode_guru);
        return $this->db->get("guru")->row();
    }

    function update_user($nuptk, $data)
    {
        $this->db->where("id_guru", $nuptk);
        $this->db->update("users", $data);
    }

    function update_kelas($kode_kelas, $data)
    {   
        $this->db->where("kode_kelas", $kode_kelas);
        $this->db->update("kelas", $data);
    }

    function delete_kelas($kode_kelas)
    {
        $this->db->where("kode_kelas", $kode_kelas);
        $this->db->delete("kelas");
    }

    function get_all_guru()
    {
        $query = $this->db->query("SELECT * from guru WHERE nuptk NOT IN(SELECT kode_guru FROM kelas)");
        return $query->result();
    }

    function get_all_tahunAjar()
    {
        $this->db->select('*');
        $query = $this->db->get('tahun_ajar');
        return $query->result();
    }

    function get_all_semester()
    {
        $this->db->select('*');
        $query = $this->db->get('semester');
        return $query->result();
    }

    function get_all_siswa()
    {
        $query = $this->db->query("SELECT * from siswa WHERE nis NOT IN(SELECT nis FROM rombel)");
        return $query->result();
    }

    function jumlah_siswa($kode_kelas)
    {
		$this->db->select('COUNT(a.kode_kelas) AS jmlh_siswa');
		$this->db->from('rombel as a');				
		$this->db->join('kelas b','a.kode_kelas=b.kode_kelas');				
		$this->db->where('a.kode_kelas', $kode_kelas);				
        return $this->db->get();
    }

    function cek_ketersediaan_kelas($tingkat, $rombel)
    {
        $where = [
            'tingkat'   => $tingkat,
            'rombel'    => $rombel
        ];

        return $this->db->get_where('kelas', $where)->row();
    }

}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */