<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TahunAjarModel extends CI_Model {
    
    function get_all()
    {
        $this->db->select('kode_tahunajar, nama_tahunajar, aktif');
        $query = $this->db->get('tahun_ajar');
        return $query->result_array();
    }

    function insert_tahunAjar($data)
    {
        $this->db->insert('tahun_ajar', $data);
    }

    function get_by_id($kode_tahunajar)
    {
        $this->db->where('kode_tahunajar', $kode_tahunajar);
        return $this->db->get("tahun_ajar")->row();
    }

    function update_tahunAjar($kode_tahun_ajar, $data)
    {
        $this->db->where("kode_tahunajar", $kode_tahun_ajar);
        $this->db->update("tahun_ajar", $data);
    }

    function delete_tahunAjar($kode_tahunajar)
    {
        $this->db->where("kode_tahunajar", $kode_tahunajar);
        $this->db->delete("tahun_ajar");
    }

}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */