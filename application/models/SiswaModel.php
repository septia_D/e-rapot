<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SiswaModel extends CI_Model {
    
    // datatables
    function json() {
        $this->datatables->select('a.nis, a.nama_lengkap, a.diterima,c.tingkat,c.rombel,b.kode_kelas');
        $this->datatables->from('siswa as a');
        $this->datatables->join('rombel as b', 'a.nis=b.nis', 'left');
        $this->datatables->join('kelas as c', 'b.kode_kelas=c.kode_kelas', 'left');
        //add this line for join
        $this->datatables->add_column('action','<button class="btn btn-success btn_atur_kelas" type="button" data-nis="$1" data-siswa="$2">Atur Kelas</button>'." ".anchor(site_url('controllerSiswa/lihat_siswa/$1'),'<i class="fa   fa-eye"></i> Lihat', array('class' => 'btn btn-info', 'title' => 'Lihat Data'))." ".anchor(site_url('controllerSiswa/hapus_siswa/$1'),'<i class="fa fa-archive"></i> Hapus','data-nama_siswa="$2" class="btn btn-danger hapus" title="Hapus Data"'), 'nis,nama_lengkap,kode_kelas');
        return $this->datatables->generate();
    }

    // get total rows
    function total_rows($q = NULL) {
        $this->db->like("nis", $q);
        $this->db->or_like("nis", $q);
        $this->db->or_like("nama_lengkap", $q);;
        return $this->db->count_all_results();
     }
 
     // get data with limit and search
     function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by('nis', 'DESC');
        $this->db->like("nis", $q);
        $this->db->or_like("nis", $q);
        $this->db->or_like("nama_lengkap", $q);
        $this->db->limit($limit, $start);
        return $this->db->get('siswa')->result();
     }

    function insert_siswa($data)
    {
        $this->db->insert('siswa', $data);
    }

    function insert_kelas_siswa($data)
    {
        $this->db->insert('rombel', $data);
    }

    function insert_aturMengajar($data)
    {
        $this->db->insert('mengajar', $data);
    }

    function get_by_id($nis)
    {
        $this->db->where('nis', $nis);
        return $this->db->get("siswa")->row();
    }

    function update_siswa($nis, $data)
    {
        $this->db->where("nis", $nis);
        $this->db->update("siswa", $data);
    }

    function update_kelas_siswa($nis, $data_update)
    {
        $this->db->where("nis", $nis);
        $this->db->update("rombel", $data_update);
    }

    function delete_siswa($nis)
    {
        $this->db->where("nis", $nis);
        $this->db->delete("siswa");
    }

    function get_all()
    {
        $this->db->select('kode_mapel, nama_mapel');
        $query = $this->db->get('mata_pelajaran');
        return $query->result();
    }

    function get_all_rombel()
    {
        $this->db->select('kode_kelas, tingkat, rombel');
        $query = $this->db->get('kelas');
        return $query->result();
    }

}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */